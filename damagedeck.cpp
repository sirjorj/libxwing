#include "damagedeck.h"

namespace libxwing {

// deck
DeckNotFound::DeckNotFound(Dck d)         : runtime_error("Deck not found (enum " + std::to_string((int)d) + ")") { }
DeckNotFound::DeckNotFound(std::string d) : runtime_error("Deck not found '" + d + "'") { }

std::vector<Deck> Deck::deck = {
  { Dck::CoreSet,             "core",  "Core Set" },
  { Dck::RebelTransport_Fore, "rtf",   "Rebel Transport (Fore)" },
  { Dck::RebelTransport_Aft,  "rta",   "Rebel Transport (Aft)" },
  { Dck::TantiveIV_Fore,      "t4f",   "Tantive IV (Fore)"},
  { Dck::TantiveIV_Aft,       "t4a",   "Tantive IV (Aft)" },
  { Dck::ImperialRaider_Fore, "irf",   "Imperial Raider (Fore)" },
  { Dck::ImperialRaider_Aft,  "ira",   "Imperial Raider (Aft)" },
  { Dck::AssaultCarrier_Fore, "acf",   "Assault Carrier (Fore)" },
  { Dck::AssaultCarrier_Aft,  "aca",   "Assault Carrier (Aft)" },
  { Dck::TFA,                 "core2", "The Force Awakens Core Set" },
  { Dck::CROC_Fore,           "crocf", "C-ROC (Fore)" },
  { Dck::CROC_Aft,            "croca", "C-ROC (Aft)" },
};

Deck Deck::GetDeck(Dck type) {
  for(Deck d : Deck::deck) {
    if(d.GetType() == type) {
      return d;
    }
  }
  throw DeckNotFound(type);
}

Deck Deck::GetDeck(std::string deck) {
  for(Deck d : Deck::deck) {
    if(d.GetXws() == deck) {
      return d;
    }
  }
  throw DeckNotFound(deck);
}

std::vector<Deck> Deck::GetAllDecks() {
  return Deck::deck;
}
  
Dck         Deck::GetType() const { return this->type; }
std::string Deck::GetXws()  const { return this->xws;  }
std::string Deck::GetName() const { return this->name; }

Deck::Deck(Dck         t,
	   std::string x,
           std::string n)
  : type(t), xws(x), name(n) { }



// trait
TraitNotFound::TraitNotFound(Trt t) : runtime_error("Trait not found (enum " + std::to_string((int)t) + ")") { }

std::vector<Trait> Trait::trait = {
  { Trt::Pilot, "Pilot" },
  { Trt::Ship,  "Ship" },
};

Trait Trait::GetTrait(Trt type) {
  for(Trait t : Trait::trait) {
    if(t.GetType() == type) {
      return t;
    }
  }
  throw TraitNotFound(type);
}

Trt         Trait::GetType() const { return this->type; }
std::string Trait::GetName() const { return this->name; }

Trait::Trait(Trt         t,
             std::string n)
  : type(t), name(n) { }



// damage card
DamageCardNotFound::DamageCardNotFound(DmgCrd dc) : runtime_error("Card not found '" + std::to_string((int)dc.first) + "/" + Deck::GetDeck(dc.second).GetName() + "'") { }

std::vector<DamageCard> DamageCard::damageCards = {
  { Dmg::BlindedPilot,          Dck::CoreSet,             Trt::Pilot, 2, "Blinded Pilot",           "The next time you attack, do not roll any attack dice.  Then flip this card facedown." },
  { Dmg::ConsoleFire,           Dck::CoreSet,             Trt::Ship,  2, "Console Fire",            "At the start of each Combat phase, roll 1 attack die.  On a {HIT} result, suffer 1 damage.  Action: Flip this card facedown." },
  { Dmg::DamagedCockpit,        Dck::CoreSet,             Trt::Pilot, 2, "Damaged Cockpit",         "After the round in which you receive this card, treat your pilot skill value as \"0.\"" },
  { Dmg::DamagedEngine,         Dck::CoreSet,             Trt::Ship,  2, "Damaged Engine",          "Treat all turn maneuvers ({LTURN} or {RTURN}) as red maneuvers." },
  { Dmg::DamagedSensorArray,    Dck::CoreSet,             Trt::Ship,  2, "Damaged Sensor Array",    "You cannot perform the actions listed in your action bar.  Action: Roll 1 attack die.  On a {HIT} result, flip this card facedown." },
  { Dmg::DirectHit,             Dck::CoreSet,             Trt::Ship,  7, "Direct Hit!",             "This card counts as 2 damage against your hull." },
  { Dmg::InjuredPilot,          Dck::CoreSet,             Trt::Pilot, 2, "Injured Pilot",           "All players must ignore your pilot ability and all of your {ELITE} Upgrade cards." },
  { Dmg::MinorExplosion,        Dck::CoreSet,             Trt::Ship,  2, "Minor Explosion",         "Immediately roll 1 attack die.  On a {HIT} result, suffer 1 damage.  Then flip this card facedown." },
  { Dmg::MinorHullBreach,       Dck::CoreSet,             Trt::Ship,  2, "Minor Hull Breach",       "After executing a red maneuver, roll 1 attack die.  On a {HIT} result, suffer 1 damage." },
  { Dmg::MunitionsFailure,      Dck::CoreSet,             Trt::Ship,  2, "Munitions Failure",       "Immediately choose 1 if your secondary weapon Upgrade cards and discard it.  Then flip this Damage card facedown." },
  { Dmg::StructuralDamage,      Dck::CoreSet,             Trt::Ship,  2, "Structural Damage",       "Reduce your agility value by 1 (to a minimum of \"0\").  Action: Roll 1 attack die.  On a {HIT} result, flip this card facedown." },
  { Dmg::StunnedPilot,          Dck::CoreSet,             Trt::Pilot, 2, "Stunned Pilot",           "After you execute a maneuver that causes you to overlap either another ship or an obstacle token, suffer 1 damage." },
  { Dmg::ThrustControlFire,     Dck::CoreSet,             Trt::Ship,  2, "Thrust Control Fire",     "Immediately receive 1 stress token.  Then flip this card facedown." },
  { Dmg::WeaponMalfunction,     Dck::CoreSet,             Trt::Ship,  2, "Weapon Malfunction",      "Reduce your primary weapon value by 1 (to a minimum of \"0\").  Action: Roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },

  { Dmg::BroadcastMalfunction,  Dck::RebelTransport_Fore, Trt::Ship,  1, "Broadcast Malfunction",   "You cannot perform the jam action.  Action: Roll 1 attack die.  On a {HIT} result, flip this card facedown." },
  { Dmg::DamagedStabilizers,    Dck::RebelTransport_Fore, Trt::Ship,  2, "Damaged Stabilizers",     "When you execute a bank maneuver ({LBANK} or {RBANK}), suffer 1 damage.  If you draw a Damage card, draw it from the Fore Damage deck.  Action: Roll 1 attack die.  On a {HIT} result, flip this card facedown." },
  { Dmg::DirectHit,             Dck::RebelTransport_Fore, Trt::Ship,  4, "Direct Hit",              "This card counts as 2 damage against your hull." },
  { Dmg::HullBreach,            Dck::RebelTransport_Fore, Trt::Ship,  2, "Hull Breach",             "Place 1 of your Upgrade cards under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 2 energy to flip this card facedown." },
  { Dmg::SecondaryDriveFailure, Dck::RebelTransport_Fore, Trt::Ship,  1, "Secondary Drive Failure", "You must spend 1 energy to perform any action." },

  { Dmg::CommandPodCasualties,  Dck::RebelTransport_Aft,  Trt::Ship,  2, "Command Pod Casualties",  "Assign 1 stress token to 1 friendly small or large ship.  Then your opponent may assign 1 stress token to 1 of your small or large ships.  Then flip this card facedown." },
  { Dmg::EngineDamage,          Dck::RebelTransport_Aft,  Trt::Ship,  3, "Engine Damage",           "When you execute a maneuver, if the speed of that maneuver plus the number of faceup \"Engine Damage\" cards you have equals or exceeds 5, you must skip your \"Perform Action\" step this round." },
  { Dmg::ProjectorPowerFailure, Dck::RebelTransport_Aft,  Trt::Ship,  1, "Projector Power Failure", "You cannot perform the reinforce action unless you have at least 3 energy." },
  { Dmg::ReactorCowlRupture,    Dck::RebelTransport_Aft,  Trt::Ship,  1, "Reactor Cowl Rupture",    "When you perform a recover action, you cannot recover more than 2 shields.  You must still spend all of your energy." },
  { Dmg::ReactorLeak,           Dck::RebelTransport_Aft,  Trt::Ship,  3, "Reactor Leak",            "When you execute a maneuver, reduce the amount of energy you gain by 1 (to a minimum of 0)." },

  { Dmg::CommsFailure,          Dck::TantiveIV_Fore,      Trt::Ship,  1, "Comms Failure",           "You cannot perform the coordinate action.  Action: Roll 1 attack die.  On a {HIT} or {FOCUE} result, flip this card facedown." },
  { Dmg::DeckBreach,            Dck::TantiveIV_Fore,      Trt::Ship,  1, "Deck Breach",             "Place 1 of the Upgrade cards equipped to this section under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 2 energy to flip this card facedown." },
  { Dmg::DirectHit,             Dck::TantiveIV_Fore,      Trt::Ship,  2, "Direct Hit",              "This card counts as 2 damage against your hull." },
  { Dmg::LifeSupportFailure,    Dck::TantiveIV_Fore,      Trt::Ship,  1, "Life Support Failure",    "Discard 1 of the {TEAM} Upgrade cards equipped to this section.  Then flip this card facedown." },
  { Dmg::ScrambledScopes,       Dck::TantiveIV_Fore,      Trt::Ship,  1, "Scrambled Scopes",        "You cannot perform the target lock action or acquire a target lock.  Action: Roll 1 attack die.  On a {HIT} or {FOCUS} result, flip this card facedown." },
  { Dmg::SecondaryDriveFailure, Dck::TantiveIV_Fore,      Trt::Ship,  1, "Secondary Drive Failure", "You must spend 1 energy to perform any action." },
  { Dmg::TrackingMisalignment,  Dck::TantiveIV_Fore,      Trt::Ship,  1, "Tracking Misalignment",   "Reduce your primary weapon value by 2 (to a minimum of \"0\").  Energy: Spend 4 energy to flip this card facedown." },
  { Dmg::WeaponDamaged,         Dck::TantiveIV_Fore,      Trt::Ship,  2, "Weapon Damaged",          "Place 1 of the {HARDPOINT} Upgrade cards equipped to this section under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 3 energy to flip this card facedown." },

  { Dmg::DeckBreach,            Dck::TantiveIV_Aft,       Trt::Ship,  1, "Deck Breach",             "Place 1 of the Upgrade cards equipped to this section under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 2 energy to flip this card facedown." },
  { Dmg::LifeSupportFailure,    Dck::TantiveIV_Aft,       Trt::Ship,  1, "Life Support Failure",    "Discard 1 of the {TEAM} Upgrade cards equipped to this section.  Then flip this card facedown." },
  { Dmg::GridOverload,          Dck::TantiveIV_Aft,       Trt::Ship,  2, "Grid Overload",           "Immediately remove all energy stored on the aft section Ship card and on all Upgrade cards equipped to this section.  Then flip this card facedown." },
  { Dmg::PowerPlantFailure,     Dck::TantiveIV_Aft,       Trt::Ship,  1, "Power Plant Failure",     "You cannot perform the recover action.  Action: Roll 1 attack die.  On a {HIT} or {FOCUS} result, flip this card facedown." },
  { Dmg::ProjectorPowerFailure, Dck::TantiveIV_Aft,       Trt::Ship,  1, "Projector Power Failure", "You cannot perform the reinforce action unless you have at least 3 energy stored on the aft section Ship card." },
  { Dmg::ReactorLeak,           Dck::TantiveIV_Aft,       Trt::Ship,  2, "Reactor Leak",            "When you execute a maneuver, reduce the amount of energy you gain by 1 (to a minimum of 0)." },
  { Dmg::StructuralCollapse,    Dck::TantiveIV_Aft,       Trt::Ship,  2, "Structural Collapse",     "Deal 2 facedown Damage cards to the fore section.  Then flip this card facedown." },

  { Dmg::DeckBreach,            Dck::ImperialRaider_Fore, Trt::Ship,  1, "Deck Breach",             "Place 1 of the Upgrade cards equipped to this section under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 2 energy to flip this card facedown." },
  { Dmg::DirectHit,             Dck::ImperialRaider_Fore, Trt::Ship,  2, "Direct Hit",              "This card counts as 2 damage against your hull." },
  { Dmg::PowerPlantFailure,     Dck::ImperialRaider_Fore, Trt::Ship,  1, "Power Plant Failure",     "You cannot perform the recover action.  Action: Roll 1 attack die.  On a {HIT} or {FOCUS} result, flip this card facedown." },
  { Dmg::ProjectorPowerFailure, Dck::ImperialRaider_Fore, Trt::Ship,  1, "Projector Power Failure", "You cannot perform the reinforce action unless you have at least 3 energy stored on the aft section Ship card." },
  { Dmg::ReactorLeak,           Dck::ImperialRaider_Fore, Trt::Ship,  2, "Reactor Leak",            "When you execute a maneuver, reduce the amount of energy you gain by 1 (to a minimum of 0)." },
  { Dmg::SecondaryDriveFailure, Dck::ImperialRaider_Fore, Trt::Ship,  1, "Secondary Drive Failure", "You must spend 1 energy to perform any action." },
  { Dmg::TrackingMisalignment,  Dck::ImperialRaider_Fore, Trt::Ship,  1, "Tracking Misalignment",   "Reduce your primary weapon value by 2 (to a minimum of \"0\").  Energy: Spend 4 energy to flip this card facedown." },
  { Dmg::WeaponDamaged,         Dck::ImperialRaider_Fore, Trt::Ship,  1, "Weapon Damaged",          "Place 1 of the {HARDPOINT} Upgrade cards equipped to this section under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 3 energy to flip this card facedown." },

  { Dmg::CommandDeckBreach,     Dck::ImperialRaider_Aft,  Trt::Ship,  1, "Command Deck Breach",     "Place 1 {CREW} and 1 {TEAM} Upgrade card equipped to this section under this card if able.  You cannot use those Upgrade cards while this card is faceup.  Energy: Spend 3 energy to flip this card facedown." },
  { Dmg::CommsFailure,          Dck::ImperialRaider_Aft,  Trt::Ship,  1, "Comms Failure",           "You cannot perform the coordinate action.  Action: Roll 1 attack die.  On a {HIT} or {FOCUE} result, flip this card facedown." },
  { Dmg::DeckBreach,            Dck::ImperialRaider_Aft,  Trt::Ship,  1, "Deck Breach",             "Place 1 of the Upgrade cards equipped to this section under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 2 energy to flip this card facedown." },
  { Dmg::LifeSupportFailure,    Dck::ImperialRaider_Aft,  Trt::Ship,  1, "Life Support Failure",    "Discard 1 of the {TEAM} Upgrade cards equipped to this section.  Then flip this card facedown." },
  { Dmg::MisfiringThrusters,    Dck::ImperialRaider_Aft,  Trt::Ship,  2, "Misfiring Thrusters",     "During the Planning phase, assign your chosen maneuver faceup.  Energy: Spend 1 energy to roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },
  { Dmg::ScrambledScopes,       Dck::ImperialRaider_Aft,  Trt::Ship,  1, "Scrambled Scopes",        "You cannot perform the target lock action or acquire a target lock.  Action: Roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },
  { Dmg::StructuralCollapse,    Dck::ImperialRaider_Aft,  Trt::Ship,  2, "Structural Collapse",     "Deal 2 facedown Damage cards to the fore section.  Then flip this card facedown." },
  { Dmg::WeaponDamaged,         Dck::ImperialRaider_Aft,  Trt::Ship,  1, "Weapon Damaged",          "Place 1 of the {HARDPOINT} Upgrade cards equipped to this section under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 3 energy to flip this card facedown." },

  { Dmg::CommsFailure,          Dck::AssaultCarrier_Fore, Trt::Ship,  1, "Comms Failure",           "You cannot perform the coordinate action.  Action: Roll 1 attack die.  On a {HIT} or {FOCUE} result, flip this card facedown." },
  { Dmg::DamagedDockingClamp,   Dck::AssaultCarrier_Fore, Trt::Ship,  3, "Damaged Docking Clamp",   "Choose a docked ship, deal it 1 facedown Damage card, and deploy it.  It cannot attack this round.  If you have no docked ships, this section suffers 1 damage.  The flip this card facedown." },
  { Dmg::HullBreach,            Dck::AssaultCarrier_Fore, Trt::Ship,  2, "Hull Breach",             "Place 1 of your Upgrade cards under this card (except Docking Clamps).  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 2 energy to flip this card facedown." },
  { Dmg::ScrambledScopes,       Dck::AssaultCarrier_Fore, Trt::Ship,  2, "Scrambled Scopes",        "You cannot perform the target lock action or acquire a target lock.  Action: Roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },
  { Dmg::SecondaryDriveFailure, Dck::AssaultCarrier_Fore, Trt::Ship,  1, "Secondary Drive Failure", "You must spend 1 energy to perform any action." },
  { Dmg::ViewportRupture,       Dck::AssaultCarrier_Fore, Trt::Ship,  1, "Viewport Rupture",        "Discard 1 of your {CREW} or {TEAM} Upgrade cards.  Then flip this card facedown." },

  { Dmg::DamagedDockingClamp,   Dck::AssaultCarrier_Aft,  Trt::Ship,  3, "Damaged Docking Clamp",   "Choose a docked ship, deal it 1 facedown Damage card, and deploy it.  It cannot attack this round.  If you have no docked ships, this section suffers 1 damage.  The flip this card facedown." },
  { Dmg::DamagedStabilizers,    Dck::AssaultCarrier_Aft,  Trt::Ship,  2, "Damaged Stabilizers",     "When you execute a bank maneuver ({LBANK} or {RBANK}), suffer 1 damage.  If you draw a Damage card, draw it from the Fore Damage deck.  Action: Roll 1 attack die.  On a {HIT} result, flip this card facedown." },
  { Dmg::ProjectorPowerFailure, Dck::AssaultCarrier_Aft,  Trt::Ship,  1, "Projector Power Failure", "You cannot perform the reinforce action unless you have at least 3 energy." },
  { Dmg::ReactorCowlRupture,    Dck::AssaultCarrier_Aft,  Trt::Ship,  1, "Reactor Cowl Rupture",    "When you perform a recover action, you cannot recover more than 2 shields.  You must still spend all of your energy." },
  { Dmg::ReactorLeak,           Dck::AssaultCarrier_Aft,  Trt::Ship,  2, "Reactor Leak",            "When you execute a maneuver, reduce the amount of energy you gain by 1 (to a minimum of 0)." },
  { Dmg::WeaponsOffline,        Dck::AssaultCarrier_Aft,  Trt::Ship,  1, "Weapons Offline",         "You cannot perform attacks.  Action: Flip this card facedown." },

  { Dmg::BlindedPilot,          Dck::TFA,                 Trt::Pilot, 2, "Blinded Pilot",           "You cannot perform attacks.  After your next opportunity to attack (even if there was no target for an attack), flip this card facedown." },
  { Dmg::ConsoleFire,           Dck::TFA,                 Trt::Ship,  2, "Console Fire",            "At the start of each Combat phase, roll 1 attack die.  On a {HIT} result, suffer 1 damage.  Action: Flip this card facedown." },
  { Dmg::DamagedCockpit,        Dck::TFA,                 Trt::Pilot, 2, "Damaged Cockpit",         "Starting the round after you receive this card, treat your pilot skill value as \"0.\"" },
  { Dmg::DamagedEngine,         Dck::TFA,                 Trt::Ship,  2, "Damaged Engine",          "Treat all turn maneuvers ({LTURN} or {RTURN}) as red maneuvers." },
  { Dmg::DamagedSensorArray,    Dck::TFA,                 Trt::Ship,  2, "Damaged Sensor Array",    "You cannot perform any actions except actions listed on Damage cards.  Action: Roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },
  { Dmg::DirectHit,             Dck::TFA,                 Trt::Ship,  7, "Direct Hit!",             "This card counts as 2 Damage against your hull." },
  { Dmg::LooseStabilizer,       Dck::TFA,                 Trt::Ship,  2, "Loose Stabilizer",        "After you execute a white maneuver, receive 1 stress token.  Action: Flip this card facedown." },
  { Dmg::MajorExplosion,        Dck::TFA,                 Trt::Ship,  2, "Major Explosion",         "Roll 1 attack die.  ON a {HIT} result, suffer 1 critical damage.  Then flip this card facedown." },
  { Dmg::MajorHullBreach,       Dck::TFA,                 Trt::Ship,  2, "Major Hull Breach",       "Starting the round after you receive this card, all Damage cards dealt to you are dealt faceup.  Action: Flip this card facedown." },
  { Dmg::ShakenPilot,           Dck::TFA,                 Trt::Pilot, 2, "Shaken Pilot",            "During the Planning phase, you cannot be assigned straight ({STRAIGHT}) maneuvers.  When you reveal a maneuver, flip this card facedown." },
  { Dmg::StructuralDamage,      Dck::TFA,                 Trt::Ship,  2, "Structural Damage",       "Reduce your agility value by 1 (to a minimum of \"0\").  Action: Roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },
  { Dmg::StunnedPilot,          Dck::TFA,                 Trt::Pilot, 2, "Stunned Pilot",           "After you execute a maneuver, if you are touching another ship or overlapping an obstacle token, suffer 1 damage." },
  { Dmg::ThrustControlFire,     Dck::TFA,                 Trt::Ship,  2, "Thrust Control Fire",     "Receive 1 stress token.  Then flip this card facedown." },
  { Dmg::WeaponsFailure,        Dck::TFA,                 Trt::Ship,  2, "Weapons Failure",         "When attacking, roll 1 fewer attack die.  Action: Roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },

  { Dmg::HullBreach,            Dck::CROC_Fore,           Trt::Ship,  2, "Hull Breach",             "Place 1 of your Upgrade cards under this card.  You cannot use that Upgrade card while this card is faceup.  Energy: Spend 2 energy to flip this card facedown." },
  { Dmg::ScrambledScopes,       Dck::CROC_Fore,           Trt::Ship,  2, "Scrambled Scopes",        "You cannot perform the target lock action or acquire a target lock.  Action: Roll 1 attack die.  On a {HIT} or {CRIT} result, flip this card facedown." },
  { Dmg::SecondaryDriveFailure, Dck::CROC_Fore,           Trt::Ship,  2, "Secondary Drive Failure", "You must spend 1 energy to perform any action." },
  { Dmg::SpilledCargo,          Dck::CROC_Fore,           Trt::Ship,  2, "Spilled Cargo",           "Discard 1 {CARGO} Upgrade card, then place 1 container token touching you.  If you have no equipped {CARGO} Upgrade cards or no container tokens, this section suffers 1 damage.  Then flip this card facedown." },
  { Dmg::ViewportRupture,       Dck::CROC_Fore,           Trt::Ship,  2, "Viewport Rupture",        "Discard 1 of your {CREW} or {TEAM} Upgrade cards.  Then flip this card facedown." },

  { Dmg::DamagedDish,           Dck::CROC_Aft,            Trt::Ship,  1, "Damaged Dish",            "You cannot perform the Jam action.  ACtion: Flip this card facedown." },
  { Dmg::DamagedStabilizers,    Dck::CROC_Aft,            Trt::Ship,  2, "Damaged Stabilizers",     "When you execute a bank maneuver ({LBANK} or {RBANK}), suffer 1 damage.  If you draw a Damage card, draw it from the Fore Damage deck.  Action: Roll 1 attack die.  On a {HIT} result, flip this card facedown." },
  { Dmg::ProjectorPowerFailure, Dck::CROC_Aft,            Trt::Ship,  1, "Projector Power Failure", "You cannot perform the reinforce action unless you have at least 3 energy." },
  { Dmg::ReactorCowlRupture,    Dck::CROC_Aft,            Trt::Ship,  1, "Reactor Cowl Rupture",    "When you perform a recover action, you cannot recover more than 2 shields.  You must still spend all of your energy." },
  { Dmg::ReactorLeak,           Dck::CROC_Aft,            Trt::Ship,  2, "Reactor Leak",            "When you execute a maneuver, reduce the amount of energy you gain by 1 (to a minimum of 0)." },
  { Dmg::SpilledCargo,          Dck::CROC_Aft,            Trt::Ship,  2, "Spilled Cargo",           "Discard 1 {CARGO} Upgrade card, then place 1 container token touching you.  If you have no equipped {CARGO} Upgrade cards or no container tokens, this section suffers 1 damage.  Then flip this card facedown." },
  { Dmg::WeaponsOffline,        Dck::CROC_Aft,            Trt::Ship,  1, "Weapons Offline",         "You cannot perform attacks.  Action: Flip this card facedown." },
};

DamageCard DamageCard::GetDamageCard(DmgCrd dc) {
  for(DamageCard d : DamageCard::damageCards) {
    if((d.GetDamage() == dc.first) && (d.GetDeck().GetType() == dc.second)) {
      return d;
    }
  }
  throw DamageCardNotFound(dc);
}

std::vector<DamageCard> DamageCard::GetAllDamageCards(Dck d) {
  std::vector<DamageCard> ret;
  for(DamageCard dc : DamageCard::damageCards) {
    if(dc.GetDeck().GetType() == d) {
      ret.push_back(dc);
    }
  }
  return ret;
}
  
Dmg         DamageCard::GetDamage() const { return this->damage; }
Deck        DamageCard::GetDeck()   const { return Deck::GetDeck(this->deck); }
Trait       DamageCard::GetTrait()  const { return Trait::GetTrait(this->trait); }
uint8_t     DamageCard::GetCount()  const { return this->count; }
std::string DamageCard::GetName()   const { return this->name; }
std::string DamageCard::GetText()   const { return this->text; }

DamageCard::DamageCard(Dmg          dmg,
                       Dck          dck,
                       Trt          trt,
                       uint8_t      cou,
                       std::string  nam,
                       std::string  txt)
  : damage(dmg), deck(dck), trait(trt), count(cou), name(nam), text(txt) { }

}
