#pragma once
#include "shared.h"
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

// PrimaryArc
enum class Arc {
  None,
  Front,
  Turret,
  FrontRear,
  FrontHalf,
  Mobile,
  Bullseye,
};

class PrimaryArcNotFound : public std::runtime_error {
 public:
  PrimaryArcNotFound(Arc a);
};

class PrimaryArc {
 public:
  static PrimaryArc GetPrimaryArc(Arc a);
  Arc         GetType()      const;
  std::string GetName()      const;
  std::string GetShortName() const;

 private:
  Arc type;
  std::string name;
  std::string shortName;

  static std::vector<PrimaryArc> primaryArcs;

  PrimaryArc(Arc         t,
             std::string n,
             std::string s);
};



// Bearing
enum class Brn {
  LTurn,
  LBank,
  Straight,
  RBank,
  RTurn,
  KTurn,
  Stationary,
  LSloop,
  RSloop,
  LTroll,
  RTroll,
  RevLBank,
  RevStraight,
  RevRBank,
};

class BearingNotFound : public std::runtime_error {
 public:
  BearingNotFound(Brn a);
  BearingNotFound(std::string txt);
};

class Bearing {
 public:
  static Bearing GetBearing(Brn b);
  static Bearing GetBearing(std::string text);

  Brn GetType()              const;
  std::string GetName()      const;
  std::string GetShortName() const;
  std::string GetCharacter() const;
  std::string GetText()      const;

 private:
  Brn         type;
  std::string name;
  std::string shortName;
  std::string character;
  std::string text;

  static std::vector<Bearing> bearings;

  Bearing(Brn         b,
          std::string n,
          std::string x,
          std::string c,
          std::string t);
};



enum class Difficulty {
  Green,
  White,
  Red
};

struct Maneuver {
  int8_t     speed;
  Brn        bearing;
  Difficulty difficulty;
  int8_t     energy=0;
};

typedef std::vector<Maneuver> Maneuvers;

bool FindManeuver(Maneuvers ms, uint8_t s, Brn b, Maneuver &m);



// ship
enum class Shp {
  XWing,
  TIEFighter,
  YWing,
  TIEAdvanced,
  YT1300,
  Firespray31,
  AWing,
  TIEInterceptor,
  GR75MediumTransport,
  HWK290,
  LambdaClassShuttle,
  BWing,
  TIEBomber,
  Z95Headhunter,
  TIEDefender,
  EWing,
  TIEPhantom,
  CR90Corvette,
  YT2400,
  VT49Decimator,
  M3AInterceptor,
  StarViper,
  IG2000,
  RaiderClassCorvette,
  YV666,
  Kihraxz,
  KWing,
  TIEPunisher,
  GozantiClassCruiser,
  VCX100,
  AttackShuttle,
  TIEAdvancedPrototype,
  G1A,
  JumpMaster5000,
  T70XWing,
  TIEfoFighter,
  ARC170,
  TIEsfFighter,
  ProtectorateStarfighter,
  LancerClassPursuitCraft,
  UpsilonClassShuttle,
  Quadjumper,
  UWing,
  TIEStriker,
  CROC,
  AuzituckGunship,
  ScurrgH6Bomber,
  TIEAggressor,
  SheathipedeClassShuttle,
  M12LKimogila,
  AlphaClassStarWing,
  BSF17Bomber,
  TIESilencer,
  TIEReaper
};

class ShipNotFound : public std::runtime_error {
 public:
  ShipNotFound(Shp s);
  ShipNotFound(std::string xws);
};

class Ship {
 public:
  static Ship GetShip(Shp s);
  static Ship GetShip(std::string xws);
  static std::vector<Ship> FindShip(std::string s);
  static std::vector<Ship> GetAllShips();

  Shp GetType()               const;
  std::string GetXws()        const;
  std::string GetName()       const;
  std::string GetShortName()  const;
  Maneuvers   GetManeuvers()  const;
  PrimaryArc  GetPrimaryArc() const;

 private:
  Shp type;
  std::string xws;
  std::string name;
  std::string shortName;
  Maneuvers   maneuvers;
  Arc         arc;

  static std::vector<Ship> ships;

  Ship(Shp         t,
       std::string x,
       std::string n,
       std::string s,
       Maneuvers   m,
       Arc         a);
};

}
