#pragma once
#include "shared.h"
#include "pilot.h"
#include <experimental/optional>
#include <vector>

namespace libxwing {

class VendorSpecific {
 public:
  VendorSpecific(std::string IMPLEMENTATION_NAME);
  std::string GetImplementationName() const;
  void SetUrl(std::string v);
  std::string GetUrl() const;
  void SetBuilder(std::string v);
  std::string GetBuilder() const;
  void SetBuilderUrl(std::string v);
  std::string GetBuilderUrl() const;

 private:
  std::string implementation_name;
  std::string url;
  std::string builder;
  std::string builderUrl;
};



class VendorSpecificNotSet : public std::runtime_error {
 public:
  VendorSpecificNotSet();
};



class SquadNotFound : public std::runtime_error {
 public:
  SquadNotFound();
  SquadNotFound(std::string xwsFile);
};



class Squad {
 public:
  Squad(Fac f);
  Squad(std::string xwsFile);
  Squad(std::istream &xwsStream);

  void SetName(std::string);
  std::string GetName() const;
  void SetDescription(std::string);
  std::string GetDescription() const;
  Faction GetFaction() const;
  void AddPilot(Pilot p);
  std::vector<Pilot>& GetPilots();
  uint16_t GetCost();
  void AddVendorSpecific(std::string application);
  void RemoveVendorSpecific();
  VendorSpecific& GetVendorSpecific();
  std::vector<std::string> Verify();
  bool Save(std::string filename);
  bool Save(std::ostream &xwsStream);

 private:
  void Load(std::istream &xws);
  std::string name;
  std::string description;
  Fac         faction;
  std::vector<Pilot> pilots;
  std::experimental::optional<VendorSpecific> vendorSpecific;
};

}
