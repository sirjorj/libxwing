#include "ship.h"

namespace libxwing {

typedef Maneuvers  M;

const Brn LT = Brn::LTurn;
const Brn LB = Brn::LBank;
const Brn ST = Brn::Straight;
const Brn RB = Brn::RBank;
const Brn RT = Brn::RTurn;
const Brn KT = Brn::KTurn;
const Brn SY = Brn::Stationary;
const Brn LS = Brn::LSloop;
const Brn RS = Brn::RSloop;
const Brn LR = Brn::LTroll;
const Brn RR = Brn::RTroll;
const Brn RLB= Brn::RevLBank;
const Brn RST= Brn::RevStraight;
const Brn RRB= Brn::RevRBank;

const Difficulty G = Difficulty::Green;
const Difficulty W = Difficulty::White;
const Difficulty R = Difficulty::Red;

M mXWING = {                    {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G}
};

M mTIEFI = {                    {5,ST,W},
                                {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W}, {3,KT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
            {1,LT,W},                               {1,RT,W}
};

M mYWING = {                    {4,ST,R},                     {4,KT,R},
            {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mTIEAD = {                    {5,ST,W},
                                {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,G},           {1,RB,G}
};

M mYT130 = {                    {4,ST,W},                     {4,KT,R},
                      {3,LB,W}, {3,ST,W}, {3,RB,W},           {3,KT,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,W}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,W}
};

M mFSP31 = {                    {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W}, {3,KT,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G}
};

M mAWING = {                    {5,ST,G},                     {5,KT,R},
                                {4,ST,G},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W}, {3,KT,R},
            {2,LT,G}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,G},
            {1,LT,W},                               {1,RT,W}
};

M mTIEIN = {                    {5,ST,W},                     {5,KT,R},
                                {4,ST,G},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W}, {3,KT,R},
            {2,LT,G}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,G},
            {1,LT,W},                               {1,RT,W}
};

M mGR75T = {            {4,ST,W,0},
                        {3,ST,W,1},
            {2,LB,W,1}, {2,ST,W,2}, {2,RB,W,1},
            {1,LB,W,2}, {1,ST,W,3}, {1,RB,W,2}
};

M mHWK29 = {                    {4,ST,R},
                      {3,LB,R}, {3,ST,W}, {3,RB,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G}
};

M mLAMBD = {          {3,LB,R}, {3,ST,W}, {3,RB,R},
            {2,LT,R}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,R},
                      {1,LB,G}, {1,ST,G}, {1,RB,G},
                                {0,SY,R}
};

M mBWING = {                    {4,ST,R},
                      {3,LB,R}, {3,ST,W}, {3,RB,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W}, {2,KT,R},
            {1,LT,R}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,R}
};

M mTIEBO = {                                                  {5,KT,R},
                                {4,ST,W},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,R}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,R},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mZ95HH = {                    {4,ST,W},
            {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W}, {3,KT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mTIEDE = {                    {5,ST,G},
                                {4,ST,G},                     {4,KT,W},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,R}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,R},
            {1,LT,R}, {1,LB,W},           {1,RB,W}, {1,RT,R}
};

M mEWING = {                    {5,ST,W},
                                {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W}, {3,KT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mTIEPH = {                    {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W}, {3,KT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
            {1,LT,W},                               {1,RT,W}
};

M mTANTI = {            {4,ST,W,1},
                        {3,ST,W,2},
            {2,LB,W,2}, {2,ST,W,3}, {2,RB,W,2},
            {1,LB,W,3},             {1,RB,W,3}
};

M mYT240 = {                    {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,W}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,W}
};

M mVT49D = {                    {4,ST,W},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,W}, {1,ST,W}, {1,RB,W}
};

M mM3AIN = {                                                  {5,KT,R},
                                {4,ST,W},
                      {3,LB,W}, {3,ST,G}, {3,RB,W},           {3,KT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
            {1,LT,W}, {1,LB,G},           {1,RB,G}, {1,RT,W}
};

M mSTARV = {                    {4,ST,W},
                      {3,LB,W}, {3,ST,G}, {3,RB,W},           {3,LS,R}, {3,RS,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,W}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,W}
};

M mAGGRE = {                                                  {4,KT,R},
                      {3,LB,G}, {3,ST,G}, {3,RB,G},                     {3,LS,R}, {3,RS,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
            {1,LT,W}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,W}
};

M mRAIDE = {            {4,ST,W,2},
                        {3,ST,W,3},
            {2,LB,W,2}, {2,ST,W,3}, {2,RB,W,2},
            {1,LB,W,3}, {1,ST,W,3}, {1,RB,W,3}
};

M mYV666 = {                    {4,ST,W},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,R}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,R},
                      {1,LB,G}, {1,ST,G}, {1,RB,G},
                                {0,SY,R}
};

M mKIHRA = {                                                  {5,KT,R},
                                {4,ST,W},                     {4,KT,R},
                      {3,LB,W}, {3,ST,W}, {3,RB,W},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
            {1,LT,W}, {1,LB,G},           {1,RB,G}, {1,RT,W}
};

M mKWING = {          {3,LB,W}, {3,ST,W}, {3,RB,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G}
};

M mTIEPU = {                                                  {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},
            {2,LT,R}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,R},
                      {1,LB,G}, {1,ST,G}, {1,RB,G}
};

M mGOZAN = {            {4,ST,W,1},
                        {3,ST,W,1},
            {2,LB,W,1}, {2,ST,W,2}, {2,RB,W,1},
            {1,LB,W,2}, {1,ST,W,3}, {1,RB,W,2}
};

M mVCX10 = {                                                  {5,KT,R},
                                {4,ST,W},
            {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
            {1,LT,R}, {1,LB,W}, {1,ST,G}, {1,RB,W}, {1,RT,R}
};

M mATTAC = {                    {4,ST,W},                     {4,KT,R},
            {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,R}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,R}
};

M mTIEAP = {                    {5,ST,W},
                                {4,ST,G},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,G}, {1,LB,G},           {1,RB,G}, {1,RT,G}
};

M mG1AST = {                    {4,ST,W},                     {4,KT,R},
                      {3,LB,R}, {3,ST,G}, {3,RB,R},           {3,KT,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,R}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,R}
};

M mJUMPM = {                    {4,ST,W},                     {4,KT,R},
                      {3,LB,W}, {3,ST,W}, {3,RB,W},
            {2,LT,G}, {2,LB,G}, {2,ST,G}, {2,RB,W}, {2,RT,W},           {2,LS,W}, {2,RS,R},
            {1,LT,G}, {1,LB,G}, {1,ST,G}, {1,RB,W}, {1,RT,W}
};

M mT70XW = {                    {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},           {3,LR,R}, {3,RR,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G}
};

M mTIEFO = {                    {5,ST,W},
                                {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,G}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,G},           {2,LS,R}, {2,RS,R},
            {1,LT,W},                               {1,RT,W}
};

M mARC17 = {                    {4,ST,R},                     {4,KT,R},
            {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G}
};

M mTIESF = {                    {4,ST,W},
            {3,LT,R}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,R}, {3,LS,R}, {3,RS,R},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,R}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,R}
};

M mPROTE = {                    {5,ST,W},
                                {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,G}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,G},           {2,LR,R}, {2,RR,R},
            {1,LT,W},                               {1,RT,W}
};

M mLANCE = {                    {5,ST,W},                     {5,KT,R},
                                {4,ST,G},
            {3,LT,G}, {3,LB,G}, {3,ST,G}, {3,RB,G}, {3,RT,G},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,W}, {1,ST,W}, {1,RB,W}
};

M mUWING = {                    {4,ST,W},
                      {3,LB,W}, {3,ST,W}, {3,RB,W},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G},
                                {0,SY,R}
};

M mTIEST = {          {3,LB,W}, {3,ST,G}, {3,RB,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W}, {2,KT,R}, {2,LS,R}, {2,RS,R},
            {1,LT,W}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,W}
};

M mUPSIL = {{3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
            {1,LT,R}, {1,LB,W}, {1,ST,G}, {1,RB,W}, {1,RT,R},
                                {0,SY,R}
};

M mQUADJ = {          {3,LB,W}, {3,ST,G}, {3,RB,W},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W}, {2,LS,R}, {2,RS,R},
            {1,LT,W},           {1,ST,W},           {1,RT,W},
                      {1,RLB,R},{1,RST,R},{1,RRB,R}
};

M mCROC  = {            {4,ST,W,1},
                        {3,ST,W,2},
            {2,LB,W,1}, {2,ST,W,2}, {2,RB,W,1},
            {1,LB,W,2}, {1,ST,W,3}, {1,RB,W,2}
};

M mAUZGS = {                    {5,ST,R},
                                {4,ST,W},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
                      {1,LB,G}, {1,ST,G}, {1,RB,G},
};

M mSCURG = {                    {5,ST,R},
                                {4,ST,W},
            {3,LT,R}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,R}, {3,LR,R}, {3,RR,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mTIEAG = {                    {4,ST,W},                     {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mPHAN2 = {                    {4,ST,R},
            {3,LT,R}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,R}, {3,KT,R},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mKIMOG = {                                                  {4,KT,R},
            {3,LT,W}, {3,LB,W}, {3,ST,G}, {3,RB,W}, {3,RT,W},
            {2,LT,R}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,R},
            {1,LT,R}, {1,LB,W}, {1,ST,G}, {1,RB,W}, {1,RT,R}
};

M mGUNBO = {                    {4,ST,R},
            {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},
            {2,LT,W}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,W},
                      {1,LB,W}, {1,ST,G}, {1,RB,W}
};

M mBSF17 = {          {3,LB,W}, {3,ST,W}, {3,RB,W},
            {2,LT,W}, {2,LB,W}, {2,ST,G}, {2,RB,W}, {2,RT,W},
            {1,LT,R}, {1,LB,G}, {1,ST,G}, {1,RB,G}, {1,RT,R},
                                {0,SY,R}
};

M mTIESI = {                    {5,ST,G},
                                {4,ST,G},                     {4,KT,R},
            {3,LT,W}, {3,LB,G}, {3,ST,G}, {3,RB,G}, {3,RT,W}, {3,LR,R}, {3,RR,R},
            {2,LT,G}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,G},
            {1,LT,W},                               {1,RT,W}
};

M mTIERE = {          {3,LB,R}, {3,ST,G}, {3,RB,R},
            {2,LT,R}, {2,LB,G}, {2,ST,G}, {2,RB,G}, {2,RT,R},
            {1,LT,W}, {1,LB,W}, {1,ST,G}, {1,RB,W}, {1,RT,W}, {1,LS,R}, {1,RS,R},
                                {0,SY,R}
};

std::vector<Ship> Ship::ships = {
  { Shp::XWing,                   "xwing",                   "X-Wing",                     "X-Wing",          mXWING, Arc::Front     },
  { Shp::TIEFighter,              "tiefighter",              "TIE Fighter",                "TIE Fighter",     mTIEFI, Arc::Front     },
  { Shp::YWing,                   "ywing",                   "Y-Wing",                     "Y-Wing",          mYWING, Arc::Front     },
  { Shp::TIEAdvanced,             "tieadvanced",             "TIE Advanced",               "TIE Advanced",    mTIEAD, Arc::Front     },
  { Shp::YT1300,                  "yt1300",                  "YT-1300",                    "YT-1300",         mYT130, Arc::Turret    },
  { Shp::Firespray31,             "firespray31",             "Firespray-31",               "Firespray-31",    mFSP31, Arc::FrontRear },
  { Shp::AWing,                   "awing",                   "A-Wing",                     "A-Wing",          mAWING, Arc::Front     },
  { Shp::TIEInterceptor,          "tieinterceptor",          "TIE Interceptor",            "TIE Interceptor", mTIEIN, Arc::Front     },
  { Shp::GR75MediumTransport,     "gr75mediumtransport",     "GR-75 Medium Transport",     "GR-75 Transport", mGR75T, Arc::None      },
  { Shp::HWK290,                  "hwk290",                  "HWK-290",                    "HWK-290",         mHWK29, Arc::Front     },
  { Shp::LambdaClassShuttle,      "lambdaclassshuttle",      "Lambda-class Shuttle",       "Lambda",          mLAMBD, Arc::Front     },
  { Shp::BWing,                   "bwing",                   "B-Wing",                     "B-Wing",          mBWING, Arc::Front     },
  { Shp::TIEBomber,               "tiebomber",               "TIE Bomber",                 "TIE Bomber",      mTIEBO, Arc::Front     },
  { Shp::Z95Headhunter,           "z95headhunter",           "Z-95 Headhunter",            "Z-95",            mZ95HH, Arc::Front     },
  { Shp::TIEDefender,             "tiedefender",             "TIE Defender",               "TIE Defender",    mTIEDE, Arc::Front     },
  { Shp::EWing,                   "ewing",                   "E-Wing",                     "E-Wing",          mEWING, Arc::Front     },
  { Shp::TIEPhantom,              "tiephantom",              "TIE Phantom",                "TIE Phantom",     mTIEPH, Arc::Front     },
  { Shp::CR90Corvette,            "cr90corvette",            "CR90 Corvette",              "CR90 Corvette",   mTANTI, Arc::Turret    }, // Fore only
  { Shp::YT2400,                  "yt2400",                  "YT-2400 Freighter",          "YT-2400",         mYT240, Arc::Turret    },
  { Shp::VT49Decimator,           "vt49decimator",           "VT-49 Decimator",            "Decimator",       mVT49D, Arc::Turret    },
  { Shp::M3AInterceptor,          "m3ainterceptor",          "M3-A Scyk Interceptor",      "M3-A Scyk",       mM3AIN, Arc::Front     },
  { Shp::StarViper,               "starviper",               "StarViper",                  "StarViper",       mSTARV, Arc::Front     },
  { Shp::IG2000,                  "aggressor",               "IG-2000",                    "IG-2000",         mAGGRE, Arc::Front     },
  { Shp::RaiderClassCorvette,     "raiderclasscorvette",     "Raider-Class Corv.",         "Raider",          mRAIDE, Arc::Front     }, // Fore only
  { Shp::YV666,                   "yv666",                   "YV-666 Freighter",           "YV-666",          mYV666, Arc::FrontHalf },
  { Shp::Kihraxz,                 "kihraxzfighter",          "Kihraxz Fighter",            "Kihraxz",         mKIHRA, Arc::Front     },
  { Shp::KWing,                   "kwing",                   "K-Wing",                     "K-Wing",          mKWING, Arc::Turret    },
  { Shp::TIEPunisher,             "tiepunisher",             "TIE Punisher",               "TIE Punisher",    mTIEPU, Arc::Front     },
  { Shp::GozantiClassCruiser,     "gozanticlasscruiser",     "Gozanti-Class Cruiser",      "Gozanti",         mGOZAN, Arc::None      },
  { Shp::VCX100,                  "vcx100",                  "VCX-100",                    "VCX-100",         mVCX10, Arc::Front     },
  { Shp::AttackShuttle,           "attackshuttle",           "Attack Shuttle",             "Attack Shuttle",  mATTAC, Arc::Front     },
  { Shp::TIEAdvancedPrototype,    "tieadvprototype",         "TIE Advanced Prototype",     "TIE Adv. Prot.",  mTIEAP, Arc::Front     },
  { Shp::G1A,                     "g1astarfighter",          "G-1A Starfighter",           "G-1A",            mG1AST, Arc::Front     },
  { Shp::JumpMaster5000,          "jumpmaster5000",          "JumpMaster 5000",            "JM5K",            mJUMPM, Arc::Turret    },
  { Shp::T70XWing,                "t70xwing",                "T-70 X-Wing Fighter",        "T-70 X-Wing",     mT70XW, Arc::Front     },
  { Shp::TIEfoFighter,            "tiefofighter",            "TIE/fo Fighter",             "TIE/fo Fighter",  mTIEFO, Arc::Front     },
  { Shp::ARC170,                  "arc170",                  "ARC-170",                    "ARC-170",         mARC17, Arc::FrontRear },
  { Shp::TIEsfFighter,            "tiesffighter",            "TIE/sf Fighter",             "TIE/sf Fighter",  mTIESF, Arc::FrontRear },
  { Shp::ProtectorateStarfighter, "protectoratestarfighter", "Protectorate Starfighter",   "Protectorate",    mPROTE, Arc::Front     },
  { Shp::LancerClassPursuitCraft, "lancerclasspursuitcraft", "Lancer-class Pursuit Craft", "Lancer",          mLANCE, Arc::Mobile    },
  { Shp::UpsilonClassShuttle,     "upsilonclassshuttle",     "Upsilon-class Shuttle",      "Upsilon",         mUPSIL, Arc::Front     },
  { Shp::Quadjumper,              "quadjumper",              "Quadjumper",                 "Quadjumper",      mQUADJ, Arc::Front     },
  { Shp::UWing,                   "uwing",                   "U-Wing",                     "U-Wing",          mUWING, Arc::Front     },
  { Shp::TIEStriker,              "tiestriker",              "TIE Striker",                "TIE Striker",     mTIEST, Arc::Front     },
  { Shp::CROC,                    "croc",                    "C-ROC Cruiser",              "C-ROC",           mCROC,  Arc::None      },
  { Shp::AuzituckGunship,         "auzituckgunship",         "Auzituck Gunship",           "Auzituck",        mAUZGS, Arc::FrontHalf },
  { Shp::ScurrgH6Bomber,          "scurrgh6bomber",          "Scurrg H6 Bomber",           "Scurrg",          mSCURG, Arc::Front     },
  { Shp::TIEAggressor,            "tieaggressor",            "TIE Aggressor",              "TIE Aggressor",   mTIEAG, Arc::Front     },
  { Shp::SheathipedeClassShuttle, "sheathipedeclassshuttle", "Sheathipede-Class Shuttle",  "Sheathipede",     mPHAN2, Arc::FrontRear },
  { Shp::M12LKimogila,            "m12lkimogilafighter",     "M12-L Kimogila Fighter",     "Kimogila",        mKIMOG, Arc::Bullseye  },
  { Shp::AlphaClassStarWing,      "alphaclassstarwing",      "Alpha Class Star Wing",      "Star Wing",       mGUNBO, Arc::Front     },
  { Shp::BSF17Bomber,             "bsf17bomber",             "B/SF-17 Bomber",             "B/SF-17 Bomber",  mBSF17, Arc::Turret    },
  { Shp::TIESilencer,             "tiesilencer",             "TIE Silencer",               "TIE Silencer",    mTIESI, Arc::Front     },
  { Shp::TIEReaper,               "tiereaper",               "TIE Reaper",                 "TIE Reaper",      mTIERE, Arc::Front     },
};

}
