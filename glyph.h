#pragma once
#include "dice.h"
#include "faction.h"
#include "shared.h"
#include "ship.h"
#include "upgrade.h"

namespace libxwing {

class ShipGlyphNotFound : public std::runtime_error {
 public:
  ShipGlyphNotFound(Shp s);
};

class ShipGlyph {
 public:
  static ShipGlyph GetShipGlyph(Shp s);
  static std::string GetGlyph(Shp s);
  static std::vector<ShipGlyph> GetAllShipGlyphs();

  Shp         GetType();
  std::string GetScssName();
  std::string GetGlyph();


 private:
  Shp         type;
  std::string scssName;
  std::string glyph;

  static std::vector<ShipGlyph> shipGlyphs;

  ShipGlyph(Shp t,
	    std::string scss,
	    std::string gly);

};







enum class Ico {
  agility,
  astromech,
  attack,
  attack_180,
  attack_bullseye,
  attack_bullseye_top_separated,
  attack_frontback,
  attack_turret,
  bankleft,
  bankright,
  barrelroll,
  bomb,
  boost,
  cannon,
  cargo,
  cloak,
  coordinate,
  condition,
  condition_outline,
  crew,
  crit,
  dalan_bankleft,
  dalan_bankright,
  elite,
  empire,
  energy,
  epic,
  evade,
  focus,
  firstorder,
  hardpoint,
  helmet_rebel,
  helmet_imperial,
  helmet_scum,
  hit,
  hull,
  ig88d_sloopleft,
  ig88d_sloopright,
  illicit,
  jam,
  kturn,
  missile,
  modification,
  obstacle_coreasteroid0,
  obstacle_coreasteroid1,
  obstacle_coreasteroid2,
  obstacle_coreasteroid3,
  obstacle_coreasteroid4,
  obstacle_coreasteroid5,
  obstacle_core2asteroid0,
  obstacle_core2asteroid1,
  obstacle_core2asteroid2,
  obstacle_core2asteroid3,
  obstacle_core2asteroid4,
  obstacle_core2asteroid5,
  obstacle_vt49decimatordebris0,
  obstacle_vt49decimatordebris1,
  obstacle_vt49decimatordebris2,
  obstacle_yt2400debris0,
  obstacle_yt2400debris1,
  obstacle_yt2400debris2,
  obstacle_coreasteroid0_outline,
  obstacle_coreasteroid1_outline,
  obstacle_coreasteroid2_outline,
  obstacle_coreasteroid3_outline,
  obstacle_coreasteroid4_outline,
  obstacle_coreasteroid5_outline,
  obstacle_core2asteroid0_outline,
  obstacle_core2asteroid1_outline,
  obstacle_core2asteroid2_outline,
  obstacle_core2asteroid3_outline,
  obstacle_core2asteroid4_outline,
  obstacle_core2asteroid5_outline,
  obstacle_vt49decimatordebris0_outline,
  obstacle_vt49decimatordebris1_outline,
  obstacle_vt49decimatordebris2_outline,
  obstacle_yt2400debris0_outline,
  obstacle_yt2400debris1_outline,
  obstacle_yt2400debris2_outline,
  overlay_180,
  overlay_frontback,
  overlay_turret,
  reload,
  rebel,
  rebel_outline,
  recover,
  reinforce,
  reversebankleft,
  reversebankright,
  reversestraight,
  rotatearc,
  salvagedastromech,
  scum,
  shield,
  slam,
  sloopleft,
  sloopright,
  squad_point_cost,
  stop,
  straight,
  system,
  targetlock,
  team,
  tech,
  title,
  token_cannotattack,
  token_cannotattack_outline,
  token_cloak,
  token_cloak_outline,
  token_crit,
  token_crit_outline,
  token_energy,
  token_energy_outline,
  token_evade,
  token_evade_outline,
  token_extramunitions,
  token_extramunitions_outline,
  token_focus,
  token_focus_outline,
  token_illicit,
  token_illicit_outline,
  token_ion,
  token_ion_outline,
  token_reinforce,
  token_reinforce_outline,
  token_shield,
  token_shield_outline,
  token_stress,
  token_stress_outline,
  token_targetlock,
  token_targetlock_outline,
  token_tractorbeam,
  token_tractorbeam_outline,
  torpedo,
  trollleft,
  trollright,
  turnleft,
  turnright,
  turret,
  unique,
  unique_outline,
  // added:
  blank
};




class IconGlyphNotFound : public std::runtime_error {
 public:
  IconGlyphNotFound(Ico i);
};

class IconGlyph {
 public:
  static IconGlyph   GetIconGlyph(Ico i);
  static std::string GetGlyph(Ico i);
  static std::string GetGlyph(std::string ctph);

  static IconGlyph   GetIconGlyph(Act a);
  static std::string GetGlyph(Act a);

  static IconGlyph   GetIconGlyph(Fac f);
  static std::string GetGlyph(Fac f);

  static IconGlyph   GetIconGlyph(SFa s);
  static std::string GetGlyph(SFa s);

  static IconGlyph   GetIconGlyph(Upg u);
  static std::string GetGlyph(Upg u);

  static IconGlyph   GetIconGlyphEx(Upg u);
  static std::string GetGlyphEx(Upg u);

  static IconGlyph   GetIconGlyph(AttD u);
  static std::string GetGlyph(AttD u);
  static IconGlyph   GetIconGlyph(DefD u);
  static std::string GetGlyph(DefD u);

  static IconGlyph   GetIconGlyph(Arc a);
  static std::string GetGlyph(Arc a);

  static IconGlyph   GetIconGlyph(Brn b);
  static std::string GetGlyph(Brn b);

  
  static std::vector<IconGlyph> GetAllIconGlyphs();

  Ico         GetType();
  std::string GetScssName();
  std::string GetCardTextPlaceholder();
  std::string GetGlyph();


 private:
  Ico         type;
  std::string scssName;
  std::string cardTextPlaceholder;
  std::string glyph;

  static Ico ActToIco(Act a);
  static Ico FacToIco(Fac f);
  static Ico SFaToIco(SFa s);
  static Ico UpgToIco(Upg u);
  static Ico ExToIco(Upg u);
  static Ico AttdToIco(AttD a);
  static Ico DefdToIco(DefD d);
  static Ico ArcToIco(Arc a);
  static Ico BrnToIco(Brn b);

  static std::vector<IconGlyph> iconGlyphs;

  IconGlyph(Ico i,
	    std::string scss,
	    std::string ctph,
	    std::string gly);
};

}
