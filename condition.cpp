#include "condition.h"

namespace libxwing {


std::vector<Condition> Condition::conditions = {
  { Cnd::FanaticalDevotion,     1, "fanaticaldevtion",      "Fanatical Devotion",          "When defending, you cannot spend focus tokens.  When attacking, if you spend a focus token to change all {FOCUS} results to {HIT} results, set aside the first {FOCUS} result that you change.  The set-aside {HIT} results cannot be canceled by defense dice, but the defender may cancel {CRIT} results before it.  During the end phase, remove this card." },
  { Cnd::IllShowYouTheDarkSide, 1, "illshowyouthedarkside", "I'll Show You the Dark Side", "When this card is assigned, if it is not already in play, the player who assigned it searches the Damage deck for 1 Damage card with the Pilot trait and may place it faceup on this card.  Then shuffle the damage deck.  When you suffer critical damage during an attack, you are instead dealt the chosen faceup Damage card.  When there is no Damage card on this card, remove it." },
  { Cnd::ADebtToPay,            1, "adebttopay",            "A Debt to Pay",               "When attacking a ship that has the \"A Score to Settle\" Upgrade card, you may change 1 {FOCUS} result to a {CRIT} result." },
  { Cnd::SuppressiveFire,       1, "suppressivefire",       "Suppressive Fire",            "When attacking a ship other than \"Captain Rex,\" roll 1 fewer attack die.  When you declare an attack targeting \"Captain Rex\" or when \"Captain Rex\" is destroyed, remove this card.  At the end of the Combat phase, if \"Captain Rex\" did not perform an attack this phase, remove this card." },
  { Cnd::Mimicked,              1, "mimicked",              "Mimicked",                    "\"Thweek\" is treated as having your pilot ability.  \"Thweek\" cannot apply a Condition card by using your pilot ability.  \"Thweek\" does not lose your pilot ability if you are destroyed." },
  { Cnd::Shadowed,              1, "shadowed",              "Shadowed",                    "\"Thweek\" is treated as having the pilot skill value you had after setup.  The pilot skill value of \"Thweek\" does not change if your pilot skill value changes or you are destroyed." },
  { Cnd::Harpooned,             0, "harpooned",             "Harpooned!",                  "When you are hit by an attack, if there is at least 1 uncancelled {CRIT} result, each other ship at Range 1 suffers 1 damage.  Then discard this card and receive 1 facedown Damage card.  When you are destroyed, each ship at Range 1 suffers 1 damage.  Action: Discard this card.  Then roll 1 attack die.  On a {HIT} or {CRIT} result, suffer 1 damage." },
  { Cnd::Rattled,               1, "rattled",               "Rattled",                     "When you suffer damage or critical damage from a bomb, you suffer 1 additional critical damage.  Then, remove this card.  Action: Roll 1 attack die.  On a {FOCUS} or {HIT} result, remove this card." },
  { Cnd::Scrambled,             1, "scrambled",             "Scrambled",                   "When attacking a ship at Range 1 that is equipped with the \"Targeting Scrambler\" Upgrade, you cannot modify your attack dice.  At the end of the combat phase, remove this card." },
  { Cnd::OptimizedPrototype,    1, "optimizedprototype",    "Optimized Prototype",         "Increase your shield value by 1.  Once per round, when performing a primary weapon attack, you may spend 1 die result to remove 1 shield from the defender.  After you perform a primary weapon attack, a friendly ship at Range 1-2 equpiied with the \"Director Krennic\" Upgrade card may acquire a target lock on the defender." },
};



ConditionNotFound::ConditionNotFound(Cnd c)           : runtime_error("Condition not found (enum " + std::to_string((int)c) + ")") { }
ConditionNotFound::ConditionNotFound(std::string xws) : runtime_error("Condition not found (" + xws + ")") { }



Condition::Condition(Cnd         t,
		     bool        u,
		     std::string x,
		     std::string n,
		     std::string txt)
  : type(t), isUnique(u), xws(x), name(n), text(txt) { }



Condition Condition::GetCondition(Cnd t) {
  for(Condition c : Condition::conditions) {
    if(c.GetType() == t) {
      return c;
    }
  }
  throw ConditionNotFound(t);
}

Condition Condition::GetCondition(std::string xws) {
  for(Condition c : Condition::conditions) {
    if(c.GetXws() == xws) {
      return c;
    }
  }
  throw ConditionNotFound(xws);
}

std::vector<Condition> Condition::GetAllConditions() {
  return Condition::conditions;
}



Cnd         Condition::GetType()  { return this->type; }
bool        Condition::IsUnique() { return this->isUnique; }
std::string Condition::GetXws()   { return this->xws;  }
std::string Condition::GetName()  { return this->name; }
std::string Condition::GetText()  { return this->text; }

}
