#include "dice.h"
#include <algorithm>

namespace libxwing {

// attack dic
AttackDieNotFound::AttackDieNotFound(AttD a)        : runtime_error("Attack Dice not found (enum " + std::to_string((int)a) + ")") { }
AttackDieNotFound::AttackDieNotFound(std::string t) : runtime_error("Attack Dice not found '" + t + "'") { }

std::vector<AttackDie> AttackDie::attackDie = { 
  {AttD::Blank, "Blank",        "Blank", "{BLANK}" },
  {AttD::Focus, "Focus",        "Focus", "{FOCUS}" },
  {AttD::Hit,   "Hit",          "Hit",   "{HIT}"   },
  {AttD::Crit,  "Critical Hit", "Crit",  "{CRIT}"  },
};

AttackDie AttackDie::GetAttackDie(AttD a) {
  for(AttackDie d : AttackDie::attackDie) {
    if(d.GetType() == a) {
      return d;
    }
  }
  throw AttackDieNotFound(a);
}

AttackDie AttackDie::GetAttackDie(std::string t) {
  for(AttackDie d : AttackDie::attackDie) {
    if(d.GetText() == t) {
      return d;
    }
  }
  throw AttackDieNotFound(t);
}

AttackDie::AttackDie(AttD a,
                      std::string n,
                      std::string s,
                      std::string t)
  : type(a), name(n), shortName(s), text(t) { }

AttD        AttackDie::GetType()      { return this->type; }
std::string AttackDie::GetName()      { return this->name; }
std::string AttackDie::GetShortName() { return this->shortName; }
std::string AttackDie::GetText()      { return this->text; }

AttackDice::AttackDice() { }

AttackDice::AttackDice(std::vector<AttD> d)
  : dice(d) { }

void AttackDice::Add(AttD a) {
  this->dice.push_back(a);
}

std::vector<AttackDie> AttackDice::GetDice() {
  std::vector<AttackDie> ret;
  for(auto d : this->dice) {
    ret.push_back(AttackDie::GetAttackDie(d));
  }
  return ret;
}

std::vector<AttackDie> AttackDice::GetSortedDice() {
  std::vector<AttackDie> ret;
  std::vector<AttD> sorted = this->dice;
  std::sort(sorted.begin(), sorted.end(), [](AttD i, AttD j){ return i>j; });
  for(auto d : sorted) {
    ret.push_back(AttackDie::GetAttackDie(d));
  }
  return ret;
}

void AttackDice::Focus() {
  printf("ping\n");
  for(AttD &a : this->dice) {
    if(a == AttD::Focus) { a = AttD::Hit; }
  }
}



// defense dice
DefenseDieNotFound::DefenseDieNotFound(DefD d)        : runtime_error("Defense Dice not found (enum " + std::to_string((int)d) + ")") { }
DefenseDieNotFound::DefenseDieNotFound(std::string t) : runtime_error("Defense Dice not found '" + t + "'") { }

std::vector<DefenseDie> DefenseDie::defenseDie = { 
  {DefD::Blank, "Blank", "Blank", "{BLANK}" },
  {DefD::Focus, "Focus", "Focus", "{FOCUS}" },
  {DefD::Evade, "Evade", "Evade", "{EVADE}" },
};

DefenseDie DefenseDie::GetDefenseDie(DefD e) {
  for(DefenseDie d : DefenseDie::defenseDie) {
    if(d.GetType() == e) {
      return d;
    }
  }
  throw DefenseDieNotFound(e);
}

DefenseDie DefenseDie::GetDefenseDie(std::string t) {
  for(DefenseDie d : DefenseDie::defenseDie) {
    if(d.GetText() == t) {
      return d;
    }
  }
  throw DefenseDieNotFound(t);
}

DefenseDie::DefenseDie(DefD e,
                       std::string n,
                       std::string s,
                       std::string t)
  : type(e), name(n), shortName(s), text(t) { }

DefD        DefenseDie::GetType()      { return this->type; }
std::string DefenseDie::GetName()      { return this->name; }
std::string DefenseDie::GetShortName() { return this->shortName; }
std::string DefenseDie::GetText()      { return this->text; }

DefenseDice::DefenseDice() { }

DefenseDice::DefenseDice(std::vector<DefD> d)
  : dice(d) { }

void DefenseDice::Add(DefD a) {
  this->dice.push_back(a);
}

std::vector<DefenseDie> DefenseDice::GetDice() {
  std::vector<DefenseDie> ret;
  for(auto d : this->dice) {
    ret.push_back(DefenseDie::GetDefenseDie(d));
  }
  return ret;
}

std::vector<DefenseDie> DefenseDice::GetSortedDice() {
  std::vector<DefenseDie> ret;
  std::vector<DefD> sorted = this->dice;
  std::sort(sorted.begin(), sorted.end(), [](DefD i, DefD j){ return i>j; });
  for(auto d : sorted) {
    ret.push_back(DefenseDie::GetDefenseDie(d));
  }
  return ret;
}

void DefenseDice::Focus() {
  for(DefD &d : this->dice) {
    if(d == DefD::Focus) { d = DefD::Evade; }
  }
}

}
