#include "faction.h"

namespace libxwing {

// Faction
std::vector<Faction> Faction::factions = {
  { Fac::None,     "",         ""         },
  { Fac::Rebel,    "rebel",    "Rebel"    },
  { Fac::Imperial, "imperial", "Imperial" },
  { Fac::Scum,     "scum",     "Scum"     },
  { Fac::All,      "",         "All"      },
};

Fac operator|(Fac a, Fac b) {
  return static_cast<Fac>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

Fac operator&(Fac a, Fac b) {
  return static_cast<Fac>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

FactionNotFound::FactionNotFound(Fac f)           : runtime_error("Faction not found (enum " + std::to_string((int)f) + ")") { }
FactionNotFound::FactionNotFound(std::string xws) : runtime_error("Faction not found (" + xws + ")") { }

Faction Faction::GetFaction(Fac typ) {
  for(Faction f : Faction::factions) {
    if(f.GetType() == typ) {
      return f;
    }
  }
  throw FactionNotFound(typ);
}

Faction Faction::GetFaction(std::string xws) {
  for(Faction f : Faction::factions) {
    if(f.GetXws() == xws) {
      return f;
    }
  }
  throw FactionNotFound(xws);
}

Fac         Faction::GetType()      const { return this->type; }
std::string Faction::GetXws()       const { return this->xws; }
std::string Faction::GetName()      const { return this->name; }

Faction::Faction(Fac         t,
                 std::string x,
                 std::string n)
  : type(t), xws(x), name(n) { }



// SubFaction
std::vector<SubFaction> SubFaction::subFactions = {
  { SFa::None,           Fac::None,     "",               ""                 },
  { SFa::RebelAlliance,  Fac::Rebel,    "rebelalliance",  "Rebel Alliance"   },
  { SFa::Resistance,     Fac::Rebel,    "resistance",     "Resistance"       },
  { SFa::GalacticEmpire, Fac::Imperial, "galacticempire", "Galactic Empire"  },
  { SFa::FirstOrder,     Fac::Imperial, "firstorder",     "First Order"      },
  { SFa::ScumAndVillany, Fac::Scum,     "scumandvillany", "Scum and Villany" },
  { SFa::All,            Fac::All,      "",               ""                 }
};

SFa operator|(SFa a, SFa b) {
  return static_cast<SFa>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

SFa operator&(SFa a, SFa b) {
  return static_cast<SFa>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

SubFactionNotFound::SubFactionNotFound(SFa s)           : runtime_error("SubFaction not found (enum " + std::to_string((int)s) + ")") { }
SubFactionNotFound::SubFactionNotFound(std::string xws) : runtime_error("SubFaction not found (" + xws + ")") { }

SubFaction SubFaction::GetSubFaction(SFa typ) {
  for(SubFaction s : SubFaction::subFactions) {
    if(s.GetType() == typ) {
      return s;
    }
  }
  throw SubFactionNotFound(typ);
}

SubFaction SubFaction::GetSubFaction(std::string xws) {
  for(SubFaction s : SubFaction::subFactions) {
    if(s.GetXws() == xws) {
      return s;
    }
  }
  throw SubFactionNotFound(xws);
}

SFa         SubFaction::GetType()       const { return this->type; }
Fac         SubFaction::GetParentType() const { return this->parentType; }
std::string SubFaction::GetXws()        const { return this->xws;  }
std::string SubFaction::GetName()       const { return this->name; }

SubFaction::SubFaction(SFa         t,
		       Fac         p,
		       std::string x,
		       std::string n)
  : type(t), parentType(p), xws(x), name(n) { }

}
