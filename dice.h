#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

// attack dice
enum class AttD {
  Blank = 0,
  Focus = 1,
  Hit   = 2,
  Crit  = 3
};

class AttackDieNotFound : public std::runtime_error {
 public:
  AttackDieNotFound(AttD a);
  AttackDieNotFound(std::string t);
};

class AttackDie {
 public:
  static AttackDie GetAttackDie(AttD a);
  static AttackDie GetAttackDie(std::string t);

  AttD GetType();
  std::string GetName();
  std::string GetShortName();
  std::string GetText();

 private:
  AttD type;
  std::string name;
  std::string shortName;
  std::string text;

  static std::vector<AttackDie> attackDie;

  AttackDie(AttD a,
            std::string n,
            std::string s,
            std::string t);
};

class AttackDice {
 public:
  AttackDice();
  AttackDice(std::vector<AttD> dice);

  void Add(AttD d);
  std::vector<AttackDie> GetDice();
  std::vector<AttackDie> GetSortedDice();
  void Focus();
  
 private:
  std::vector<AttD> dice;
};



// defense dice
enum class DefD {
  Blank = 0,
  Focus = 1,
  Evade = 4
};

class DefenseDieNotFound : public std::runtime_error {
 public:
  DefenseDieNotFound(DefD d);
  DefenseDieNotFound(std::string t);
};

class DefenseDie {
 public:
  static DefenseDie GetDefenseDie(DefD a);
  static DefenseDie GetDefenseDie(std::string t);

  DefD GetType();
  std::string GetName();
  std::string GetShortName();
  std::string GetText();

 private:
  DefD type;
  std::string name;
  std::string shortName;
  std::string text;

  static std::vector<DefenseDie> defenseDie;

  DefenseDie(DefD e,
             std::string n,
             std::string s,
             std::string t);
};



class DefenseDice {
 public:
  DefenseDice();
  DefenseDice(std::vector<DefD> dice);

  void Add(DefD d);
  std::vector<DefenseDie> GetDice();
  std::vector<DefenseDie> GetSortedDice();
  void Focus();

 private:
  std::vector<DefD> dice;
};

}
