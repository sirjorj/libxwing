#pragma once
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

// Base Size
enum class BSz {
  None  = 0,
  Small = 1,
  Large = 2,
  Huge  = 4,
  All   = 7
};

BSz operator|(BSz a, BSz b);
BSz operator&(BSz a, BSz b);

class BaseSizeNotFound : public std::runtime_error {
 public:
  BaseSizeNotFound(BSz b);
};

class BaseSize {
 public:
  static BaseSize GetBaseSize(BSz s);
  BSz         GetType()      const;
  std::string GetName()      const;

 private:
  BSz type;
  std::string name;

  static std::vector<BaseSize> baseSizes;

  BaseSize(BSz         t,
           std::string n);
};



// Actions
enum class Act : uint32_t {
  None       = 0x0000,
  Focus      = 0x0001,
  TargetLock = 0x0002,
  BarrelRoll = 0x0004,
  Boost      = 0x0008,
  Evade      = 0x0010,
  Recover    = 0x0020,
  Reinforce  = 0x0040,
  Coordinate = 0x0080,
  Jam        = 0x0100,
  Cloak      = 0x0200,
  SLAM       = 0x0400,
  RotateArc  = 0x0800,
  Reload     = 0x1000
};

Act operator|(Act a, Act b);
Act operator&(Act a, Act b);
Act operator+(Act a, Act b);
Act operator-(Act a, Act b);

class ActionNotFound : public std::runtime_error {
 public:
  ActionNotFound(Act a);
};

class Action {
 public:
  static Action GetAction(Act s);
  Act         GetType()      const;
  std::string GetName()      const;
  std::string GetShortName() const;

 private:
  Act type;
  std::string name;
  std::string shortName;

  static std::vector<Action> actions;

  Action(Act         t,
         std::string n,
         std::string s);
};

void ForEachAction(Act actions, std::function<void(Act)>f);



// misc helpers
std::string ToLower(std::string s);

}
