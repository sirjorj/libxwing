#include "upgrade.h"
#include "shared.h"
#include <algorithm>

namespace libxwing {

// *** UpgradeType ***
UpgradeTypeNotFound::UpgradeTypeNotFound(Upg u)           : runtime_error("UpgradeType not found: (enum " + std::to_string((int)u) + ")") { }
UpgradeTypeNotFound::UpgradeTypeNotFound(std::string xws) : runtime_error("UpgradeType not found: '" + xws + "'") { }

std::vector<UpgradeType> UpgradeType::upgradeTypes = {
  { Upg::Elite,             "ept",       "Elite Pilot Talent", "EPT",   },
  { Upg::Astromech,         "amd",       "Astromech Droid",    "AMD",   },
  { Upg::Torpedo,           "torpedo",   "Torpedo",            "Trp",   },
  { Upg::Turret,            "turret",    "Turret",             "Tur",   },
  { Upg::Missile,           "missile",   "Missile",            "Misl",  },
  { Upg::Crew,              "crew",      "Crew",               "Crew",  },
  { Upg::Title,             "title",     "Title",              "Title", },
  { Upg::Modification,      "mod",       "Modification",       "Mod" ,  },
  { Upg::Cannon,            "cannon",    "Cannon",             "Cann",  },
  { Upg::Bomb,              "bomb",      "Bomb",               "Bomb",  },
  { Upg::Cargo,             "cargo",     "Cargo",              "Cargo", },
  { Upg::System,            "system",    "System",             "Sys",   },
  { Upg::Hardpoint,         "hardpoint", "Hardpoint",          "Hrdpt", },
  { Upg::Team,              "team",      "Team",               "Team",  },
  { Upg::SalvagedAstromech, "samd",      "Salvaged Astromech", "SAMD",  },
  { Upg::Illicit,           "illicit",   "Illicit",            "Illi",  },
  { Upg::Tech,              "tech",      "Tech",               "Tech",  },
};

UpgradeType UpgradeType::GetUpgradeType(Upg typ) {
  for(UpgradeType u : UpgradeType::upgradeTypes) {
    if(u.GetType() == typ) {
      return u;
    }
  }
  throw UpgradeTypeNotFound(typ);
}

UpgradeType UpgradeType::GetUpgradeType(std::string xws) {
  for(UpgradeType u : UpgradeType::upgradeTypes) {
    if(u.GetXws() == xws) {
      return u;
    }
  }
  throw UpgradeTypeNotFound(xws);
}

Upg         UpgradeType::GetType()       const { return this->type; }
std::string UpgradeType::GetXws()        const { return this->xws; }
std::string UpgradeType::GetName()       const { return this->name; }
std::string UpgradeType::GetShortName()  const { return this->shortName; }

UpgradeType::UpgradeType(Upg         t,
                         std::string x,
                         std::string n,
                         std::string s)
  : type(t), xws(x), name(n), shortName(s) { }

Upg operator|(Upg a, Upg b) {
  return static_cast<Upg>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

Upg operator&(Upg a, Upg b) {
  return static_cast<Upg>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}



// *** Upgrade ***
UpgradeNotFound::UpgradeNotFound(std::string cat, std::string name) : runtime_error("Upgrade not found: '" + cat + "/" + name + "'") { }

// factories
Upgrade Upgrade::GetUpgrade(std::string category, std::string name) {
  for(Upgrade &us : Upgrade::upgrades) {
    if((us.GetType().GetXws() == category) && (us.GetXws() == name)) {
      return us;
    }
  }
  throw UpgradeNotFound(category, name);
}

std::vector<Upgrade> Upgrade::FindUpgrade(std::string u, std::vector<Upgrade> src) {
  std::vector<Upgrade> ret;
  std::string ss = ToLower(u); // searchString
  for(Upgrade &us : src) {
    if((ToLower(us.GetName()).find(ss)      != std::string::npos) ||
       (ToLower(us.GetShortName()).find(ss) != std::string::npos) ||
       (ToLower(us.GetXws()).find(ss)       != std::string::npos)
       ) {
      ret.push_back(us);
    }
  }
  return ret;

}

std::vector<Upgrade> Upgrade::GetAllUpgrades() { return Upgrade::upgrades; }

// maintenance
void Upgrade::SanityCheck() {
  typedef std::tuple<std::string, std::string> Entry;
  std::vector<Entry> entries;
  std::vector<Entry> dupes;
  int counter=0;
  printf("Checking Upgrades");
  for(Upgrade u : upgrades) {
    counter++;
    Entry e = Entry{u.GetType().GetXws(), u.GetXws() };
    if(std::find(entries.begin(), entries.end(), e) == entries.end()) {
      printf("."); fflush(stdout);
    } else {
      dupes.push_back(e);
      printf("X"); fflush(stdout);
    }
    entries.push_back(e);
  }
  printf("DONE\n");
  printf("Upgrades: %zu\n", entries.size());
  printf("Dupes: %zu\n", dupes.size());
  if(dupes.size()) {
    for(Entry e : dupes) {
      printf("  %s - %s\n", std::get<0>(e).c_str(), std::get<1>(e).c_str());
    }
  }
}

// info
std::string Upgrade::GetXws()             const { return this->nameXws; }
std::string Upgrade::GetName()            const { return this->sides[this->curSide].name; }
std::string Upgrade::GetShortName()       const { return this->sides[this->curSide].nameShort; }
UpgradeType Upgrade::GetType()            const { return UpgradeType::GetUpgradeType(this->type); }
std::string Upgrade::GetRestrictionText() const { return this->restrictionText; }
std::string Upgrade::GetText()            const { return this->sides[this->curSide].text; }
bool        Upgrade::IsUnique()           const { return (this->perSquad == 1); }
bool        Upgrade::IsLimited()          const { return (this->perPilot == 1); }
uint8_t     Upgrade::GetPerSquad()        const { return this->perSquad; }
uint8_t     Upgrade::GetPerPilot()        const { return this->perPilot; }
bool        Upgrade::IsUnreleased()       const { return this->isUnreleased; }
std::experimental::optional<AttackStats> Upgrade::GetAttackStats()    const {return this->sides[this->curSide].attackStats; }
std::experimental::optional<uint8_t>     Upgrade::GetEnergyLimit()    const {return this->sides[this->curSide].energyLimit; }
std::experimental::optional<int8_t>      Upgrade::GetEnergyModifier() const {return this->sides[this->curSide].energyModifier; }

// stats
std::vector<Upg> Upgrade::GetReqSlots()         const { return this->slots; }
int8_t           Upgrade::GetCost()             const { return this->cost; }
StatModifiers    Upgrade::GetStatModifier()     const { return this->sides[this->curSide].statModifier; }
ManModifiers     Upgrade::GetManModifier()      const { return this->sides[this->curSide].manModifier; }
RestrictionCheck Upgrade::GetRestrictionCheck() const { return this->restrictionCheck; }
uint8_t          Upgrade::GetExtras()           const { return this->extras; }
void             Upgrade::SetExtras(uint8_t x)        { this->extras = x; }
void             Upgrade::ExtraUp()                   { if(this->extras < 255) { this->extras++; } }
void             Upgrade::ExtraDn()                   { if(this->extras >   0) { this->extras--; } }

// state
bool Upgrade::IsEnabled() const    { return this->isEnabled; }
void Upgrade::Enable()             { this->isEnabled = true; }
void Upgrade::Disable()            { this->isEnabled = false; }
bool Upgrade::IsDualSided() const  { return this->sides.size() > 1; }
void Upgrade::Flip() {
  if(this->sides.size() > 1) {
    this->curSide = (this->curSide+1) % 2;
  }
}

Prepper Upgrade::GetPrepper() { return this->prepper; }

// constructor
Upgrade::Upgrade(std::string      nx,
                 Upg              typ,
                 std::vector<Upg> slts,
                 int8_t           cst,
		 uint8_t          psq,
		 uint8_t          ppi,
                 RestrictionCheck rc,
		 std::string      rt,
		 Prepper          p,
                 UpgradeSide      s1,
                 bool             iu)
  : isUnreleased(iu), isEnabled(true),nameXws(nx), type(typ), slots(slts), cost(cst), perSquad(psq),
    perPilot(ppi), restrictionCheck(rc), restrictionText(rt), prepper(p), extras(0), curSide(0), sides({{s1}}) { }

Upgrade::Upgrade(std::string      nx,
                 Upg              typ,
                 std::vector<Upg> slts,
                 int8_t           cst,
		 uint8_t          psq,
		 uint8_t          ppi,
                 RestrictionCheck rc,
		 std::string      rt,
		 Prepper          p,
                 UpgradeSide      s1,
                 UpgradeSide      s2,
                 bool             iu)
  : isUnreleased(iu), isEnabled(true),nameXws(nx), type(typ), slots(slts), cost(cst), perSquad(psq),
    perPilot(ppi), restrictionCheck(rc), restrictionText(rt), prepper(p), extras(0), curSide(0), sides({{s1, s2}}) { }

}
