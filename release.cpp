#include "release.h"

namespace libxwing {

ReleaseNotFound::ReleaseNotFound(std::string sku) : runtime_error("Release not found (" + sku + ")") { }

Release::Release(std::string                          sku,
                 std::string                          isbn,
                 std::string                          nam,
                 RelGroup                             grp,
                 RelDate                              ad,
                 RelDate                              rd,
                 RelUrls                              url,
                 std::vector<RelShip>                 shps,
                 std::vector<RelPilot>                plts,
                 std::vector<RelUpgrade>              upgs,
                 std::vector<Cnd>                     cond,
                 std::vector<std::shared_ptr<Tokens>> tok,
                 bool                                 iu)
  : sku(sku), isbn(isbn), name(nam), group(grp), announceDate(ad), releaseDate(rd), urls(url), ships(shps), pilots(plts), upgrades(upgs), conditions(cond), tokens(tok), isUnreleased(iu) { }



std::vector<Release> Release::GetPilot(std::string plt, Fac fac, Shp shp) {
  std::vector<Release> ret;
  for(Release r : Release::releases) {
    for(RelPilot p : r.pilots) {
      if((p.name == plt) && (p.faction == fac) && (p.ship == shp)) {
        ret.push_back(r);
        break;
      }
    }
  }
  return ret;
}



std::vector<Release> Release::GetUpgrade(Upg type, std::string name) {
  std::vector<Release> ret;
  for(Release r : Release::releases) {
    for(RelUpgrade u : r.upgrades) {
      if((u.type == type) && (u.name == name)) {
        ret.push_back(r);
        break;
      }
    }
  }
  return ret;
}



Release Release::GetRelease(std::string sku) {
  for(Release r : Release::releases) {
    if(r.GetSku() == sku) {
      return r;
    }
  }
  throw ReleaseNotFound(sku);
}

std::vector<Release> Release::GetAllReleases() { return Release::releases; }



bool                     Release::IsUnreleased()       { return this->isUnreleased; }
std::string              Release::GetSku()             { return this->sku;  }
std::string              Release::GetIsbn()            { return this->isbn; }
std::string              Release::GetName()            { return this->name; }
RelGroup                 Release::GetGroup()           { return this->group; }
RelDate                  Release::GetAnnounceDate()    { return this->announceDate; }
RelDate                  Release::GetReleaseDate()     { return this->releaseDate; }
std::string              Release::GetAnnouncementUrl() { return this->urls.announcement; }
std::vector<std::string> Release::GetPreviewUrls()     { return this->urls.previews; }
std::string              Release::GetReleaseUrl()      { return this->urls.release; }
std::vector<RelShip>     Release::GetShips()           { return this->ships; }
std::vector<RelPilot>    Release::GetPilots()          { return this->pilots; }
std::vector<RelUpgrade>  Release::GetUpgrades()        { return this->upgrades; }
std::vector<Cnd>         Release::GetConditions()      { return this->conditions; }
TokenCollection          Release::GetTokens()          { return this->tokens; }

}
