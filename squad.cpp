#include "squad.h"
#include "./json/json.h"
#include <algorithm>
#include <fstream>
#include <iostream>

namespace libxwing {

VendorSpecificNotSet::VendorSpecificNotSet() : runtime_error("VendorSpecific not set") { }



VendorSpecific::VendorSpecific(std::string IMPLEMENTATION_NAME)
  : implementation_name(IMPLEMENTATION_NAME) { }

std::string VendorSpecific::GetImplementationName() const { return this->implementation_name; }

void VendorSpecific::SetUrl(std::string v) { this->url = v; }
std::string VendorSpecific::GetUrl() const { return this->url; }

void VendorSpecific::SetBuilder(std::string v) { this->builder = v; }
std::string VendorSpecific::GetBuilder() const { return this->builder; }

void VendorSpecific::SetBuilderUrl(std::string v) { this->builderUrl = v; }
std::string VendorSpecific::GetBuilderUrl() const { return this->builderUrl; }



SquadNotFound::SquadNotFound() : runtime_error("Squad not found") { }
SquadNotFound::SquadNotFound(std::string f) : runtime_error("Squad not found: '" + f + "'") { }



Squad::Squad(Fac f) : faction(f) { }

Squad::Squad(std::string xwsFile) {
  std::ifstream xws(xwsFile, std::ifstream::binary);
  if(!xws.good()) { throw SquadNotFound(xwsFile); }
  this->Load(xws);
}

Squad::Squad(std::istream &xwsStream) {
  this->Load(xwsStream);
}



void Squad::Load(std::istream &xws) {
  Json::Value root;
  xws >> root;

  this->name        = root.get("name", "").asString();
  this->description = root.get("listdesc", "").asString();
  this->faction     = Faction::GetFaction(root.get("faction", "").asString()).GetType();

  for(int i=0; i<root["pilots"].size(); i++) {
    Json::Value jsonPilot = root["pilots"][i];

    std::string pilotname = jsonPilot.get("name", "").asString();
    std::string shipname = jsonPilot.get("ship", "").asString();
    Pilot p = Pilot::GetPilot(pilotname, this->GetFaction().GetXws(), shipname);

    Json::Value jsu = jsonPilot["upgrades"];
    for(Json::ValueIterator iCat = jsu.begin(); iCat != jsu.end() ; iCat++ ) {
      std::string category = iCat.key().asString().c_str();
      Json::Value jsCat = jsu[category];
      for(int i=0; i<jsCat.size(); i++) {
        std::string upgrade = jsCat[i].asString();
        Upgrade u = Upgrade::GetUpgrade(category, upgrade);
        p.ApplyUpgrade(u);
      }
    }
    this->pilots.push_back(p);
  }

  // prep the cards (preload tokens for EM, Jabba, etc...)
  for(Pilot& p : this->GetPilots()) {
    for(Upgrade u : p.GetAppliedUpgrades()) {
      u.GetPrepper()(p, this->GetPilots());
    }
  }
}



void        Squad::SetName(std::string n) { this->name = n; }
std::string Squad::GetName()        const { return this->name; }

void        Squad::SetDescription(std::string d) { this->description = d; }
std::string Squad::GetDescription()        const { return this->description; }

Faction Squad::GetFaction()     const { return Faction::GetFaction(this->faction); }

void                Squad::AddPilot(Pilot p) { this->pilots.push_back(p); }
std::vector<Pilot>& Squad::GetPilots()       { return this->pilots; }

uint16_t Squad::GetCost() {
  uint16_t cost=0;
  for(auto p : this->GetPilots()) {
    cost += p.GetModCost();
  }
  return cost;
}

void Squad::AddVendorSpecific(std::string application) {
  this->vendorSpecific = VendorSpecific(application);
}
void Squad::RemoveVendorSpecific() {
  this->vendorSpecific = std::experimental::nullopt;
}
VendorSpecific& Squad::GetVendorSpecific() {
  if(this->vendorSpecific) { return *this->vendorSpecific; }
  else { throw VendorSpecificNotSet(); }
}

std::vector<std::string> Squad::Verify() {
  std::vector<std::string> ret;
  int cost = 0;
  std::map<std::string, std::pair<uint8_t, uint8_t>> perSquads;
  for(Pilot &p : this->GetPilots()) {
    cost += p.GetModCost();

    // check perSquads (uniques)
    if(p.GetPerSquad() > 0) {
      std::string n = p.GetName();
      if(perSquads.find(n) == perSquads.end()) {
	perSquads[n] = {0, p.GetPerSquad() };
      }
      if(++perSquads[n].first > p.GetPerSquad()) {
	ret.push_back("There are " + std::to_string(perSquads[n].first) + " " + n + "card in this squad but the limit is " + std::to_string(perSquads[n].second));
      }
    }

    // check faction
    if(!(p.GetFaction().GetType() <= this->GetFaction().GetType())) {
      ret.push_back("Squad is " + this->GetFaction().GetName() + " but pilot '" + p.GetName() + "' is " + p.GetFaction().GetName());
    }

    // check upgrades
    std::map<std::string, std::pair<uint8_t, uint8_t>> perPilots;
    std::vector<Upg> openSlots = p.GetModPossibleUpgrades();
    for(Upgrade &u : p.GetAppliedUpgrades()) {

      // check perSquads (uniques)
      if(u.GetPerSquad() > 0) {
	std::string n = u.GetName();
	if(perSquads.find(n) == perSquads.end()) {
	  perSquads[n] = {0, u.GetPerSquad() };
	}
	if(++perSquads[n].first > perSquads[n].second) {
	  ret.push_back("There are " + std::to_string(perSquads[n].first) + " " + n + " cards in this squad but the limit is " + std::to_string(perSquads[n].second));
	}
      }

      // check perPilots (limited)
      if(u.GetPerPilot() > 0) {
	std::string n = u.GetName();
	if(perPilots.find(n) == perPilots.end()) {
	  perPilots[n] = {0, u.GetPerPilot() };
	}
	if(++perPilots[n].first > perPilots[n].second) {
	  ret.push_back(p.GetName() + " contains " + std::to_string(perPilots[n].first) + " " + n + "upgrades, which exceeds the limit of " + std::to_string(perPilots[n].second) + "\n");
	}
      }

      // have slot
      if(std::find(openSlots.begin(), openSlots.end(), u.GetType().GetType()) == openSlots.end()) {
        ret.push_back("No " + u.GetType().GetName() + " slot for upgrade '" + u.GetName() + "'\n");
      } else {
	auto it = std::find(openSlots.rbegin(), openSlots.rend(), u.GetType().GetType());
	if(it != openSlots.rend()) {
	  openSlots.erase(std::next(it).base());
	}
      }

      // check upgrade restrictions
      auto errs = u.GetRestrictionCheck()(p,this->GetPilots());
      for(std::string e : errs) {
        ret.push_back(e);
      }

    }
  }

  // check cost
  if(cost > 100) {
    ret.push_back("Squad cost is " + std::to_string(cost));
  }

  // no epic support at this time
  for(Pilot p : this->GetPilots()) {
    if(p.GetBaseSize().GetType() == BSz::Huge) {
      ret.push_back("Huge ships (" + p.GetName()+ ") are not currently supported");
    }
  }

  return ret;
}



bool Squad::Save(std::string xwsFile) {
  std::ofstream xws(xwsFile, std::ifstream::binary);
  return this->Save(xws);
  
}

bool Squad::Save(std::ostream &xwsStream) {
  int pc=0;
  Json::Value pilots;
  for(Pilot p : this->GetPilots()) {
    pilots[pc]["name"] = p.GetXws();
    pilots[pc]["ship"] = p.GetShip().GetXws();
    pilots[pc]["points"] = p.GetModCost();
    std::map<Upg, std::vector<Upgrade>> upgs = p.GetOrganizedUpgrades();
    Json::Value upgrades;
    for(std::pair<Upg, std::vector<Upgrade>> up : upgs) {
      int uc=0;
      for(Upgrade u : up.second) {
	upgrades[UpgradeType::GetUpgradeType(up.first).GetXws()][uc] = u.GetXws();
	uc++;
      }
    }
    pilots[pc]["upgrades"] = upgrades;
    pc++;
  }

  Json::Value root;
  root["name"] = this->GetName();
  root["description"] = this->GetDescription();
  root["faction"] = this->GetFaction().GetXws();
  root["pilots"] = pilots;
  root["points"] = this->GetCost();
  if(this->vendorSpecific) {
    Json::Value vs;
    if(this->GetVendorSpecific().GetUrl() != "")        { vs["url"]        = this->GetVendorSpecific().GetUrl(); }
    if(this->GetVendorSpecific().GetBuilder() != "")    { vs["builder"]    = this->GetVendorSpecific().GetBuilder(); }
    if(this->GetVendorSpecific().GetBuilderUrl() != "") { vs["builderurl"] = this->GetVendorSpecific().GetBuilderUrl(); }
    root["vendor"][this->GetVendorSpecific().GetImplementationName()] = vs;
  }
  root["version"] = "1.0.0";

  try{
    xwsStream << root;
  } catch(...) {
    return false;
  }
  return true;
}

}
