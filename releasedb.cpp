#include "release.h"

namespace libxwing {

// ToKen
#define TK(t)   std::make_shared<Tokens>(t,1)
// TokenS
#define TS(t,c) std::make_shared<Tokens>(t,c)
// TargetLock
#define TL(a,b) std::make_shared<TLTokens>(a,b)
// ID
#define ID(n)   std::make_shared<IDTokens>(n)

// https://www.fantasyflightgames.com/en/products/x-wing/
std::vector<Release> Release::releases = {

// [Info]
// SKU  - product SKU
// ISBN - product ISBN
// Name - expansion name
// Type - expansion type (wave#, coreset, aces, epic)

// [Dates] (year, month, date)
// Announced - the date this expansion was announced
// Released  - the date this expansion was released

// [URLs]
// AnnouncementURL - url to expansion announcement article
// PreviewURLS     - url(s) to expansion preview article(s)
// ReleaseURL      - url to expansion release article

// [Items]

// [Ships] (ships included in the expansion)
// Ship - the ship
// Desc - a description (for alternate paint jobs)

// [Pilots] (pilots included in the expansion)
// XwsName - pilot name per xws format
// Faction - pilot faction
// Ship    - pilot ship

// [Upgrades]
// Type    - upgrade type
// XwsName - upgrade name per xws format

// [Conditions] (condition cards included in the expansion)
// XwsName - xws-formatted name of the condition card

// [Tokens] (tokens included in the expansion)
// TK(type)        - single token
// TS(type, count) - multiple tokens
// ID(num)         - id token set (3 of number 'num')
// TL(a,b)         - target lock set (red and blue with letters 'a' on one side and 'b' on the other)

//{ SKU,     ISBN,            Name,                                           Type,               Announced,    Released,
//                                                                                                { AnnouncementURL,
//                                                                                                 {PreviewURLs},
//                                                                                                  ReleaseURL},
//                                                                                                {{Ship, Desc}},
//                                                                                                 {{XwsName, Faction, Ship}},
//                                                                                                 {{Type, XwsName}},
//                                                                                                 {XwsName},
//                                                                                                 {TK(type), TS(type,4), ID(1), TL("A","B")}
//                                                                                                },

  { "SWX01", "9781616613761", "X-Wing Miniatures Core Set",                   RelGroup::CoreSets, {2011, 8, 2}, {2012, 9,14},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2011/8/2/i-have-you-now/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2012/7/9/iconic-starfighters-and-dramatic-dogfights/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2012/7/23/starfighters-droids-rookies-and-legends/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2012/9/14/x-wing-tm-is-now-available/"},
                                                                                                  {{Shp::XWing, ""}, {Shp::TIEFighter, ""}, {Shp::TIEFighter, ""}},
                                                                                                   {{"lukeskywalker", Fac::Rebel, Shp::XWing}, {"biggsdarklighter", Fac::Rebel, Shp::XWing}, {"redsquadronpilot", Fac::Rebel, Shp::XWing}, {"rookiepilot", Fac::Rebel, Shp::XWing}, {"maulermithel", Fac::Imperial, Shp::TIEFighter}, {"darkcurse", Fac::Imperial, Shp::TIEFighter}, {"nightbeast", Fac::Imperial, Shp::TIEFighter}, {"blacksquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"blacksquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"obsidiansquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"obsidiansquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"academypilot", Fac::Imperial, Shp::TIEFighter}, {"academypilot", Fac::Imperial, Shp::TIEFighter}},
                                                                                                   {{Upg::Elite, "determination"}, {Upg::Elite, "marksmanship"}, {Upg::Astromech, "r2d2"}, {Upg::Astromech, "r2f2"}, {Upg::Torpedo, "protontorpedoes"}},
                                                                                                   {},
                                                                                                   {TS(Tok::Crit,3), TS(Tok::Evade,4), TS(Tok::Focus,3), ID(1), ID(2), ID(3), ID(4), ID(5), ID(6), ID(7), ID(8), ID(9), TS(Tok::Shield,2), TS(Tok::Stress,3), TL("A","B"), TL("C","D"), TL("E","F"), TL("G","H"), TL("I","J"), TL("K","L"), TS(Tok::Asteroid,6), TS(Tok::Satellite,4), TK(Tok::SenatorsShuttle), TS(Tok::Tracking,8)}
                                                                                                  },

  { "SWX02", "9781616613778", "X-Wing Expansion Pack",                        RelGroup::Wave1,    {2012, 4,17}, {2012, 9,14},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/4/17/prepare-to-launch-the-starfighters/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2012/7/30/stay-on-target/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2012/9/14/x-wing-tm-is-now-available/"},
                                                                                                  {{Shp::XWing, ""}},
                                                                                                   {{"wedgeantilles", Fac::Rebel, Shp::XWing}, {"garvendreis", Fac::Rebel, Shp::XWing}, {"redsquadronpilot", Fac::Rebel, Shp::XWing}, {"rookiepilot", Fac::Rebel, Shp::XWing}},
                                                                                                   {{Upg::Elite, "experthandling"}, {	Upg::Elite, "marksmanship"}, {Upg::Astromech, "r5astromech"}, {Upg::Astromech, "r5k6"}, {Upg::Torpedo, "protontorpedoes"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(10), ID(11), TS(Tok::Shield, 2), TK(Tok::Stress), TL("M","N"), TL("O","P")}
                                                                                                  },

  { "SWX03", "9781616613785", "TIE Fighter Expansion Pack",                   RelGroup::Wave1,    {2012, 4,17}, {2012, 9,14},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/4/17/prepare-to-launch-the-starfighters/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2012/8/2/well-have-to-destroy-them-ship-to-ship/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2012/9/14/x-wing-tm-is-now-available/"},
                                                                               	                   {{Shp::TIEFighter, ""}},
                                                                                                   {{"howlrunner", Fac::Imperial, Shp::TIEFighter}, {"backstabber", Fac::Imperial, Shp::TIEFighter}, {"wingedgundark", Fac::Imperial, Shp::TIEFighter}, {"blacksquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"obsidiansquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"academypilot", Fac::Imperial, Shp::TIEFighter}},
                                                                                                   {{Upg::Elite, "determination"}, {Upg::Elite, "swarmtactics"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus),ID(12), ID(13), ID(14), ID(15), TK(Tok::Stress)}
                                                                                                  },

  { "SWX04", "9781616613792", "Y-Wing Expansion Pack",                        RelGroup::Wave1,    {2012, 4,17}, {2012, 9,14},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/4/17/prepare-to-launch-the-starfighters/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2012/8/7/fire-the-ion-cannon/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2012/9/14/x-wing-tm-is-now-available/"},
                                                                                                  {{Shp::YWing, ""}},
                                                                                                   {{"hortonsalm", Fac::Rebel, Shp::YWing}, {"dutchvander", Fac::Rebel, Shp::YWing}, {"graysquadronpilot", Fac::Rebel, Shp::YWing}, {"goldsquadronpilot", Fac::Rebel, Shp::YWing}},
                                                                                                   {{Upg::Astromech, "r2astromech"}, {Upg::Astromech, "r5d8"}, {Upg::Torpedo, "protontorpedoes"}, {Upg::Turret, "ioncannonturret"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(16), ID(17), TS(Tok::Shield,3), TK(Tok::Stress), TL("Q","R"), TK(Tok::Ion)}
                                                                                                  },

  { "SWX05", "9781616613808", "TIE Advanced Expansion Pack",                  RelGroup::Wave1,    {2012, 4,17}, {2012, 9,14},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/4/17/prepare-to-launch-the-starfighters/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2012/8/8/elite-pilots-and-advanced-tactics/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2012/9/14/x-wing-tm-is-now-available/"},
                                                                                                  {{Shp::TIEAdvanced, ""}},
                                                                                                   {{"darthvader", Fac::Imperial, Shp::TIEAdvanced}, {"maarekstele", Fac::Imperial, Shp::TIEAdvanced}, {"stormsquadronpilot", Fac::Imperial, Shp::TIEAdvanced}, {"tempestsquadronpilot", Fac::Imperial, Shp::TIEAdvanced}},
                                                                                                   {{Upg::Elite, "squadleader"}, {Upg::Elite, "experthandling"}, {Upg::Elite, "swarmtactics"}, {Upg::Missile, "clustermissiles"}, {Upg::Missile, "concussionmissiles"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(18), ID(19), TS(Tok::Shield,2), TK(Tok::Stress), TL("S","T"), TL("U","V")}
                                                                                                  },

  { "SWX06", "9781616615352", "Millennium Falcon Expansion Pack",             RelGroup::Wave2,    {2012, 9,14}, {2013, 2,28},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/9/14/new-starfighters-blast-into-action/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/1/3/the-millennium-falcon-tm/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/2/28/make-the-jump-to-lightspeed/"},
                                                                                                  {{Shp::YT1300, "Original (round dish)"}},
                                                                                                   {{"hansolo", Fac::Rebel, Shp::YT1300}, {"landocalrissian", Fac::Rebel, Shp::YT1300}, {"chewbacca", Fac::Rebel, Shp::YT1300}, {"outerrimsmuggler", Fac::Rebel, Shp::YT1300}},
                                                                                                   {{Upg::Elite, "drawtheirfire"}, {Upg::Elite, "elusiveness"}, {Upg::Elite, "veteraninstincts"}, {Upg::Missile, "assaultmissiles"}, {Upg::Missile, "concussionmissiles"}, {Upg::Crew, "chewbacca"}, {Upg::Crew, "lukeskywalker"}, {Upg::Crew, "niennunb"}, {Upg::Crew, "weaponsengineer"}, {Upg::Title, "millenniumfalcon"}, {Upg::Modification, "engineupgrade"}, {Upg::Modification, "engineupgrade"}, {Upg::Modification, "shieldupgrade"}, {Upg::Modification, "shieldupgrade"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(20), TS(Tok::Shield,5), TS(Tok::Stress,2), TK(Tok::Ion), TS(Tok::Container,3), TS(Tok::Escort,2), TK(Tok::Smuggler)}
                                                                                                  },

  { "SWX07", "9781616615369", "Slave I Expansion Pack",                       RelGroup::Wave2,    {2012, 9,14}, {2013, 2,28},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/9/14/new-starfighters-blast-into-action/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/1/23/take-them-by-surprise/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/2/28/make-the-jump-to-lightspeed/"},
                                                                                                  {{Shp::Firespray31, ""}},
                                                                                                   {{"bobafett", Fac::Imperial, Shp::Firespray31}, {"kathscarlet", Fac::Imperial, Shp::Firespray31}, {"krassistrelix", Fac::Imperial, Shp::Firespray31}, {"bountyhunter", Fac::Imperial, Shp::Firespray31}},
                                                                                                   {{Upg::Elite, "expose"}, {Upg::Elite, "veteraninstincts"}, {Upg::Missile, "assaultmissiles"}, {Upg::Missile, "homingmissiles"}, {Upg::Cannon, "heavylasercannon"}, {Upg::Cannon, "ioncannon"}, {Upg::Bomb, "proximitymines"}, {Upg::Bomb, "seismiccharges"}, {Upg::Crew, "gunner"}, {Upg::Crew, "mercenarycopilot"}, {Upg::Title, "slavei"}, {Upg::Modification, "stealthdevice"}, {Upg::Modification, "stealthdevice"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(21), ID(22), TS(Tok::Shield,4), TS(Tok::Stress,2), TS(Tok::Ion,2), TK(Tok::ProximityMine), TK(Tok::SeismicCharge), TK(Tok::Bounty)}
                                                                                                  },

  { "SWX08", "9781616615376", "A-Wing Expansion Pack",                        RelGroup::Wave2,    {2012, 9,14}, {2013, 2,28},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/9/14/new-starfighters-blast-into-action/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/2/20/lead-the-green-squadron/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/2/28/make-the-jump-to-lightspeed/"},
                                                                                                  {{Shp::AWing, "Original (red)"}},
                                                                                                   {{"tychocelchu", Fac::Rebel, Shp::AWing}, {"arvelcrynyd", Fac::Rebel, Shp::AWing}, {"greensquadronpilot", Fac::Rebel, Shp::AWing}, {"prototypepilot", Fac::Rebel, Shp::AWing}},
                                                                                                   {{Upg::Elite, "deadeye"}, {Upg::Elite, "pushthelimit"}, {Upg::Missile, "clustermissiles"}, {Upg::Missile, "concussionmissiles"}, {Upg::Missile, "homingmissiles"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(23), ID(24), TS(Tok::Shield,2), TS(Tok::Stress,3), TL("W","X")}
                                                                                                  },

  { "SWX09", "9781616615383", "TIE Interceptor Expansion Pack",               RelGroup::Wave2,    {2012, 9,14}, {2013, 2,28},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2012/9/14/new-starfighters-blast-into-action/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/2/15/fly-with-the-saber-squadron/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/2/28/make-the-jump-to-lightspeed/"},
                                                                                                  {{Shp::TIEInterceptor, "Original (gray)"}},
                                                                                                   {{"soontirfel", Fac::Imperial, Shp::TIEInterceptor}, {"turrphennir", Fac::Imperial, Shp::TIEInterceptor}, {"felswrath", Fac::Imperial, Shp::TIEInterceptor}, {"sabersquadronpilot", Fac::Imperial, Shp::TIEInterceptor}, {"avengersquadronpilot", Fac::Imperial, Shp::TIEInterceptor}, {"alphasquadronpilot", Fac::Imperial, Shp::TIEInterceptor}},
                                                                                                   {{Upg::Elite, "daredevil"}, {Upg::Elite, "elusiveness"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(25), ID(26), ID(27), ID(28), TK(Tok::Stress)}
                                                                                                  },

//   SWX10 - 9781616614478 - X-Wing Dice Pack

  { "SWX11", "9781616616748", "Rebel Transport Expansion Pack",               RelGroup::Epic,     {2013, 8,20}, {2014, 4,30},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2013/8/20/assemble-the-fleet/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/1/31/get-to-your-transport/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/3/6/heroes-of-the-rebel-alliance/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/3/21/evacuation-of-hoth/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/4/30/the-first-transport-is-away/"},
                                                                                                  {{Shp::GR75MediumTransport, ""}, {Shp::XWing, "Alternate paint"}},
                                                                                                   {{"gr75mediumtransport", Fac::Rebel, Shp::GR75MediumTransport}, {"hobbieklivian", Fac::Rebel, Shp::XWing}, {"jekporkins", Fac::Rebel, Shp::XWing}, {"tarnmison", Fac::Rebel, Shp::XWing}, {"wesjanson", Fac::Rebel, Shp::XWing}, {"redsquadronpilot", Fac::Rebel, Shp::XWing}, {"rookiepilot", Fac::Rebel, Shp::XWing}},
                                                                                                   {{Upg::Astromech, "r2d6"}, {Upg::Astromech, "r3a2"}, {Upg::Astromech, "r4d6"}, {Upg::Astromech, "r5p9"}, {Upg::Crew, "carlistrieekan"}, {Upg::Crew, "jandodonna"}, {Upg::Crew, "torynfarr"}, {Upg::Crew, "wed15repairdroid"}, {Upg::Torpedo, "flechettetorpedoes"}, {Upg::Torpedo, "flechettetorpedoes"}, {Upg::Torpedo, "flechettetorpedoes"}, {Upg::Modification, "combatretrofit"}, {Upg::Title, "brighthope"}, {Upg::Title, "dutyfree"}, {Upg::Title, "quantumstorm"}, {Upg::Cargo, "backupshieldgenerator"}, {Upg::Cargo, "commsbooster"}, {Upg::Cargo, "ememitter"}, {Upg::Cargo, "enginebooster"}, {Upg::Cargo, "expandedcargohold"}, {Upg::Cargo, "frequencyjammer"}, {Upg::Cargo, "shieldprojector"}, {Upg::Cargo, "slicertools"}, {Upg::Cargo, "tibannagassupplies"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(34), TS(Tok::Shield,7), TS(Tok::Stress,3), TL("U","V"), TS(Tok::Ion,6), TS(Tok::Energy,6), TK(Tok::Reinforce), TS(Tok::Control,6), TS(Tok::Group,4), TS(Tok::IonBlast,2), TS(Tok::Maneuver,8), TS(Tok::Mine,6), TS(Tok::Role,4), TS(Tok::ScopeHyperspace,4), TS(Tok::Turbolaser,3)}
                                                                                                  },

  { "SWX12", "9781616617042", "HWK-290 Expansion Pack",                       RelGroup::Wave3,    {2013, 5, 4}, {2013, 9,13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2013/5/4/support-ships-and-system-upgrades/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/6/13/new-wingmen-for-the-win/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/6/new-starships-tough-command-decisions/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/13/your-fleet-support-has-just-arrived/"},
                                                                                                  {{Shp::HWK290, ""}},
                                                                                                   {{"janors", Fac::Rebel, Shp::HWK290}, {"kylekatarn", Fac::Rebel, Shp::HWK290}, {"roarkgarnet", Fac::Rebel, Shp::HWK290}, {"rebeloperative", Fac::Rebel, Shp::HWK290}},
                                                                                                   {{Upg::Turret, "blasterturret"}, {Upg::Turret, "ioncannonturret"}, {Upg::Crew, "intelligenceagent"}, {Upg::Crew, "reconspecialist"}, {Upg::Crew, "saboteur"}, {Upg::Title,"moldycrow"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(29), TK(Tok::Shield), TS(Tok::Stress,2), TL("Y","Z"), TK(Tok::Ion)}
                                                                                                  },

  { "SWX13", "9781616616755", "Lambda-class Shuttle Expansion Pack",          RelGroup::Wave3,    {2013,  5,  4}, {2013,  9, 13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2013/5/4/support-ships-and-system-upgrades/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/6/13/new-wingmen-for-the-win/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/7/8/advanced-technology/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/6/new-starships-tough-command-decisions/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/13/your-fleet-support-has-just-arrived/"},
                                                                                                  {{Shp::LambdaClassShuttle, ""}},
                                                                                                   {{"captainkagi", Fac::Imperial, Shp::LambdaClassShuttle}, {"coloneljendon", Fac::Imperial, Shp::LambdaClassShuttle}, {"captainyorr", Fac::Imperial, Shp::LambdaClassShuttle}, {"omicrongrouppilot", Fac::Imperial, Shp::LambdaClassShuttle}},
                                                                                                   {{Upg::Cannon, "heavylasercannon"}, {Upg::Crew, "darthvader"}, {Upg::Crew, "flightinstructor"}, {Upg::Crew, "intelligenceagent"}, {Upg::Crew, "navigator"}, {Upg::Crew, "rebelcaptive"}, {Upg::Crew, "weaponsengineer"}, {Upg::Modification, "antipursuitlasers"}, {Upg::Title, "st321"}, {Upg::System, "advancedsensors"}, {Upg::System, "sensorjammer"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), TS(Tok::Shield,5), TS(Tok::Stress,3), TL("M","N"), TS(Tok::Ion,4), TS(Tok::Tracking,4), TS(Tok::DisabledShip,3)}
                                                                                                  },

  { "SWX14", "9781616616762", "B-Wing Expansion Pack",                        RelGroup::Wave3,    {2013, 5, 4}, {2013, 9,13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2013/5/4/support-ships-and-system-upgrades/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/7/8/advanced-technology/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/8/5/target-lock-acquired/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/6/new-starships-tough-command-decisions/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/13/your-fleet-support-has-just-arrived/"},
                                                                                                  {{Shp::BWing, ""}},
                                                                                                   {{"tennumb", Fac::Rebel, Shp::BWing}, {"ibtisam", Fac::Rebel, Shp::BWing}, {"daggersquadronpilot", Fac::Rebel, Shp::BWing}, {"bluesquadronpilot", Fac::Rebel, Shp::BWing}},
                                                                                                   {{Upg::Torpedo, "advprotontorpedoes"}, {Upg::Torpedo, "protontorpedoes"}, {Upg::Cannon, "autoblaster"}, {Upg::Cannon, "ioncannon"}, {Upg::System, "firecontrolsystem"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(30), TS(Tok::Shield,5), TK(Tok::Stress), TL("S","T"), TK(Tok::Ion)}
                                                                                                  },

  { "SWX15", "9781616616779", "TIE Bomber Expansion Pack",                    RelGroup::Wave3,    {2013, 5, 4}, {2013, 9,13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2013/5/4/support-ships-and-system-upgrades/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2013/8/5/target-lock-acquired/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/6/new-starships-tough-command-decisions/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2013/9/13/your-fleet-support-has-just-arrived/"},
                                                                                                  {{Shp::TIEBomber, "Original (gray)"}},
                                                                                                   {{"majorrhymer", Fac::Imperial, Shp::TIEBomber}, {"captainjonus", Fac::Imperial, Shp::TIEBomber}, {"gammasquadronpilot", Fac::Imperial, Shp::TIEBomber}, {"scimitarsquadronpilot", Fac::Imperial, Shp::TIEBomber}},
                                                                                                   {{Upg::Elite, "adrenalinerush"}, {Upg::Torpedo, "advprotontorpedoes"}, {Upg::Missile, "assaultmissiles"}, {Upg::Bomb, "protonbombs"}, {Upg::Bomb, "seismiccharges"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(31), TS(Tok::Stress,2), TL("O","P"), TK(Tok::SeismicCharge), TK(Tok::ProtonBomb)}
                                                                                                  },

  { "SWX16", "9781616617714", "Z-95 Headhunter Expansion Pack",               RelGroup::Wave4,    {2014, 2, 7}, {2014, 6,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/2/7/upgrade-your-fleet/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/3/24/missiles-away/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/6/26/wave-iv-is-now-available/"},
                                                                                                  {{Shp::Z95Headhunter, ""}},
                                                                                                   {{"airencracken", Fac::Rebel, Shp::Z95Headhunter}, {"lieutenantblount", Fac::Rebel, Shp::Z95Headhunter}, {"talasquadronpilot", Fac::Rebel, Shp::Z95Headhunter}, {"banditsquadronpilot", Fac::Rebel, Shp::Z95Headhunter}},
                                                                                                   {{Upg::Elite, "decoy"}, {Upg::Elite, "wingman"}, {Upg::Missile, "ionpulsemissiles"}, {Upg::Missile, "assaultmissiles"}, {Upg::Modification, "munitionsfailsafe"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(35), ID(36), TS(Tok::Shield,2), TK(Tok::Stress), TL("M","N"), TS(Tok::Ion,2)}
                                                                                                  },

  { "SWX17", "9781616617721", "TIE Defender Expansion Pack",                  RelGroup::Wave4,    {2014, 2, 7}, {2014, 6,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/2/7/upgrade-your-fleet/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/4/9/designed-for-success/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/6/26/wave-iv-is-now-available/"},
                                                                                                  {{Shp::TIEDefender, "Original (gray)"}},
                                                                                                   {{"rexlerbrath", Fac::Imperial, Shp::TIEDefender}, {"colonelvessery", Fac::Imperial, Shp::TIEDefender}, {"onyxsquadronpilot", Fac::Imperial, Shp::TIEDefender}, {"deltasquadronpilot", Fac::Imperial, Shp::TIEDefender}},
                                                                                                   {{Upg::Elite, "outmaneuver"}, {Upg::Elite, "predator"}, {Upg::Cannon, "ioncannon"}, {Upg::Missile, "ionpulsemissiles"}, {Upg::Modification, "munitionsfailsafe"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TS(Tok::Focus,2), ID(37), ID(38), TS(Tok::Shield,3), TK(Tok::Stress), TL("W","X"), TS(Tok::Ion,2)}
                                                                                                  },

  { "SWX18", "9781616617738", "E-Wing Expansion Pack",                        RelGroup::Wave4,    {2014, 2, 7}, {2014, 6,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/2/7/upgrade-your-fleet/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/4/28/performing-the-impossible/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/6/26/wave-iv-is-now-available/"},
                                                                                                  {{Shp::EWing, ""}},
                                                                                                   {{"corranhorn", Fac::Rebel, Shp::EWing}, {"etahnabaht", Fac::Rebel, Shp::EWing}, {"blackmoonsquadronpilot", Fac::Rebel, Shp::EWing}, {"knavesquadronpilot", Fac::Rebel, Shp::EWing}},
                                                                                                   {{Upg::Elite, "outmaneuver"}, {Upg::Astromech, "r7astromech"}, {Upg::Astromech, "r7t1"}, {Upg::Torpedo, "flechettetorpedoes"}, {Upg::System, "advancedsensors"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(39), ID(40), TS(Tok::Shield,3), TS(Tok::Stress,2), TL("O","P")}
                                                                                                  },

  { "SWX19", "9781616617745", "TIE Phantom Expansion Pack",                   RelGroup::Wave4,    {2014, 2, 7}, {2014, 6,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/2/7/upgrade-your-fleet/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/5/5/cloaked-in-battle/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/6/26/wave-iv-is-now-available/"},
                                                                                                  {{Shp::TIEPhantom, ""}},
                                                                                                   {{"whisper", Fac::Imperial, Shp::TIEPhantom}, {"echo", Fac::Imperial, Shp::TIEPhantom}, {"shadowsquadronpilot", Fac::Imperial, Shp::TIEPhantom}, {"sigmasquadronpilot", Fac::Imperial, Shp::TIEPhantom}},
                                                                                                   {{Upg::Crew, "reconspecialist"}, {Upg::Crew, "tactician"}, {Upg::System, "firecontrolsystem"}, {Upg::Modification, "advancedcloakingdevice"}, {Upg::Modification, "stygiumparticleaccelerator"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TS(Tok::Focus,2), ID(41), ID(42), TS(Tok::Shield,2), TK(Tok::Stress), TL("Y","Z"), TK(Tok::Cloak)}
                                                                                                  },

//   SWX20 - Starfield Game Tile Kit (Cancelled)

  { "SWX21", "9781616618032", "Imperial Aces Expansion Pack",                 RelGroup::Aces,     {2013,  9, 16}, {2014, 3,17},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2013/9/16/earn-your-bloodstripes/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/1/2/flying-with-the-aces/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/3/17/crush-the-rebellion/"},
                                                                                                  {{Shp::TIEInterceptor, "red"}, {Shp::TIEInterceptor, "red stripe"}},
                                                                                                   {{"carnorjax", Fac::Imperial, Shp::TIEInterceptor}, {"tetrancowall", Fac::Imperial, Shp::TIEInterceptor}, {"kirkanos", Fac::Imperial, Shp::TIEInterceptor}, {"lieutenantlorrir", Fac::Imperial, Shp::TIEInterceptor}, {"royalguardpilot", Fac::Imperial, Shp::TIEInterceptor}, {"royalguardpilot", Fac::Imperial, Shp::TIEInterceptor}, {"sabersquadronpilot", Fac::Imperial, Shp::TIEInterceptor}, {"sabersquadronpilot", Fac::Imperial, Shp::TIEInterceptor}},
                                                                                                   {{Upg::Elite, "opportunist"}, {Upg::Elite, "opportunist"}, {Upg::Elite, "pushthelimit"}, {Upg::Elite, "pushthelimit"}, {Upg::Title, "royalguardtie"}, {Upg::Title, "royalguardtie"}, {Upg::Modification, "hullupgrade"}, {Upg::Modification, "hullupgrade"}, {Upg::Modification, "shieldupgrade"}, {Upg::Modification, "shieldupgrade"}, {Upg::Modification, "targetingcomputer"}, {Upg::Modification, "targetingcomputer"}},
                                                                                                   {},
                                                                                                   {TS(Tok::Evade,2), TS(Tok::Focus,2), ID(32), ID(33), TS(Tok::Shield,2), TS(Tok::Stress,2), TL("Q","R"), TS(Tok::Damage,6), TS(Tok::Function,6), TK(Tok::Prototype)}
                                                                                                  },

  { "SWX22", "9781616617691", "Tantive IV Expansion Pack",                    RelGroup::Epic,     {2013,  8, 20}, {2014, 5,22},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2013/8/20/assemble-the-fleet/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/2/21/begin-a-new-adventure/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/4/4/point-of-no-return/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/5/16/battle-ready/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/5/22/aid-the-rebel-alliance/"},
                                                                                                  {{Shp::CR90Corvette, ""}},
                                                                                                   {{"cr90corvettefore", Fac::Rebel, Shp::CR90Corvette}, {"cr90corvetteaft", Fac::Rebel, Shp::CR90Corvette}},
                                                                                                   {{Upg::Cargo, "backupshieldgenerator"}, {Upg::Cargo, "commsbooster"}, {Upg::Cargo, "commsbooster"}, {Upg::Cargo, "enginebooster"}, {Upg::Cargo, "ionizationreactor"}, {Upg::Cargo, "tibannagassupplies"}, {Upg::Crew, "c3po"}, {Upg::Crew, "hansolo"}, {Upg::Crew, "leiaorgana"}, {Upg::Crew, "r2d2-swx22"}, {Upg::Crew, "raymusantilles"}, {Upg::Crew, "targetingcoordinator"}, {Upg::Hardpoint, "quadlasercannons"}, {Upg::Hardpoint, "quadlasercannons"}, {Upg::Hardpoint, "quadlasercannons"}, {Upg::Hardpoint, "singleturbolasers"}, {Upg::Hardpoint, "singleturbolasers"}, {Upg::Hardpoint, "singleturbolasers"}, {Upg::Team, "engineeringteam"}, {Upg::Team, "gunneryteam"}, {Upg::Team, "sensorteam"}, {Upg::Title, "dodonnaspride"}, {Upg::Title, "jainaslight"}, {Upg::Title, "tantiveiv"}},
                                                                                                   {},
                                                                                                   {TS(Tok::Shield,14), TL("W","R"), TS(Tok::Ion,6), TS(Tok::Energy,16), TK(Tok::Reinforce), TS(Tok::Satellite,2), TS(Tok::Damage,5),TS(Tok::ActiveCharge,5), TK(Tok::ClassECargoContainer),  TS(Tok::DudCharge,4), TK(Tok::Hyperdrive), TS(Tok::HyperdriveRepair,6), TS(Tok::Scope,6)}
                                                                                                  },

  { "SWX23", "9781616619176", "YT-2400 Freighter Expansion Pack",             RelGroup::Wave5,    {2014, 6,13}, {2014,11,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/6/13/bring-out-the-big-guns-1/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/8/11/blast-into-action/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/11/26/wave-v-is-now-available/"},
                                                                                                  {{Shp::YT2400, ""}},
                                                                                                   {{"dashrendar", Fac::Rebel, Shp::YT2400}, {"leebo", Fac::Rebel, Shp::YT2400}, {"eadenvrill", Fac::Rebel, Shp::YT2400}, {"wildspacefringer", Fac::Rebel, Shp::YT2400}},
                                                                                                   {{Upg::Elite, "lonewolf"}, {Upg::Elite, "stayontarget"}, {Upg::Missile, "protonrockets"}, {Upg::Cannon, "heavylasercannon"}, {Upg::Crew, "leebo"}, {Upg::Crew, "dashrendar"}, {Upg::Crew, "gunner"}, {Upg::Crew, "landocalrissian"}, {Upg::Crew, "mercenarycopilot"}, {Upg::Title, "outrider"}, {Upg::Modification, "countermeasures"}, {Upg::Modification, "experimentalinterface"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Focus), TS(Tok::Shield,5), TK(Tok::Stress), TS(Tok::Ion,2), TS(Tok::DebrisCloud,3), TS(Tok::Cache,5)}
                                                                                                  },

  { "SWX24", "9781616619183", "VT-49 Decimator Expansion Pack",               RelGroup::Wave5,    {2014, 6,13}, {2014,11,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/6/13/bring-out-the-big-guns-1/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/8/11/ruthlessness-and-intimidation/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/11/26/wave-v-is-now-available/"},
                                                                                                  {{Shp::VT49Decimator, ""}},
                                                                                                   {{"rearadmiralchiraneau", Fac::Imperial, Shp::VT49Decimator}, {"commanderkenkirk", Fac::Imperial, Shp::VT49Decimator}, {"captainoicunn", Fac::Imperial, Shp::VT49Decimator}, {"patrolleader", Fac::Imperial, Shp::VT49Decimator}},
                                                                                                   {{Upg::Elite, "intimidation"}, {Upg::Elite, "ruthlessness"}, {Upg::Elite, "ruthlessness"}, {Upg::Torpedo, "iontorpedoes"}, {Upg::Torpedo, "iontorpedoes"}, {Upg::Bomb, "protonbombs"}, {Upg::Crew, "fleetofficer"}, {Upg::Crew, "marajade"}, {Upg::Crew, "moffjerjerrod"}, {Upg::Crew, "ysanneisard"}, {Upg::Title, "dauntless"}, {Upg::Modification, "tacticaljammer"}, {Upg::Modification, "tacticaljammer"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Evade), TK(Tok::Focus), TS(Tok::Shield,4), TK(Tok::Stress), TK(Tok::Ion), TK(Tok::ProtonBomb), TS(Tok::DebrisCloud,3)}
                                                                                                  },

  { "SWX25", "9781616619381", "StarViper Expansion Pack",                     RelGroup::Wave6,    {2014, 8,16}, {2015, 2,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/8/16/scum-and-villainy/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/11/24/lethal-ambitions/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/2/26/wave-vi-is-now-available/"},
                                                                                                  {{Shp::StarViper, ""}},
                                                                                                   {{"princexizor", Fac::Scum, Shp::StarViper}, {"guri", Fac::Scum, Shp::StarViper}, {"blacksunvigo", Fac::Scum, Shp::StarViper}, {"blacksunenforcer", Fac::Scum, Shp::StarViper}},
                                                                                                   {{Upg::Elite, "bodyguard"}, {Upg::Elite, "calculation"}, {Upg::Torpedo, "iontorpedoes"}, {Upg::System, "accuracycorrector"}, {Upg::Title, "virago"}, {Upg::Modification, "autothrusters"}, {Upg::Modification, "autothrusters"}, {Upg::Modification, "hullupgrade"}, {Upg::Illicit, "inertialdampeners"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Focus), ID(44), TK(Tok::Shield), TK(Tok::Stress), TL("EE","FF"), TS(Tok::Ion,7)}
                                                                                                  },

  { "SWX26", "9781616619398", "M3-A Interceptor Expansion Pack",              RelGroup::Wave6,    {2014, 8,16}, {2015, 2,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/8/16/scum-and-villainy/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/12/29/intercept-and-destroy/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/2/26/wave-vi-is-now-available/"},
                                                                                                  {{Shp::M3AInterceptor, ""}},
                                                                                                   {{"laetinashera", Fac::Scum, Shp::M3AInterceptor}, {"serissu", Fac::Scum, Shp::M3AInterceptor}, {"tansariipointveteran", Fac::Scum, Shp::M3AInterceptor}, {"cartelspacer", Fac::Scum, Shp::M3AInterceptor}},
                                                                                                   {{Upg::Cannon, "manglercannon"}, {Upg::Cannon, "flechettecannon"}, {Upg::Cannon, "ioncannon"}, {Upg::Title, "heavyscykinterceptor"}, {Upg::Modification, "stealthdevice"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Evade), TK(Tok::Focus), ID(45), TK(Tok::Shield), TS(Tok::Stress,2), TL("GG","HH"), TS(Tok::Ion,3)}
                                                                                                  },

  { "SWX27", "9781616619404", "IG-2000 Expansion Pack",                       RelGroup::Wave6,    {2014, 8,16}, {2015, 2,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/8/16/scum-and-villainy/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/1/12/cold-metal-and-evil/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/2/26/wave-vi-is-now-available/"},
                                                                                                  {{Shp::IG2000, ""}},
                                                                                                   {{"ig88a", Fac::Scum, Shp::IG2000}, {"ig88b", Fac::Scum, Shp::IG2000}, {"ig88c", Fac::Scum, Shp::IG2000}, {"ig88d", Fac::Scum, Shp::IG2000}},
                                                                                                   {{Upg::Cannon, "manglercannon"}, {Upg::Cannon, "autoblaster"}, {Upg::Bomb, "proximitymines"}, {Upg::Bomb, "seismiccharges"}, {Upg::System, "accuracycorrector"}, {Upg::Title, "ig2000"}, {Upg::Illicit, "hotshotblaster"}, {Upg::Illicit, "deadmansswitch"}, {Upg::Illicit, "deadmansswitch"}, {Upg::Illicit, "feedbackarray"}, {Upg::Illicit, "feedbackarray"}, {Upg::Illicit, "inertialdampeners"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Evade), TK(Tok::Focus), TS(Tok::Shield,4), TK(Tok::Stress), TL("II","JJ"), TS(Tok::Ion,2), TK(Tok::ProximityMine), TK(Tok::SeismicCharge)}
                                                                                                  },

  { "SWX28", "9781616619411", "Most Wanted Expansion Pack",                   RelGroup::Wave6,    {2014, 8,16}, {2015, 2,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/8/16/scum-and-villainy/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/10/20/the-galaxys-most-wanted-part-one/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/11/3/the-galaxys-most-wanted-part-two/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/2/26/wave-vi-is-now-available/"},
                                                                                                  {{Shp::YWing, "Scum colors"}, {Shp::Z95Headhunter, "Scum colors"}, {Shp::Z95Headhunter, "Scum colors"}},
                                                                                                   {{"kavil", Fac::Scum, Shp::YWing}, {"drearenthal", Fac::Scum, Shp::YWing}, {"hiredgun", Fac::Scum, Shp::YWing}, {"syndicatethug", Fac::Scum, Shp::YWing}, {"ndrusuhlak", Fac::Scum, Shp::Z95Headhunter}, {"kaatoleeachos", Fac::Scum, Shp::Z95Headhunter}, {"blacksunsoldier", Fac::Scum, Shp::Z95Headhunter}, {"binayrepirate", Fac::Scum, Shp::Z95Headhunter}, {"dacebonearm", Fac::Scum, Shp::HWK290}, {"palobgodalhi", Fac::Scum, Shp::HWK290}, {"torkilmux", Fac::Scum, Shp::HWK290}, {"spicerunner", Fac::Scum, Shp::HWK290}, {"bobafett", Fac::Scum, Shp::Firespray31}, {"kathscarlet", Fac::Scum, Shp::Firespray31}, {"emonazzameen", Fac::Scum, Shp::Firespray31}, {"mandalorianmercenary", Fac::Scum, Shp::Firespray31}},
                                                                                                   {{Upg::Turret, "autoblasterturret"}, {Upg::Turret, "autoblasterturret"}, {Upg::Torpedo, "bombloadout"}, {Upg::Torpedo, "bombloadout"}, {Upg::Crew, "greedo"}, {Upg::Crew, "k4securitydroid"}, {Upg::Crew, "outlawtech"}, {Upg::SalvagedAstromech, "genius"}, {Upg::SalvagedAstromech, "r4agromech"}, {Upg::SalvagedAstromech, "r4agromech"}, {Upg::SalvagedAstromech, "r4b11"}, {Upg::SalvagedAstromech, "salvagedastromech"}, {Upg::SalvagedAstromech, "salvagedastromech"}, {Upg::SalvagedAstromech, "unhingedastromech"}, {Upg::SalvagedAstromech, "unhingedastromech"}, {Upg::Title, "btla4ywing"}, {Upg::Title, "btla4ywing"}, {Upg::Title, "andrasta"}, {Upg::Illicit, "hotshotblaster"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Focus), TS(Tok::Shield,7), TS(Tok::Stress,4), TL("KK","LL")}
                                                                                                  },

  { "SWX29", "9781616619114", "Rebel Aces Expansion Pack",                    RelGroup::Aces,     {2014, 3,18}, {2014, 9,25},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/3/18/the-rebellions-finest/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2014/6/23/a-more-versatile-a-wing/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/7/25/b-wing-expanded/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/9/15/jump-to-subspace/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2014/9/25/join-the-aces/"},
                                                                                                  {{Shp::AWing, "Alternate (blue)"}, {Shp::BWing, "Alternate"}},
                                                                                                   {{"keyanfarlander", Fac::Rebel, Shp::BWing}, {"neradantels", Fac::Rebel, Shp::BWing}, {"daggersquadronpilot", Fac::Rebel, Shp::BWing}, {"bluesquadronpilot", Fac::Rebel, Shp::BWing}, {"jakefarrell", Fac::Rebel, Shp::AWing}, {"gemmersojan", Fac::Rebel, Shp::AWing}, {"greensquadronpilot", Fac::Rebel, Shp::AWing}, {"prototypepilot", Fac::Rebel, Shp::AWing}},
                                                                                                   {{Upg::Missile, "chardaanrefit"}, {Upg::Missile, "chardaanrefit"}, {Upg::Missile, "chardaanrefit"}, {Upg::Missile, "protonrockets"}, {Upg::Missile, "protonrockets"}, {Upg::Crew, "janors"}, {Upg::Crew, "kylekatarn"}, {Upg::System, "enhancedscopes"}, {Upg::System, "enhancedscopes"}, {Upg::Title, "awingtestpilot"}, {Upg::Title, "awingtestpilot"}, {Upg::Modification, "bwinge2"}, {Upg::Modification, "bwinge2"}},
                                                                                                   {},
                                                                                                   {TS(Tok::Evade,2), TS(Tok::Focus,3), ID(43), TS(Tok::Shield,7), TS(Tok::Stress,2), TL("AA","BB"), TL("CC","DD"), TK(Tok::EscapePod)}
                                                                                                  },

  { "SWX30", "9781616619671", "Imperial Raider Expansion Pack",               RelGroup::Epic,     {2014,12,19}, {2015, 8,13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2014/12/19/here-comes-the-imperial-raider/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/4/24/the-might-of-the-empire/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/5/4/the-academys-finest/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/5/26/the-grand-design/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/8/10/the-will-of-the-empire/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/8/13/hunt-down-and-destroy-the-rebellion/"},
                                                                                                  {{Shp::RaiderClassCorvette, ""}, {Shp::TIEAdvanced, "Alternate (Raider)"}},
                                                                                                   {{"raiderclasscorvettefore", Fac::Imperial, Shp::RaiderClassCorvette},{"raiderclasscorvetteaft", Fac::Imperial, Shp::RaiderClassCorvette}, {"zertikstrom", Fac::Imperial, Shp::TIEAdvanced}, {"junoeclipse", Fac::Imperial, Shp::TIEAdvanced}, {"commanderalozen", Fac::Imperial, Shp::TIEAdvanced}, {"lieutenantcolzet", Fac::Imperial, Shp::TIEAdvanced}, {"stormsquadronpilot", Fac::Imperial, Shp::TIEAdvanced}, {"tempestsquadronpilot", Fac::Imperial, Shp::TIEAdvanced}},
                                                                                                   {{Upg::Missile, "clustermissiles"}, {Upg::Missile, "protonrockets"}, {Upg::Crew, "admiralozzel"}, {Upg::Crew, "captainneeda"}, {Upg::Crew, "emperorpalpatine"}, {Upg::Crew, "grandmofftarkin"}, {Upg::Crew, "shieldtechnician"}, {Upg::Crew, "shieldtechnician"}, {Upg::System, "advtargetingcomputer"}, {Upg::System, "advtargetingcomputer"}, {Upg::System, "advtargetingcomputer"}, {Upg::System, "advtargetingcomputer"}, {Upg::Title, "assailer"}, {Upg::Title, "impetuous"}, {Upg::Title, "instigator"}, {Upg::Title, "tiex1"}, {Upg::Title, "tiex1"}, {Upg::Title, "tiex1"}, {Upg::Title, "tiex1"}, {Upg::Cargo, "backupshieldgenerator"}, {Upg::Cargo, "commsbooster"}, {Upg::Cargo, "enginebooster"}, {Upg::Cargo, "tibannagassupplies"}, {Upg::Hardpoint, "ioncannonbattery"}, {Upg::Hardpoint, "ioncannonbattery"}, {Upg::Hardpoint, "ioncannonbattery"}, {Upg::Hardpoint, "ioncannonbattery"}, {Upg::Hardpoint, "quadlasercannons"}, {Upg::Hardpoint, "quadlasercannons"}, {Upg::Hardpoint, "singleturbolasers"}, {Upg::Hardpoint, "singleturbolasers"}, {Upg::Team, "engineeringteam"}, {Upg::Team, "gunneryteam"}, {Upg::Team, "sensorteam"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Evade), TK(Tok::Focus), TS(Tok::Shield,12), TK(Tok::Stress), TL("MM","NN"), TS(Tok::Ion,4), TS(Tok::Energy,16), TK(Tok::Reinforce), TS(Tok::DebrisCloud,3), TS(Tok::ActiveSentry,5), TS(Tok::DormantSentry,5)}
                                                                                                  },

  { "SWX31", "9781633440692", "Hound's Tooth Expansion Pack",                 RelGroup::Wave7,    {2015, 4,20}, {2015, 9, 2},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/4/20/hit-them-hard-and-fast/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/6/15/any-methods-necessary/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/9/2/start-your-attack-run/"},
                                                                                                  {{Shp::YV666, ""}},
                                                                                                   {{"bossk", Fac::Scum, Shp::YV666}, {"lattsrazzi", Fac::Scum, Shp::YV666}, {"moraloeval", Fac::Scum, Shp::YV666}, {"trandoshanslaver", Fac::Scum, Shp::YV666}},
                                                                                                   {{Upg::Elite, "crackshot"}, {Upg::Elite, "lonewolf"}, {Upg::Elite, "stayontarget"}, {Upg::Cannon, "heavylasercannon"}, {Upg::Crew, "bossk"}, {Upg::Crew, "k4securitydroid"}, {Upg::Crew, "outlawtech"}, {Upg::Title, "houndstooth"}, {Upg::Modification, "engineupgrade"}, {Upg::Modification, "ionprojector"}, {Upg::Modification, "ionprojector"}, {Upg::Modification, "maneuveringfins"}, {Upg::Illicit, "glitterstim"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Focus), TS(Tok::Shield,6), TK(Tok::Stress), TL("OO","PP"), TS(Tok::Ion,4), TS(Tok::Damage,4)}
                                                                                                  },

  { "SWX32", "9781633440708", "Kihraxz Fighter Expansion Pack",               RelGroup::Wave7,    {2015, 4,20}, {2015, 9, 2},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/4/20/hit-them-hard-and-fast/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/6/22/lightning-reflexes/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/9/2/start-your-attack-run/"},
                                                                                                  {{Shp::Kihraxz, ""}},
                                                                                                   {{"talonbanecobra", Fac::Scum, Shp::Kihraxz}, {"grazthehunter", Fac::Scum, Shp::Kihraxz}, {"blacksunace", Fac::Scum, Shp::Kihraxz}, {"cartelmarauder", Fac::Scum, Shp::Kihraxz}},
                                                                                                   {{Upg::Elite, "crackshot"}, {Upg::Elite, "lightningreflexes"}, {Upg::Elite, "predator"}, {Upg::Missile, "homingmissiles"}, {Upg::Illicit, "glitterstim"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(46), ID(47), TK(Tok::Shield), TK(Tok::Stress), TL("QQ","RR")}
                                                                                                  },

  { "SWX33", "9781633440715", "K-wing Expansion Pack",                        RelGroup::Wave7,    {2015, 4,20}, {2015, 9, 2},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/4/20/hit-them-hard-and-fast/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/6/29/slam-and-bomb/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/9/2/start-your-attack-run/"},
                                                                                                  {{Shp::KWing, ""}},
                                                                                                   {{"mirandadoni", Fac::Rebel, Shp::KWing}, {"esegetuketu", Fac::Rebel, Shp::KWing}, {"guardiansquadronpilot", Fac::Rebel, Shp::KWing}, {"wardensquadronpilot", Fac::Rebel, Shp::KWing}},
                                                                                                   {{Upg::Torpedo, "extramunitions"}, {Upg::Torpedo, "plasmatorpedoes"}, {Upg::Missile, "advhomingmissiles"}, {Upg::Turret, "twinlaserturret"}, {Upg::Turret, "twinlaserturret"}, {Upg::Bomb, "connernet"}, {Upg::Bomb, "ionbombs"}, {Upg::Crew, "bombardier"}, {Upg::Modification, "advancedslam"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Focus), TS(Tok::Shield,4), TK(Tok::Stress), TL("SS","TT"), TK(Tok::ConnerNet), TK(Tok::IonBomb), TS(Tok::Ordnance,4), TK(Tok::WeaponsDisabled)}
                                                                                                  },

  { "SWX34", "9781633440722", "TIE Punisher Expansion Pack",                  RelGroup::Wave7,    {2015, 4,20}, {2015, 9, 2},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/4/20/hit-them-hard-and-fast/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/7/6/obliterate-your-enemies/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/9/2/start-your-attack-run/"},
                                                                                                  {{Shp::TIEPunisher, ""}},
                                                                                                   {{"redline", Fac::Imperial, Shp::TIEPunisher}, {"deathrain", Fac::Imperial, Shp::TIEPunisher}, {"blackeightsqpilot", Fac::Imperial, Shp::TIEPunisher}, {"cutlasssquadronpilot", Fac::Imperial, Shp::TIEPunisher}},
                                                                                                   {{Upg::Torpedo, "extramunitions"}, {Upg::Torpedo, "flechettetorpedoes"}, {Upg::Torpedo, "plasmatorpedoes"}, {Upg::Missile, "advhomingmissiles"}, {Upg::Bomb, "clustermines"}, {Upg::Bomb, "ionbombs"}, {Upg::System, "enhancedscopes"}, {Upg::Modification, "twinionenginemkii"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Focus), TS(Tok::Shield,3), TK(Tok::Stress), TS(Tok::Ion,4), TK(Tok::IonBomb), TS(Tok::Ordnance,5), TK(Tok::ClusterMine)}
                                                                                                  },

  { "SWX35", "9781633441446", "Imperial Assault Carrier",                     RelGroup::Epic,     {2015, 7,31}, {2015,12,23},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/7/31/lying-in-wait/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/11/20/epic-tactics/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/11/27/swarm-tactics/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/12/23/begin-your-pursuit/"},
                                                                                                  {{Shp::GozantiClassCruiser, ""}, {Shp::TIEFighter, "Alternate (Assault Carrier)"}, {Shp::TIEFighter, "Alternate (Assault Carrier)"}},
                                                                                                   {{"gozanticlasscruiser", Fac::Imperial, Shp::GozantiClassCruiser}, {"scourge", Fac::Imperial, Shp::TIEFighter}, {"wampa", Fac::Imperial, Shp::TIEFighter}, {"youngster", Fac::Imperial, Shp::TIEFighter}, {"chaser", Fac::Imperial, Shp::TIEFighter}, {"blacksquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"blacksquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"obsidiansquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"obsidiansquadronpilot", Fac::Imperial, Shp::TIEFighter}, {"academypilot", Fac::Imperial, Shp::TIEFighter}, {"academypilot", Fac::Imperial, Shp::TIEFighter}},
                                                                                                   {{Upg::Elite, "expose"}, {Upg::Elite, "marksmanship"}, {Upg::Elite, "experthandling"}, {Upg::Missile, "clustermissiles"}, {Upg::Missile, "homingmissiles"}, {Upg::Torpedo, "iontorpedoes"}, {Upg::Crew, "agentkallus"}, {Upg::Crew, "rearadmiralchiraneau"}, {Upg::Crew, "constructiondroid"}, {Upg::Crew, "constructiondroid"}, {Upg::Title, "requiem"}, {Upg::Title, "suppressor"}, {Upg::Title, "vector"}, {Upg::Team, "ordnanceexperts"}, {Upg::Team, "ordnanceexperts"}, {Upg::Modification, "automatedprotocols"}, {Upg::Modification, "automatedprotocols"}, {Upg::Modification, "optimizedgenerators"}, {Upg::Modification, "optimizedgenerators"}, {Upg::Modification, "ordnancetubes"}, {Upg::Modification, "ordnancetubes"}, {Upg::Hardpoint, "duallaserturret"}, {Upg::Cargo, "broadcastarray"}, {Upg::Cargo, "dockingclamps"}, {Upg::Cargo, "clusterbombs"}, {Upg::Cargo, "clusterbombs"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TS(Tok::Evade,2), TS(Tok::Focus,2), TS(Tok::Shield,5), TS(Tok::Stress,4), TS(Tok::Energy,6), TK(Tok::Reinforce), TS(Tok::Maneuver,8), TS(Tok::Damage,3), TS(Tok::Signal,6), TS(Tok::Scanner,4)}
                                                                                                  },

  { "SWX36", "841333100001", "X-Wing Miniatures The Force Awakens Core Set",  RelGroup::CoreSets, {2015, 9, 4}, {0,0,0},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/9/4/man-your-ships/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/9/7/updating-a-classic/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/9/8/swx36-strength-in-numbers/"},
                                                                                                    ""},
                                                                                                  {{Shp::T70XWing, ""}, {Shp::TIEfoFighter, ""}, {Shp::TIEfoFighter, ""}},
                                                                                                   {{"poedameron", Fac::Rebel, Shp::T70XWing}, {"blueace", Fac::Rebel, Shp::T70XWing}, {"redsquadronveteran", Fac::Rebel, Shp::T70XWing}, {"bluesquadronnovice", Fac::Rebel, Shp::T70XWing}, {"omegaace", Fac::Imperial, Shp::TIEfoFighter}, {"epsilonleader", Fac::Imperial, Shp::TIEfoFighter}, {"zetaace", Fac::Imperial, Shp::TIEfoFighter}, {"omegasquadronpilot", Fac::Imperial, Shp::TIEfoFighter}, {"zetasquadronpilot", Fac::Imperial, Shp::TIEfoFighter}, {"epsilonsquadronpilot", Fac::Imperial, Shp::TIEfoFighter}},
                                                                                                   {{Upg::Astromech, "bb8"}, {Upg::Astromech, "r5x3"}, {Upg::Elite, "wired"}, {Upg::Tech, "weaponsguidance"}, {Upg::Torpedo, "protontorpedoes"}},
                                                                                                   {},
                                                                                                   {TS(Tok::Crit,3), TS(Tok::Evade,4), TS(Tok::Focus,4), ID(1), ID(2), ID(3), ID(4), ID(5), ID(6), TS(Tok::Shield,7), TS(Tok::Stress,3), TL("A","B"), TL("C","D"), TL("E","F"), TK(Tok::Initiative), TS(Tok::Asteroid,6), TS(Tok::Satellite,5), TS(Tok::Tracking,8), TS(Tok::Mine,6), TS(Tok::Damage,4), TK(Tok::Squadmate)}
                                                                                                  },

  { "SWX37", "841333100162", "T-70 X-wing Expansion Pack",                    RelGroup::Wave8,    {2015, 9,10}, {2015,12,17},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/9/10/fight-a-bigger-battle/",
                                                                                                   {""},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/12/17/join-the-resistance/"},
                                                                                                  {{Shp::T70XWing, ""}},
                                                                                                   {{"elloasty", Fac::Rebel, Shp::T70XWing}, {"redace", Fac::Rebel, Shp::T70XWing}, {"redsquadronveteran", Fac::Rebel, Shp::T70XWing}, {"bluesquadronnovice", Fac::Rebel, Shp::T70XWing}},
                                                                                                   {{Upg::Elite, "coolhand"}, {Upg::Astromech, "targetingastromech"}, {Upg::Torpedo, "advprotontorpedoes"}, {Upg::Modification, "integratedastromech"}, {Upg::Tech, "weaponsguidance"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TS(Tok::Evade,2), TS(Tok::Focus,2), ID(7), ID(8), TS(Tok::Shield,3), TK(Tok::Stress), TL("H","H")}
                                                                                                  },

  { "SWX38", "841333100179", "TIE/fo Fighter Expansion Pack",                 RelGroup::Wave8,    {2015, 9,10}, {2015,12,17},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/9/10/fight-a-bigger-battle/",
                                                                                                   {""},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/12/17/join-the-resistance/"},
                                                                                                  {{Shp::TIEfoFighter, ""}},
                                                                                                   {{"omegaleader", Fac::Imperial, Shp::TIEfoFighter}, {"zetaleader", Fac::Imperial, Shp::TIEfoFighter}, {"epsilonace", Fac::Imperial, Shp::TIEfoFighter}, {"omegasquadronpilot", Fac::Imperial, Shp::TIEfoFighter}, {"zetasquadronpilot", Fac::Imperial, Shp::TIEfoFighter}, {"epsilonsquadronpilot", Fac::Imperial, Shp::TIEfoFighter}},
                                                                                                   {{Upg::Elite, "juke"}, {Upg::Tech, "commrelay"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(9), ID(10), TK(Tok::Shield), TS(Tok::Stress,2), TL("I","J")}
                                                                                                  },

  { "SWX39", "841333100605", "Ghost Expansion Pack",                          RelGroup::Wave8,    {2015, 7,31}, {2016, 3,17},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/7/31/xwing-out-of-hiding/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/12/4/lothal-rebels-part-one/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2015/12/11/lothal-rebels-part-two/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2016/3/17/a-chance-to-change-things/"},
                                                                                                  {{Shp::VCX100, ""}, {Shp::AttackShuttle, ""}},
                                                                                                   {{"herasyndulla", Fac::Rebel, Shp::VCX100}, {"kananjarrus", Fac::Rebel, Shp::VCX100}, {"chopper", Fac::Rebel, Shp::VCX100}, {"lothalrebel", Fac::Rebel, Shp::VCX100}, {"herasyndulla", Fac::Rebel, Shp::AttackShuttle}, {"sabinewren", Fac::Rebel, Shp::AttackShuttle}, {"ezrabridger", Fac::Rebel, Shp::AttackShuttle}, {"zeborrelios", Fac::Rebel, Shp::AttackShuttle}},
                                                                                                   {{Upg::Elite, "predator"}, {Upg::Torpedo, "advprotontorpedoes"}, {Upg::Turret, "dorsalturret"}, {Upg::Turret, "dorsalturret"}, {Upg::Bomb, "clustermines"}, {Upg::Bomb, "connernet"}, {Upg::Bomb, "thermaldetonators"}, {Upg::Crew, "chopper"}, {Upg::Crew, "zeborrelios"}, {Upg::Crew, "ezrabridger"}, {Upg::Crew, "herasyndulla"}, {Upg::Crew, "kananjarrus"}, {Upg::Crew, "sabinewren"}, {Upg::Title, "ghost"}, {Upg::Title, "phantom"}, {Upg::System, "reinforceddeflectors"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Evade), TK(Tok::Focus), TS(Tok::Shield,8), TK(Tok::Stress), TK(Tok::ConnerNet), TK(Tok::ClusterMine), TK(Tok::ThermalDetonator), TS(Tok::SabotageExplosives,5), TK(Tok::SabotageOperative)}
                                                                                                  },

  { "SWX40", "841333100612", "Inquisitor's TIE Expansion Pack",               RelGroup::Wave8,    {2015, 7,31}, {2016, 3,17},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/7/31/xwing-out-of-hiding/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2015/12/31/hunted-by-the-inquisitor/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2016/3/17/a-chance-to-change-things/"},
                                                                                                  {{Shp::TIEAdvancedPrototype, ""}},
                                                                                                   {{"theinquisitor", Fac::Imperial, Shp::TIEAdvancedPrototype}, {"valenrudor", Fac::Imperial, Shp::TIEAdvancedPrototype}, {"baronoftheempire", Fac::Imperial, Shp::TIEAdvancedPrototype}, {"sienartestpilot", Fac::Imperial, Shp::TIEAdvancedPrototype}},
                                                                                                   {{Upg::Elite, "deadeye"}, {Upg::Missile, "homingmissiles"}, {Upg::Missile, "xx23sthreadtracers"}, {Upg::Modification, "guidancechips"}, {Upg::Title, "tiev1"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(48), ID(49), TS(Tok::Shield,2), TK(Tok::Stress), TL("UU","VV")}
                                                                                                  },

  { "SWX41", "841333100629", "Mist Hunter Expansion Pack",                    RelGroup::Wave8,    {2015, 7,31}, {2016, 3,17},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/7/31/xwing-out-of-hiding/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/1/12/tricks-traps-and-tractor-beams/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2016/3/17/a-chance-to-change-things/"},
                                                                                                  {{Shp::G1A, ""}},
                                                                                                   {{"zuckuss", Fac::Scum, Shp::G1A}, {"4lom", Fac::Scum, Shp::G1A}, {"gandfindsman", Fac::Scum, Shp::G1A}, {"ruthlessfreelancer", Fac::Scum, Shp::G1A}},
                                                                                                   {{Upg::Elite, "adaptability"}, {Upg::Elite, "adaptability"}, {Upg::Crew, "4lom"}, {Upg::Crew, "zuckuss"}, {Upg::Cannon, "tractorbeam"}, {Upg::Title, "misthunter"}, {Upg::System, "electronicbaffle"}, {Upg::System, "electronicbaffle"}, {Upg::Illicit, "cloakingdevice"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(50), ID(51), TS(Tok::Shield,4), TS(Tok::Stress,3), TL("WW","XX"), TK(Tok::Cloak), TS(Tok::TractorBeam,2)}
                                                                                                  },

  { "SWX42", "841333100636", "Punishing One Expansion Pack",                  RelGroup::Wave8,    {2015, 7,31}, {2016, 3,17},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/7/31/xwing-out-of-hiding/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/2/10/payback/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2016/3/17/a-chance-to-change-things/"},
                                                                                                  {{Shp::JumpMaster5000, ""}},
                                                                                                   {{"dengar", Fac::Scum, Shp::JumpMaster5000}, {"teltrevura", Fac::Scum, Shp::JumpMaster5000}, {"manaroo", Fac::Scum, Shp::JumpMaster5000}, {"contractedscout", Fac::Scum, Shp::JumpMaster5000}},
                                                                                                   {{Upg::Elite, "rage"}, {Upg::Elite, "attannimindlink"}, {Upg::Elite, "attannimindlink"}, {Upg::Torpedo, "plasmatorpedoes"}, {Upg::Crew, "gonk"}, {Upg::Crew, "bobafett"}, {Upg::Crew, "dengar"}, {Upg::SalvagedAstromech, "r5p8"}, {Upg::SalvagedAstromech, "overclockedr4"}, {Upg::SalvagedAstromech, "overclockedr4"}, {Upg::Title, "punishingone"}, {Upg::Modification, "guidancechips"}, {Upg::Modification, "guidancechips"}, {Upg::Illicit, "feedbackarray"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(52), ID(53), TS(Tok::Shield,7), TS(Tok::Stress,5), TL("YY","ZZ"), TS(Tok::Microjump,3)}
                                                                                                  },

//   SWX43 - 841333100797 - Blue Bases and Pegs
//   SWX44 - 841333100803 - Red Bases and Pegs
//   SWX45 - 841333100810 - Green Bases and Pegs
//   SWX46 - 841333100827 - Purple Bases and Pegs
//   SWX47 - 841333100834 - Orange Bases and Pegs
//   SWX48 - 841333100940 - Clear Bases and Pegs
//   SWX49 - 841333100766 - Rebel Maneuver Dial Upgrade Kit
//   SWX50 - 841333100773 - Imperial Maneuver Dial Upgrade Kit
//   SWX51 - 841333100780 - Scum Maneuver Dial Upgrade Kit

  { "SWX52", "841333101183", "Imperial Veterans Expansion Pack",              RelGroup::Aces,     {2015,12,14}, {0,0,0},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2015/12/14/rule-the-skies/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/4/15/bombs-away/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2016/4/25/defend-the-empire/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2016/6/24/hone-your-veteran-instincts/"},
                                                                                                  {{Shp::TIEBomber, "Alternate (white stripe)"}, {Shp::TIEDefender, "Alternate (red)"}},
                                                                                                   {{"maarekstele", Fac::Imperial, Shp::TIEDefender}, {"countessryad", Fac::Imperial, Shp::TIEDefender}, {"glaivesquadronpilot", Fac::Imperial, Shp::TIEDefender}, {"glaivesquadronpilot", Fac::Imperial, Shp::TIEDefender}, {"tomaxbren", Fac::Imperial, Shp::TIEBomber}, {"deathfire", Fac::Imperial, Shp::TIEBomber}, {"gammasquadronveteran", Fac::Imperial, Shp::TIEBomber}, {"gammasquadronveteran", Fac::Imperial, Shp::TIEBomber}},
                                                                                                   {{Upg::Bomb, "proximitymines"}, {Upg::Bomb, "clustermines"}, {Upg::Cannon, "tractorbeam"}, {Upg::Crew, "systemsofficer"}, {Upg::Elite, "crackshot"}, {Upg::Title, "tiex7"}, {Upg::Title, "tiex7"}, {Upg::Title, "tied"}, {Upg::Title, "tied"}, {Upg::Title, "tieshuttle"}, {Upg::Title, "tieshuttle"}, {Upg::Modification, "longrangescanners"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Evade), TS(Tok::Focus,2), ID(54), TS(Tok::Shield,3), TS(Tok::Stress,2), TL("AA","BB"), TK(Tok::ProximityMine), TK(Tok::ClusterMine), TK(Tok::TractorBeam), TK(Tok::ImperialDisruptorBomb), TK(Tok::RebelDisruptorBomb)}
                                                                                                  },

  { "SWX53", "841333101350", "ARC-170 Expansion Pack",                        RelGroup::Wave9,    {2016, 6, 2}, {2016, 9,22},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/6/2/stay-in-attack-formation/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/7/11/alliance-overhaul/"},
                                                                                                    ""},
                                                                                                  {{Shp::ARC170, ""}},
                                                                                                   {{"norrawexley", Fac::Rebel, Shp::ARC170}, {"sharabey", Fac::Rebel, Shp::ARC170}, {"thanekyrell", Fac::Rebel, Shp::ARC170}, {"braylenstramm", Fac::Rebel, Shp::ARC170}},
                                                                                                   {{Upg::Elite, "adrenalinerush"}, {Upg::Torpedo, "seismictorpedo"}, {Upg::Crew, "tailgunner"}, {Upg::Crew, "reconspecialist"}, {Upg::Astromech, "r3astromech"}, {Upg::Astromech, "r3astromech"}, {Upg::Title, "allianceoverhaul"}, {Upg::Modification, "vectoredthrusters"}, {Upg::Modification, "vectoredthrusters"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(55), ID(56), TS(Tok::Shield,3), TK(Tok::Stress), TL("CC","DD")}
                                                                                                  },

  { "SWX54", "841333101367", "Special Forces TIE Expansion Pack",             RelGroup::Wave9,    {2016, 6, 2}, {2016, 9,22},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/6/2/stay-in-attack-formation/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/7/18/special-ops/"},
                                                                                                    ""},
                                                                                                  {{Shp::TIEsfFighter, ""}},
                                                                                                   {{"quickdraw", Fac::Imperial, Shp::TIEsfFighter}, {"backdraft", Fac::Imperial, Shp::TIEsfFighter}, {"omegaspecialist", Fac::Imperial, Shp::TIEsfFighter}, {"zetaspecialist", Fac::Imperial, Shp::TIEsfFighter}},
                                                                                                   {{Upg::Elite, "wired"}, {Upg::System, "collisiondetector"}, {Upg::Tech, "sensorcluster"}, {Upg::Tech, "sensorcluster"}, {Upg::Title, "specialopstraining"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(57), ID(58), TS(Tok::Shield,3), TK(Tok::Stress), TL("EE","FF")}
                                                                                                  },

  { "SWX55", "841333101374", "Protectorate Starfighter Expansion Pack",       RelGroup::Wave9,    {2016, 6, 2}, {2016, 9,22},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/6/2/stay-in-attack-formation/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/7/25/fearless/"},
                                                                                                    ""},
                                                                                                  {{Shp::ProtectorateStarfighter, ""}},
                                                                                                   {{"fennrau", Fac::Scum, Shp::ProtectorateStarfighter}, {"oldteroch", Fac::Scum, Shp::ProtectorateStarfighter}, {"kadsolus", Fac::Scum, Shp::ProtectorateStarfighter}, {"concorddawnace", Fac::Scum, Shp::ProtectorateStarfighter}, {"concorddawnveteran", Fac::Scum, Shp::ProtectorateStarfighter}, {"zealousrecruit", Fac::Scum, Shp::ProtectorateStarfighter}},
                                                                                                   {{Upg::Elite, "fearlessness"}, {Upg::Title, "concorddawnprotector"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(59), ID(60), TK(Tok::Stress), TL("GG","HH")}
                                                                                                  },

  { "SWX56", "841333101381", "Shadow Caster Expansion Pack",                  RelGroup::Wave9,    {2016, 6, 2}, {2016, 9,22},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/6/2/stay-in-attack-formation/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/8/1/full-of-surprises/"},
                                                                                                    ""},
                                                                                                  {{Shp::LancerClassPursuitCraft, ""}},
                                                                                                   {{"ketsuonyo", Fac::Scum, Shp::LancerClassPursuitCraft}, {"asajjventress", Fac::Scum, Shp::LancerClassPursuitCraft}, {"sabinewren", Fac::Scum, Shp::LancerClassPursuitCraft}, {"shadowporthunter", Fac::Scum, Shp::LancerClassPursuitCraft}},
                                                                                                   {{Upg::Elite, "veteraninstincts"}, {Upg::Crew, "lattsrazzi"}, {Upg::Crew, "ketsuonyo"}, {Upg::Crew, "ig88d"}, {Upg::Illicit, "riggedcargochute"}, {Upg::Illicit, "blackmarketslicertools"}, {Upg::Illicit, "blackmarketslicertools"}, {Upg::Title, "shadowcaster"}, {Upg::Modification, "countermeasures"}, {Upg::Modification, "tacticaljammer"}, {Upg::Modification, "tacticaljammer"}, {Upg::Modification, "gyroscopictargeting"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(0), ID(1), ID(2), TS(Tok::Shield,3), TK(Tok::Stress), TL("II","JJ"), TK(Tok::TractorBeam), TK(Tok::Cargo), TS(Tok::Intel,4), TK(Tok::Hyperspace)}
                                                                                                  },

  { "SWX57", "841333101411", "Heroes of the Resistance Expansion Pack",       RelGroup::Aces,     {2016, 5, 4}, {0,0,0},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/5/4/these-are-your-first-steps/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/9/6/the-garbage-will-do/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2016/9/19/the-galaxys-greatest/"},
                                                                                                    ""},
                                                                                                  {{Shp::T70XWing, "Alternate (Black One)"}, {Shp::YT1300, "TFA (Rectangular Dish)"}},
                                                                                                   {{"hansolo-swx57", Fac::Rebel, Shp::YT1300}, {"rey", Fac::Rebel, Shp::YT1300}, {"chewbacca-swx57", Fac::Rebel, Shp::YT1300}, {"resistancesympathizer", Fac::Rebel, Shp::YT1300}, {"poedameron-swx57", Fac::Rebel, Shp::T70XWing}, {"niennunb", Fac::Rebel, Shp::T70XWing}, {"snapwexley", Fac::Rebel, Shp::T70XWing}, {"jesspava", Fac::Rebel, Shp::T70XWing}, {"redsquadronveteran", Fac::Rebel, Shp::T70XWing}, {"bluesquadronnovice", Fac::Rebel, Shp::T70XWing}},
                                                                                                   {{Upg::Astromech, "m9g8"}, {Upg::Crew, "finn"}, {Upg::Crew, "rey"}, {Upg::Crew, "hotshotcopilot"}, {Upg::Elite, "snapshot"}, {Upg::Elite, "snapshot"}, {Upg::Elite, "trickshot"}, {Upg::Elite, "trickshot"}, {Upg::Illicit, "burnoutslam"}, {Upg::Illicit, "burnoutslam"}, {Upg::Modification, "integratedastromech"}, {Upg::Modification, "integratedastromech"}, {Upg::Modification, "smugglingcompartment"}, {Upg::Tech, "patternanalyzer"}, {Upg::Tech, "patternanalyzer"}, {Upg::Tech, "primedthrusters"}, {Upg::Title, "blackone"}, {Upg::Title, "millenniumfalcon-swx57"}},
                                                                                                   {},
                                                                                                   {TS(Tok::Focus,2), TS(Tok::Shield,8), TS(Tok::Stress,2), TK(Tok::WeaponsDisabled), TS(Tok::Target,6)}
                                                                                                  },

  { "SWX58", "841333102548", "C-ROC Cruiser Expansion Pack",                  RelGroup::Epic,     {2017, 1, 9}, {2017, 6, 8},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/1/9/bargains-bribes-and-battles/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/2/20/merchant-one/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/3/10/hit-and-run/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/3/27/jabba-the-hutt/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/6/8/showdown-in-the-outer-rim/"},
                                                                                                  {{Shp::CROC, ""}, {Shp::M3AInterceptor, "Alternate (red stripe)"}},
                                                                                                   {{"croccruiser", Fac::Scum, Shp::CROC}, {"genesisred", Fac::Scum, Shp::M3AInterceptor}, {"quinnjast", Fac::Scum, Shp::M3AInterceptor}, {"inaldra", Fac::Scum, Shp::M3AInterceptor}, {"sunnybounder", Fac::Scum, Shp::M3AInterceptor}, {"tansariipointveteran", Fac::Scum, Shp::M3AInterceptor}, {"cartelspacer", Fac::Scum, Shp::M3AInterceptor}},
                                                                                                   {{Upg::Crew, "azmorigan"}, {Upg::Crew, "cikatrovizago"}, {Upg::Crew, "jabbathehutt"}, {Upg::Title, "merchantone"}, {Upg::Title, "brokenhorn"}, {Upg::Title, "insatiableworrt"}, {Upg::Title, "lightscykinterceptor"}, {Upg::Title, "lightscykinterceptor"}, {Upg::Title, "lightscykinterceptor"}, {Upg::Title, "lightscykinterceptor"}, {Upg::Title, "lightscykinterceptor"}, {Upg::Title, "lightscykinterceptor"}, {Upg::Title, "heavyscykinterceptor"}, {Upg::Team, "igrmthugdroids"}, {Upg::Cannon, "arccaster"}, {Upg::Cannon, "arccaster"}, {Upg::Cannon, "arccaster"}, {Upg::Cannon, "arccaster"}, {Upg::Cannon, "arccaster"}, {Upg::Modification, "automatedprotocols"}, {Upg::Modification, "optimizedgenerators"}, {Upg::Modification, "pulsedrayshield"}, {Upg::Modification, "pulsedrayshield"}, {Upg::Modification, "pulsedrayshield"}, {Upg::Modification, "pulsedrayshield"}, {Upg::Modification, "pulsedrayshield"}, {Upg::Hardpoint, "heavylaserturret"}, {Upg::Cargo, "superchargedpowercells"}, {Upg::Cargo, "superchargedpowercells"}, {Upg::Cargo, "quickreleasecargolocks"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(67), TS(Tok::Shield,5), TS(Tok::Stress,3), TS(Tok::Energy,6), TK(Tok::Reinforce), TS(Tok::WeaponsDisabled,2), TS(Tok::Illicit,10), TK(Tok::QuickReleaseContainer)}
                                                                                                  },

  { "SWX59", "841333101909", "Sabine's TIE Fighter Expansion Pack",           RelGroup::Wave10,   {2016, 8, 5}, {2017, 2, 2},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/8/5/ill-show-you-the-dark-side/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/10/17/suppressive-fire/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/2/2/a-shadow-across-the-galaxy/"},
                                                                                                  {{Shp::TIEFighter, "Sabine's TIE Fighter"}},
                                                                                                   {{"ahsokatano", Fac::Rebel, Shp::TIEFighter}, {"sabinewren", Fac::Rebel, Shp::TIEFighter}, {"captainrex", Fac::Rebel, Shp::TIEFighter}, {"zeborrelios", Fac::Rebel, Shp::TIEFighter}},
                                                                                                   {{Upg::Elite, "veteraninstincts"}, {Upg::Crew, "captainrex"}, {Upg::Title, "sabinesmasterpiece"}, {Upg::Illicit, "empdevice"}, {Upg::Modification, "capturedtie"}},
                                                                                                   {Cnd::SuppressiveFire},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), ID(64), TK(Tok::Stress), TS(Tok::Ion,5), TK(Tok::SuppressiveFire)}
                                                                                                  },

  { "SWX60", "841333101916", "Upsilon-class Shuttle Expansion Pack",          RelGroup::Wave10,   {2016, 8, 5}, {2017, 2, 2},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/8/5/ill-show-you-the-dark-side/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/10/24/fanatical-devotion/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/2/2/a-shadow-across-the-galaxy/"},
                                                                                                  {{Shp::UpsilonClassShuttle, ""}},
                                                                                                   {{"kyloren", Fac::Imperial, Shp::UpsilonClassShuttle}, {"majorstridan", Fac::Imperial, Shp::UpsilonClassShuttle}, {"lieutenantdormitz", Fac::Imperial, Shp::UpsilonClassShuttle}, {"starkillerbasepilot", Fac::Imperial, Shp::UpsilonClassShuttle}},
                                                                                                   {{Upg::Elite, "snapshot"}, {Upg::Elite, "snapshot"}, {Upg::Crew, "kyloren"}, {Upg::Crew, "generalhux"}, {Upg::Crew, "operationsspecialist"}, {Upg::Crew, "operationsspecialist"}, {Upg::Tech, "targetingsynchronizer"}, {Upg::Tech, "targetingsynchronizer"}, {Upg::Tech, "hyperwavecommscanner"}, {Upg::Tech, "hyperwavecommscanner"}, {Upg::Title, "kylorensshuttle"}, {Upg::Modification, "ionprojector"}, {Upg::Modification, "ionprojector"}},
                                                                                                   {Cnd::IllShowYouTheDarkSide, Cnd::FanaticalDevotion},
                                                                                                   {TK(Tok::Crit), TS(Tok::Focus,2), ID(65), TS(Tok::Shield,6), TS(Tok::Stress,2), TL("MM","NN"), TK(Tok::ISYTDS), TK(Tok::FanaticalDevotion)}
                                                                                                  },

  { "SWX61", "841333101923", "Quadjumper Expansion Pack",                     RelGroup::Wave10,   {2016, 8, 5}, {2017, 2, 2},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/8/5/ill-show-you-the-dark-side/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/11/22/fly-backward/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/2/2/a-shadow-across-the-galaxy/"},
                                                                                                  {{Shp::Quadjumper, ""}},
                                                                                                   {{"constablezuvio", Fac::Scum, Shp::Quadjumper}, {"sarcoplank", Fac::Scum, Shp::Quadjumper}, {"unkarplutt", Fac::Scum, Shp::Quadjumper}, {"jakkugunrunner", Fac::Scum, Shp::Quadjumper}},
                                                                                                   {{Upg::Elite, "ascoretosettle"}, {Upg::Crew, "unkarplutt"}, {Upg::Crew, "boshek"}, {Upg::Bomb, "thermaldetonators"}, {Upg::Tech, "hyperwavecommscanner"}, {Upg::Illicit, "scavengercrane"}, {Upg::Modification, "spacetugtractorarray"}},
                                                                                                   {Cnd::ADebtToPay},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(66), TK(Tok::Stress), TS(Tok::TractorBeam,5), TK(Tok::ThermalDetonator), TK(Tok::ADebtToPay)}
                                                                                                  },

  { "SWX62", "841333101848", "U-wing Expansion Pack",                         RelGroup::Wave10,   {2016, 9, 2}, {2016,12,15},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/9/2/go-rogue/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2016/11/28/valuable-intel/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/2/2/a-shadow-across-the-galaxy/"},
                                                                                                  {{Shp::UWing, ""}},
                                                                                                   {{"cassianandor", Fac::Rebel, Shp::UWing}, {"bodhirook", Fac::Rebel, Shp::UWing}, {"hefftobber", Fac::Rebel, Shp::UWing}, {"bluesquadronpathfinder", Fac::Rebel, Shp::UWing}},
                                                                                                   {{Upg::Elite, "expertise"}, {Upg::Elite, "expertise"}, {Upg::Crew, "jynerso"}, {Upg::Crew, "cassianandor"}, {Upg::Crew, "bazemalbus"}, {Upg::Crew, "bistan"}, {Upg::Crew, "bodhirook"}, {Upg::Crew, "inspiringrecruit"}, {Upg::Crew, "inspiringrecruit"}, {Upg::Torpedo, "flechettetorpedoes"}, {Upg::System, "sensorjammer"}, {Upg::Title, "pivotwing"}, {Upg::Modification, "stealthdevice"}, {Upg::Modification, "stealthdevice"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TS(Tok::Focus,6), ID(61), ID(62), TS(Tok::Shield,4), TS(Tok::Stress,2), TL("KK","LL")}
                                                                                                  },

  { "SWX63", "841333101855", "TIE Striker Expansion Pack",                    RelGroup::Wave10,   {2016, 9, 2}, {2016,12,15},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2016/9/2/go-rogue/",
												   {"https://www.fantasyflightgames.com/en/news/2016/12/5/a-swift-and-vigilant-defense/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/2/2/a-shadow-across-the-galaxy/"},
                                                                                                  {{Shp::TIEStriker, ""}},
                                                                                                   {{"duchess", Fac::Imperial, Shp::TIEStriker}, {"puresabacc", Fac::Imperial, Shp::TIEStriker}, {"countdown", Fac::Imperial, Shp::TIEStriker}, {"blacksquadronpilot", Fac::Imperial, Shp::TIEStriker}, {"scarifdefender", Fac::Imperial, Shp::TIEStriker}, {"imperialtrainee", Fac::Imperial, Shp::TIEStriker}},
                                                                                                   {{Upg::Elite, "swarmleader"}, {Upg::Title, "adaptiveailerons"}, {Upg::Modification, "lightweightframe"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TS(Tok::Focus,2), ID(63), TK(Tok::Stress)}
                                                                                                  },

  { "SWX64", "841333102821", "Auzituck Gunship Expansion Pack",               RelGroup::Wave11,   {2017, 3,15}, {2017, 7,13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/3/15/independent-operations/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/6/9/defenders-of-kashyyyk/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/7/13/new-approaches-to-battle/"},
                                                                                                  {{Shp::AuzituckGunship, ""}},
                                                                                                   {{"wullffwarro", Fac::Rebel, Shp::AuzituckGunship}, {"lowhhrick", Fac::Rebel, Shp::AuzituckGunship}, {"wookieeliberator", Fac::Rebel, Shp::AuzituckGunship}, {"kashyyykdefender", Fac::Rebel, Shp::AuzituckGunship}},
                                                                                                   {{Upg::Elite, "intimidation"}, {Upg::Elite, "selflessness"}, {Upg::Crew, "breachspecialist"}, {Upg::Crew, "wookieecommandos"}, {Upg::Modification, "hullupgrade"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(68), ID(69), TS(Tok::Shield,3), TK(Tok::Stress), TS(Tok::Reinforce,2)}
                                                                                                  },

  { "SWX65", "841333102838", "Scurrg H-6 Bomber Expansion Pack",              RelGroup::Wave11,   {2017, 3,15}, {2017, 7,13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/3/15/independent-operations/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/6/16/cry-havoc/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/7/13/new-approaches-to-battle/"},
                                                                                                  {{Shp::ScurrgH6Bomber, ""}},
                                                                                                   {{"captainnym", Fac::Scum, Shp::ScurrgH6Bomber}, {"captainnym", Fac::Rebel, Shp::ScurrgH6Bomber}, {"solsixxa", Fac::Scum, Shp::ScurrgH6Bomber}, {"lokrevenant", Fac::Scum, Shp::ScurrgH6Bomber}, {"karthakkpirate", Fac::Scum, Shp::ScurrgH6Bomber}},
                                                                                                   {{Upg::Elite, "lightningreflexes"}, {Upg::Torpedo, "seismictorpedo"}, {Upg::Missile, "cruisemissiles"}, {Upg::Bomb, "bombletgenerator"}, {Upg::System, "minefieldmapper"}, {Upg::Turret, "syncedturret"}, {Upg::Crew, "cadbane"}, {Upg::SalvagedAstromech, "r4e1"}, {Upg::Title, "havoc"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(70), TS(Tok::Shield,5), TK(Tok::Stress), TS(Tok::Bomblet,2)}
                                                                                                  },

  { "SWX66", "841333102845", "TIE Aggressor Expansion Pack",                  RelGroup::Wave11,   {2017, 3,15}, {2017, 7,13},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/3/15/independent-operations/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/5/22/play-the-aggressor/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/7/13/new-approaches-to-battle/"},
                                                                                                  {{Shp::TIEAggressor, ""}},
                                                                                                   {{"lieutenantkestal", Fac::Imperial, Shp::TIEAggressor}, {"doubleedge", Fac::Imperial, Shp::TIEAggressor}, {"onyxsquadronescort", Fac::Imperial, Shp::TIEAggressor}, {"sienarspecialist", Fac::Imperial, Shp::TIEAggressor}},
                                                                                                   {{Upg::Elite, "intensity"}, {Upg::Missile, "unguidedrockets"}, {Upg::Turret, "twinlaserturret"}, {Upg::Turret, "syncedturret"}, {Upg::Modification, "lightweightframe"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(71), ID(72), TK(Tok::Shield), TK(Tok::Stress), TL("QQ","RR")}
                                                                                                  },

  { "SWX67", "841333103682", "Resistance Bomber Expansion Pack",              RelGroup::Wave13,   {2017, 9, 8}, {2017,12, 7},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/9/8/finish-what-you-start/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/11/22/turn-the-tide-of-battle/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/12/8/raw-untamed-power/"},
                                                                                                  {{Shp::BSF17Bomber, ""}},
                                                                                                   {{"crimsonleader", Fac::Rebel, Shp::BSF17Bomber}, {"cobaltleader", Fac::Rebel, Shp::BSF17Bomber}, {"crimsonspecialist", Fac::Rebel, Shp::BSF17Bomber}, {"crimsonsquadronpilot", Fac::Rebel, Shp::BSF17Bomber}},
                                                                                                   {{Upg::Bomb, "ordnancesilos"}, {Upg::Title, "crossfireformation"}, {Upg::System, "trajectorysimulator"}, {Upg::System, "trajectorysimulator"}, {Upg::System, "trajectorysimulator"},  {Upg::Modification, "deflectiveplating"}, {Upg::Bomb, "thermaldetonators"}, {Upg::Bomb, "seismiccharges"}, {Upg::Tech, "advancedoptics"}, {Upg::Tech, "advancedoptics"}, {Upg::Bomb, "connernet"}, {Upg::Bomb, "connernet"}},
                                                                                                   {Cnd::Rattled},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), TS(Tok::Shield,3), TS(Tok::Stress,2), TS(Tok::Ion,2), TK(Tok::SeismicCharge), TS(Tok::ConnerNet,2), TS(Tok::Ordnance,4), TK(Tok::ThermalDetonator), TK(Tok::Rattled)}
                                                                                                  },

  { "SWX68", "841333103699", "TIE Silencer Expansion Pack",                   RelGroup::Wave13,   {2017, 9, 8}, {2017,12, 7},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/9/8/finish-what-you-start/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/11/29/space-superiority/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/12/8/raw-untamed-power/"},
                                                                                                  {{Shp::TIESilencer, ""}},
                                                                                                   {{"kyloren", Fac::Imperial, Shp::TIESilencer}, {"testpilotblackout", Fac::Imperial, Shp::TIESilencer}, {"firstordertestpilot", Fac::Imperial, Shp::TIESilencer}, {"sienarjaemusanalyst", Fac::Imperial, Shp::TIESilencer}},
                                                                                                   {{Upg::Title, "firstordervanguard"}, {Upg::Tech, "advancedoptics"}, {Upg::Tech, "advancedoptics"}, {Upg::Elite, "debrisgambit"}, {Upg::Elite, "debrisgambit"}, {Upg::System, "sensorjammer"}, {Upg::Tech, "primedthrusters"}, {Upg::Tech, "primedthrusters"}, {Upg::Modification, "autothrusters"}, {Upg::Modification, "autothrusters"}, {Upg::Tech, "threattracker"}, {Upg::Tech, "threattracker"}},
                                                                                                   {Cnd::IllShowYouTheDarkSide},
                                                                                                   {TK(Tok::Crit), TS(Tok::Evade,2), TK(Tok::Focus), ID(74), TS(Tok::Shield,2), TS(Tok::Stress,3), TL("SS","TT"), TK(Tok::ISYTDS) }
                                                                                                  },

  { "SWX69", "841333104405", "Alpha-class Star Wing Expansion Pack",          RelGroup::Wave12,   {2017, 8,18}, {2017,12, 7},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/8/21/bring-on-the-alpha-strike/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/10/20/indiscriminate-devastation/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/12/8/raw-untamed-power/"},
                                                                                                  {{Shp::AlphaClassStarWing, ""}},
                                                                                                   {{"majorvynder", Fac::Imperial, Shp::AlphaClassStarWing}, {"lieutenantkarsabi", Fac::Imperial, Shp::AlphaClassStarWing}, {"rhosquadronveteran", Fac::Imperial, Shp::AlphaClassStarWing}, {"nusquadronpilot", Fac::Imperial, Shp::AlphaClassStarWing}},
                                                                                                   {{Upg::Title, "xg1assaultconfiguration"}, {Upg::Title, "os1arsenalloadout"}, {Upg::Modification, "advancedslam"}, {Upg::Cannon, "linkedbattery"}, {Upg::Cannon, "jammingbeam"}, {Upg::Missile, "cruisemissiles"}, {Upg::Elite, "saturationsalvo"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), TS(Tok::Shield,3), TK(Tok::Stress), TL("WW","XX"), TS(Tok::WeaponsDisabled,2), TS(Tok::Jam,2)}
                                                                                                  },

  { "SWX70", "841333104412", "M12-L Kimogila Fighter Expansion Pack",         RelGroup::Wave12,   {2017, 8,18}, {2017,12, 7},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/8/21/bring-on-the-alpha-strike/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/11/3/bullseye/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/12/8/raw-untamed-power/"},
                                                                                                  {{Shp::M12LKimogila, ""}},
                                                                                                   {{"toranikulda", Fac::Scum, Shp::M12LKimogila}, {"dalanoberos", Fac::Scum, Shp::M12LKimogila}, {"cartelexecutioner", Fac::Scum, Shp::M12LKimogila}, {"cartelbrute", Fac::Scum, Shp::M12LKimogila}},
                                                                                                   {{Upg::Title, "enforcer"}, {Upg::Illicit, "deadmansswitch"}, {Upg::Illicit, "contrabandcybernetics"}, {Upg::Illicit, "contrabandcybernetics"}, {Upg::SalvagedAstromech, "r5tk"}, {Upg::Missile, "scramblermissiles"}, {Upg::Missile, "scramblermissiles"}, {Upg::Torpedo, "plasmatorpedoes"}, {Upg::Elite, "saturationsalvo"}, {Upg::Elite, "saturationsalvo"}},
                                                                                                   {},
                                                                                                   { TK(Tok::Crit), TK(Tok::Focus), TS(Tok::Shield,3), TS(Tok::Stress,3), TL("UU","VV"), TS(Tok::WeaponsDisabled,2), TS(Tok::Jam,4)}
                                                                                                  },

// SWX71 ???

  { "SWX72", "841333104429", "Phantom II Expansion Pack",                     RelGroup::Wave12,   {2017, 8,18}, {2017,12, 7},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/8/21/bring-on-the-alpha-strike/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/11/10/coordinate-your-attack/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/12/8/raw-untamed-power/"},
                                                                                                  {{Shp::SheathipedeClassShuttle, ""}},
                                                                                                   {{"fennrau", Fac::Rebel, Shp::SheathipedeClassShuttle}, {"ezrabridger", Fac::Rebel, Shp::SheathipedeClassShuttle}, {"zeborrelios", Fac::Rebel, Shp::SheathipedeClassShuttle}, {"ap5", Fac::Rebel, Shp::SheathipedeClassShuttle}},
                                                                                                   {{Upg::Title, "phantomii"}, {Upg::Title, "ghost-swx72"}, {Upg::Astromech, "flightassistastromech"}, {Upg::Astromech, "chopper"}, {Upg::Crew, "maul"}, {Upg::Crew, "courierdroid"}},
                                                                                                   {},
                                                                                                   {TK(Tok::Crit), TK(Tok::Focus), ID(75), TK(Tok::Shield), TS(Tok::Stress,3), TL("YY","ZZ")}
                                                                                                  },

  { "SWX73", "841333103705", "Guns for Hire Expansion Pack",                  RelGroup::Aces,     {2017, 5,30}, {2017,10,26},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2017/5/30/fearless-and-inventive/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2017/7/24/starviper-mk-ii/",
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/8/15/vaksai/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2017/10/26/when-crime-pays/"},
                                                                                                  {{Shp::Kihraxz, "Alternate (Black Sun)"}, {Shp::StarViper, "Alternate (red)"}},
                                                                                                   {{"viktorhel", Fac::Scum, Shp::Kihraxz}, {"captainjostero", Fac::Scum, Shp::Kihraxz}, {"blacksunace", Fac::Scum, Shp::Kihraxz}, {"cartelmarauder", Fac::Scum, Shp::Kihraxz}, {"dalanoberos", Fac::Scum, Shp::StarViper}, {"thweek", Fac::Scum, Shp::StarViper}, {"blacksunassassin", Fac::Scum, Shp::StarViper}, {"blacksunassassin", Fac::Scum, Shp::StarViper}},
                                                                                                   {{Upg::Missile, "harpoonmissiles"}, {Upg::Missile, "harpoonmissiles"}, {Upg::Title, "vaksai"}, {Upg::Title, "vaksai"}, {Upg::Title, "starvipermkii"}, {Upg::Title, "starvipermkii"}, {Upg::Modification, "pulsedrayshield"}, {Upg::Modification, "pulsedrayshield"}, {Upg::Modification, "stealthdevice"}, {Upg::Modification, "vectoredthrusters"}, {Upg::Illicit, "iondischargers"}, {Upg::Illicit, "iondischargers"}},
                                                                                                   {Cnd::Shadowed, Cnd::Mimicked, Cnd::Harpooned, Cnd::Harpooned},
                                                                                                   {TS(Tok::Focus,2), ID(73), TS(Tok::Shield,2), TS(Tok::Stress,3), TS(Tok::Ion, 4)},
                                                                                                  },

  { "SWX74", "841333105303", "Saw's Renegades Expansion Pack",                RelGroup::Wave14,   {2018, 2,13}, {2018,06,21},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2018/2/13/save-the-dream/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2018/4/24/save-the-rebellion/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2018/6/21/built-on-hope/"},
                                                                                                  {{Shp::UWing, "Alternate (Black & White)"}, {Shp::XWing, "Alternate (Black & White)"}},
                                                                                                   {{"sawgerrera", Fac::Rebel, Shp::UWing}, {"magvayarro", Fac::Rebel, Shp::UWing}, {"benthictwotubes", Fac::Rebel, Shp::UWing}, {"partisanrenegade", Fac::Rebel, Shp::UWing}, {"kullbeesperado", Fac::Rebel, Shp::XWing}, {"leevantenza", Fac::Rebel, Shp::XWing}, {"edriotwotubes", Fac::Rebel, Shp::XWing}, {"cavernangelszealot", Fac::Rebel, Shp::XWing}, {"cavernangelszealot", Fac::Rebel, Shp::XWing}, {"cavernangelszealot", Fac::Rebel, Shp::XWing}},
                                                                                                   {{Upg::Crew, "sawgerrera"}, {Upg::Astromech, "flightassistastromech"}, {Upg::Crew, "magvayarro"}, {Upg::System, "targetingscrambler"}, {Upg::Modification, "servomotorsfoils"}, {Upg::Modification, "servomotorsfoils"}, {Upg::Modification, "servomotorsfoils"}, {Upg::Title, "pivotwing"}, {Upg::Torpedo, "renegaderefit"}, {Upg::Torpedo, "renegaderefit"}, {Upg::Torpedo, "renegaderefit"}, {Upg::System, "thrustcorrector"}, {Upg::System, "thrustcorrector"}, {Upg::Elite, "crackshot"}},
                                                                                                   {Cnd::Scrambled},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), TS(Tok::Shield,6), TK(Tok::Stress), TL("YY", "ZZ"), TK(Tok::WeaponsDisabled), TK(Tok::Scrambled)},
                                                                                                  },

  { "SWX75", "841333105310", "TIE Reaper Expansion Pack",                     RelGroup::Wave14,   {2018, 2,13}, {2018,06,21},
                                                                                                  { "https://www.fantasyflightgames.com/en/news/2018/2/13/save-the-dream/",
                                                                                                   {"https://www.fantasyflightgames.com/en/news/2018/4/30/sowing-fear/"},
                                                                                                    "https://www.fantasyflightgames.com/en/news/2018/6/21/built-on-hope/"},
                                                                                                  {{Shp::TIEReaper, ""}},
                                                                                                   {{"majorvermeil", Fac::Imperial, Shp::TIEReaper}, {"captainferoph", Fac::Imperial, Shp::TIEReaper}, {"vizier", Fac::Imperial, Shp::TIEReaper}, {"scarifbasepilot", Fac::Imperial, Shp::TIEReaper}},
                                                                                                   {{Upg::Crew, "directorkrennic"}, {Upg::Crew, "deathtroopers"}, {Upg::Crew, "systemsofficer"}, {Upg::Crew, "systemsofficer"}, {Upg::Title, "advancedailerons"}, {Upg::Crew, "isbslicer"}, {Upg::Crew, "tacticalofficer"}, {Upg::Crew, "tacticalofficer"}, {Upg::Modification, "multispectralcamouflage"}, {Upg::Elite, "decoy"}},
                                                                                                   {Cnd::OptimizedPrototype},
                                                                                                   {TK(Tok::Crit), TK(Tok::Evade), TK(Tok::Focus), TS(Tok::Shield,2), TK(Tok::Stress), TS(Tok::Jam, 2), TK(Tok::OptimizedPrototype)},
                                                                                                  },

};

}
