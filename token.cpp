#include "token.h"
#include <algorithm>

namespace libxwing {

// token
std::vector<Token> Token::tokens = {
  { Tok::Crit,                  "Critical Hit",            "Crit"           },
  { Tok::Evade,                 "Evade",                   "Evade"          },
  { Tok::Focus,                 "Focus",                   "Focus"          },
  { Tok::ID,                    "ID",                      "ID"             },
  { Tok::Shield,                "Shield",                  "Shield"         },
  { Tok::Stress,                "Stress",                  "Stress"         },
  { Tok::TargetLock,            "Target Lock",             "TargetLock"     },
  { Tok::Ion,                   "Ion",                     "Ion"            },
  { Tok::ProximityMine,         "Proximity Mine",          "ProxMine"       },
  { Tok::SeismicCharge,         "Seismic Charge",          "SeismicCh"      },
  { Tok::Energy,                "Energy",                  "Energy"         },
  { Tok::Reinforce,             "Reinforce",               "Reinforce"      },
  { Tok::ProtonBomb,            "Proton Bomb",             "Proton Bomb"    },
  { Tok::Cloak,                 "Cloak",                   "Cloak"          },
  { Tok::ConnerNet,             "Conner Net",              "Conner Net"     },
  { Tok::IonBomb,               "Ion Bomb",                "Ion Bomb"       },
  { Tok::Ordnance,              "Ordnance",                "Ordnance"       },
  { Tok::WeaponsDisabled,       "Weapons Disabled",        "Weap Disabled"  },
  { Tok::ClusterMine,           "Cluster Mine",            "Clstr Mine"     },
  { Tok::Initiative,            "Initiative",              "Initiative"     },
  { Tok::ThermalDetonator,      "Thermal Detonator",       "Thermal Det"    },
  { Tok::TractorBeam,           "Tractor Beam",            "Tractor Beam"   },
  { Tok::Cargo,                 "Cargo",                   "Cargo"          },
  { Tok::Illicit,               "Illicit",                 "Illicit"        },
  { Tok::QuickReleaseContainer, "Quick-Release Container", "Container"      },
  { Tok::SuppressiveFire,       "Suppressive Fire",        "Supp. Fire"     },
  { Tok::ISYTDS,                "ISYTDS",                  "ISYTDS"         },
  { Tok::FanaticalDevotion,     "Fanatical Devotion",      "Fanatical Dev"  },
  { Tok::ADebtToPay,            "A Debt To Pay",           "A Debt To Pay"  },
  { Tok::Bomblet,               "Bomblet",                 "Bomblet"        },
  { Tok::Jam,                   "Jam",                     "Jam"            },
  { Tok::Rattled,               "Rattled",                 "Rattled"        },
  { Tok::Scrambled,             "Scrambled",               "Scrambled"      },
  { Tok::OptimizedPrototype,    "Optimized Prototype",     "Opt. Proto."    },

  { Tok::Asteroid,              "Asteroid",                "Asteroid"       },
  { Tok::DebrisCloud,           "Debris Cloud",            "Debris"         },

  { Tok::Satellite,             "Satellite",               "Satellite"      },
  { Tok::SenatorsShuttle,       "Senator's Shuttle",       "SenatorShuttle" },
  { Tok::Tracking,              "Tracking",                "Tracking"       },
  { Tok::Container,             "Container",               "Container"      },
  { Tok::Escort,                "Escort",                  "Escort"         },
  { Tok::Smuggler,              "Smuggler",                "Smuggler"       },
  { Tok::Bounty,                "Bounty",                  "Bounty"         },
  { Tok::Control,               "Control",                 "Control"        },
  { Tok::Group,                 "Group",                   "Group"          },
  { Tok::IonBlast,              "Ion Blast",               "Ion Blast"      },
  { Tok::Maneuver,              "Maneuver",                "Maneuver"       },
  { Tok::Mine,                  "Mine",                    "Mine"           },
  { Tok::Role,                  "Role",                    "Role"           },
  { Tok::ScopeHyperspace,       "Scope/Hyperspace",        "Scope/Hypersp"  },
  { Tok::Turbolaser,            "Turbolaser",              "Turbolaser"     },
  { Tok::DisabledShip,          "Disabled Ship",           "DisabledShip"   },
  { Tok::Damage,                "Damage",                  "Damage"         },
  { Tok::Function,              "Function",                "Function"       },
  { Tok::Prototype,             "Prototype",               "Prototype"      },
  { Tok::ActiveCharge,          "Active Charge",           "Active Chrg"    },
  { Tok::ClassECargoContainer,  "Class-E Cargo Container", "Class-E Cargo"  },
  { Tok::DudCharge,             "Dud Charge",              "Dud Charge"     },
  { Tok::Hyperdrive,            "Hyperdrive",              "Hyperdrive"     },
  { Tok::HyperdriveRepair,      "Hyperdrive Repair",       "Hyperdrive Rep" },
  { Tok::Scope,                 "Scope",                   "Scope"          },
  { Tok::Cache,                 "Cache",                   "Cache"          },
  { Tok::EscapePod,             "Escape Pod",              "Escape Pod"     },
  { Tok::ActiveSentry,          "ActiveS entry",           "Act Sentry"     },
  { Tok::DormantSentry,         "Dormant Sentry",          "Dmt Sentry"     },
  { Tok::Signal,                "Signal",                  "Signal"         },
  { Tok::Scanner,               "Scanner",                 "Scanner"        },
  { Tok::Squadmate,             "Squadmate",               "Squadmate"      },
  { Tok::SabotageExplosives,    "Sabotage (Explosives)",   "Sabotage (Exp)" },
  { Tok::SabotageOperative,     "Sabotage (Operative)",    "Sabotage (Op)"  },
  { Tok::Microjump,             "Microjump",               "Microjump"      },
  { Tok::ImperialDisruptorBomb, "Imperial Disruptor Bomb", "Imp Disrpt Bmb" },
  { Tok::RebelDisruptorBomb,    "Rebel Disruptor Bomb",    "Reb Disrpt Bmb" },
  { Tok::Intel,                 "Intel",                   "Intel"          },
  { Tok::Hyperspace,            "Hyperspace",              "Hyperspace"     },
  { Tok::Target,                "Target",                  "Target"         },

};

TokenNotFound::TokenNotFound(Tok t) : runtime_error("Token not found (enum " + std::to_string((int)t) + ")") { }

Token Token::GetToken(Tok tt) {
  for(Token t : Token::tokens) {
    if(t.GetType() == tt) {
      return t;
    }
  }
  throw TokenNotFound(tt);
}

Tok         Token::GetType()      const { return this->type; }
std::string Token::GetName()      const { return this->name; }
std::string Token::GetShortName() const { return this->shortName; }

Token::Token(Tok         t,
             std::string n,
             std::string s)
  : type(t), name(n), shortName(s) { }



// tokens
Tokens::Tokens(Tok t, uint16_t c) : type(t), count(c) { }
Tokens::~Tokens() { }
Tok      Tokens::GetType()  { return this->type;  }
uint16_t Tokens::GetCount() { return this->count; }

TLTokens::TLTokens(std::string l1, std::string l2)
  : Tokens(Tok::TargetLock, 2), tl1(l1), tl2(l2) { }
std::string TLTokens::GetLetter1() { return this->tl1; }
std::string TLTokens::GetLetter2() { return this->tl2; }

IDTokens::IDTokens(uint8_t n)
  : Tokens(Tok::ID, 3), number(n) { }
uint8_t IDTokens::GetNumber() { return this->number; }



// tokencollection
TokenCollection::TokenCollection(std::vector<std::shared_ptr<Tokens>> t) : tokens(t) { }

std::vector<Tok> TokenCollection::GetTokenTypes() {
  std::vector<Tok> ret;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    ret.push_back(t->GetType());
  }
  std::sort(ret.begin(), ret.end());
  auto last = std::unique(ret.begin(), ret.end());
  ret.erase(last, ret.end());
  return ret;
}

uint16_t TokenCollection::GetTokenCount(Tok tt) {
  uint16_t ret=0;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    if(t->GetType() == tt) {
      ret += t->GetCount();
    }
  }
  return ret;
}

std::vector<std::pair<std::string, std::string>> TokenCollection::GetTargetLockTokens() {
  std::vector<std::pair<std::string, std::string>> ret;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    if(t->GetType() == Tok::TargetLock) {
      if(TLTokens *tlt = dynamic_cast<TLTokens*>(t.get())) {
        ret.push_back(std::pair<std::string, std::string>(tlt->GetLetter1(), tlt->GetLetter2()));
      }
    }
  }
  std::sort(ret.begin(), ret.end());
  return ret;
}

std::vector<uint8_t> TokenCollection::GetIdTokens() {
  std::vector<uint8_t> ret;
  for(std::shared_ptr<Tokens>& t : this->tokens) {
    if(t->GetType() == Tok::ID) {
      if(IDTokens* idt = dynamic_cast<IDTokens*>(t.get())) {
        ret.push_back(idt->GetNumber());
      }
    }
  }
  std::sort(ret.begin(), ret.end());
  return ret;
}

}
