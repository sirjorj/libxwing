#include "shared.h"
#include <algorithm>

namespace libxwing {

// Base Size
std::vector<BaseSize> BaseSize::baseSizes = {
  { BSz::None,  "None"  },
  { BSz::Small, "Small" },
  { BSz::Large, "Large" },
  { BSz::Huge,  "Huge"  },
  { BSz::All,   "All"   },
};

BSz operator|(BSz a, BSz b) {
  return static_cast<BSz>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

BSz operator&(BSz a, BSz b) {
  return static_cast<BSz>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

BaseSizeNotFound::BaseSizeNotFound(BSz b) : runtime_error("Base Size not found (enum " + std::to_string((int)b) + ")") { }

BaseSize BaseSize::GetBaseSize(BSz typ) {
  for(BaseSize b : BaseSize::baseSizes) {
    if(b.GetType() == typ) {
      return b;
    }
  }
  throw BaseSizeNotFound(typ);
}

BSz         BaseSize::GetType()      const { return this->type; }
std::string BaseSize::GetName()      const { return this->name; }

BaseSize::BaseSize(BSz         t,
                   std::string n)
  : type(t), name(n) { }



// Actions
std::vector<Action> Action::actions = {
  { Act::None,       "None",        "None"   },
  { Act::Focus,      "Focus",       "Focus"  },
  { Act::TargetLock, "Target Lock", "TLock"  },
  { Act::BarrelRoll, "Barrel Roll", "BRoll"  },
  { Act::Boost,      "Boost",       "Boost"  },
  { Act::Evade,      "Evade",       "Evade"  },
  { Act::Recover,    "Recover",     "Rec"    },
  { Act::Reinforce,  "Reinforce",   "Rforce" },
  { Act::Coordinate, "Coordinate",  "Coord"  },
  { Act::Jam,        "Jam",         "Jam"    },
  { Act::Cloak,      "Cloak",       "Cloak"  },
  { Act::SLAM,       "SLAM",        "SLAM"   },
  { Act::RotateArc,  "Rotate Arc",  "RotArc" },
  { Act::Reload,     "Reload",      "Reload" },
};


Act operator|(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

Act operator&(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

Act operator+(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

Act operator-(Act a, Act b) {
  return static_cast<Act>(static_cast<uint32_t>(a) & ~static_cast<uint32_t>(b));
}

ActionNotFound::ActionNotFound(Act a) : runtime_error("Action not found (enum " + std::to_string((int)a) + ")") { }

Action Action::GetAction(Act typ) {
  for(Action a : Action::actions) {
    if(a.GetType() == typ) {
      return a;
    }
  }
  throw ActionNotFound(typ);
}

Act         Action::GetType()      const { return this->type; }
std::string Action::GetName()      const { return this->name; }
std::string Action::GetShortName() const { return this->shortName; }

Action::Action(Act         t,
               std::string n,
               std::string s)
  : type(t), name(n), shortName(s) { }


void ForEachAction(Act actions, std::function<void(Act)>f) {
  for(int i=0; i<sizeof(Act)*8; i++) {
    uint32_t v = (1<<i) & (uint32_t)actions;
    if(v) {
      Act a = (Act)v;
      f(a);
    }
  }
}



// misc helpers
std::string ToLower(std::string s) {
  std::string ret;
  ret.resize(s.length());
  std::transform(s.begin(), s.end(), ret.begin(), [](char c){ return std::tolower(c); });
  return ret;
}

}
