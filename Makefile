#DEBUG=-g
CPP=clang++
CPPFLAGS=$(DEBUG) -Wall -fPIC -std=c++14

OBJ=jsoncpp.o condition.o damagedeck.o dice.o faction.o glyph.o pilot.o pilotdb.o release.o releasedb.o shared.o ship.o shipdb.o squad.o test.o token.o upgrade.o upgradedb.o

all: libxwing.a

libxwing.a: $(OBJ)
	ar cr libxwing.a $(OBJ)

jsoncpp.o: jsoncpp.cpp json/json.h
condition.o: condition.cpp condition.h
damagedeck.o : damagedeck.cpp damagedeck.h
dice.o: dice.cpp dice.h
faction.o: faction.cpp faction.h
glyph.o: glyph.cpp glyph.h ship.h
pilot.o: pilot.cpp pilot.h shared.h
pilotdb.o: pilotdb.cpp pilot.h shared.h
release.o: release.cpp release.h
releasedb.o: releasedb.cpp release.h
shared.o: shared.cpp shared.h
ship.o: ship.cpp ship.h
shipdb.o: shipdb.cpp ship.h
squad.o: squad.cpp squad.h shared.h
test.o: test.cpp test.h
token.o : token.cpp token.h
upgrade.o: upgrade.cpp upgrade.h shared.h
upgradedb.o: upgradedb.cpp upgrade.h shared.h

clean:
	rm -rf *.o libxwing *~ libxwing.dSYM libxwing.a

.SUFFIXES: .cpp .o
.cpp.o:
	$(CPP) $(CPPFLAGS) -c $<
