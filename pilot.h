#pragma once
#include "damagedeck.h"
#include "faction.h"
#include "shared.h"
#include "ship.h"
#include "upgrade.h"
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

class PilotNotFound : public std::runtime_error {
 public:
  PilotNotFound(std::string pilot, std::string faction, std::string ship);
};

struct PilotSide {
  const std::string pilotName;
  const std::string pilotNameShort;
  const int8_t      energy;
  const int8_t      attack;
  const int8_t      agility;
  const int8_t      hull;
  const int8_t      shield;
  const int8_t      cost;
  Act               actions;
  std::vector<Upg>  possibleUpgrades;
  const bool        hasAbility;
  const std::string text;
};

class Pilot {
public:
  // factories
  static Pilot              GetPilot(std::string pilot, std::string faction, std::string ship);
  static std::vector<Pilot> FindPilot(std::string, Fac f=Fac::All, std::vector<Pilot> src=Pilot::pilots);
  static std::vector<Pilot> GetAllPilots(Fac faction=Fac::All);

  // maintenance
  static void SanityCheck();

  // info
  std::string GetXws()        const;
  std::string GetName()       const;
  std::string GetShortName()  const;
  Faction     GetFaction()    const;
  SubFaction  GetSubFaction() const;
  BaseSize    GetBaseSize()   const;
  Ship        GetShip()       const;
  bool        HasAbility()    const;
  std::string GetText()       const;
  bool        IsUnique()      const;
  uint8_t     GetPerSquad()   const;
  bool        IsUnreleased()  const;

  // stats
  int8_t GetNatSkill() const;
  int8_t GetModSkill() const;
  int8_t GetNatAttack() const;
  int8_t GetModAttack() const;
  int8_t GetNatAgility() const;
  int8_t GetModAgility() const;
  int8_t GetNatHull() const;
  int8_t GetModHull() const;
  int8_t GetNatShield() const;
  int8_t GetModShield() const;
  int8_t GetNatCost() const;
  int8_t GetModCost() const;
  Act GetNatActions() const;
  Act GetModActions() const;
  Maneuvers GetNatManeuvers() const;
  Maneuvers GetModManeuvers() const;
  std::vector<Upg> GetNatPossibleUpgrades() const;
  std::vector<Upg> GetModPossibleUpgrades() const;

  // loadout
  std::vector<Upgrade>& GetAppliedUpgrades();
  std::map<Upg, std::vector<Upgrade>> GetOrganizedUpgrades();
  void ReplaceUpgrade(uint8_t index, std::string type, std::string name);
  void ApplyUpgrade(Upgrade u);

  // overrides
  void OverrideSkill(int8_t o);
  void OverrideAttack(int8_t o);
  void OverrideAgility(int8_t o);
  void OverrideHull(int8_t o);
  void OverrideShield(int8_t o);
  void ClearOverrideSkill();
  void ClearOverrideAttack();
  void ClearOverrideAgility();
  void ClearOverrideHull();
  void ClearOverrideShield();

  // gamestate
  bool IsEnabled() const;
  void Enable();
  void Disable();
  int8_t GetCurShield()  const;
  int8_t GetShieldHits() const;
  void   ShieldUp();
  void   ShieldDn();
  int8_t GetCurHull()  const;
  int8_t GetHullHits() const;
  void   HullUp();
  void   HullDn();
  std::vector<DamageCard> GetCrits() const;
  void AddCrit(DmgCrd c);
  void RemoveCrit(DmgCrd c);
  bool IsDualSided() const;
  void Flip();


 private:
  bool isUnreleased;
  SFa  subFaction;
  BSz  baseSize;
  std::string pilotNameXws;
  Shp    ship;
  uint8_t perSquad;

  int8_t skill;
  std::vector<Upgrade> appliedUpgrades;

  bool   isEnabled;
  int8_t shieldHits;
  int8_t hullHits;
  std::vector<DmgCrd> crits;

  uint8_t curSide;
  std::vector<PilotSide> sides;

  std::experimental::optional<int8_t> oSkill;
  std::experimental::optional<int8_t> oAttack;
  std::experimental::optional<int8_t> oAgility;
  std::experimental::optional<int8_t> oHull;
  std::experimental::optional<int8_t> oShield;

  static std::vector<Pilot> pilots;

  // constructor
  Pilot(SFa         sfa,
        BSz         bs,
        std::string xnam,
        Shp         sh,
        uint8_t     psq,
        int8_t      ps,
        PilotSide   side,
        bool        iu=false);

  Pilot(SFa         sfa,
        BSz         bs,
        std::string xnam,
        Shp         sh,
        uint8_t     psq,
        int8_t      ps,
        PilotSide   s1,
        PilotSide   s2,
        bool        iu=false);
};

}
