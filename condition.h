#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

enum class Cnd {
  FanaticalDevotion,
    IllShowYouTheDarkSide,
    ADebtToPay,
    SuppressiveFire,
    Mimicked,
    Shadowed,
    Harpooned,
    Rattled,
    Scrambled,
    OptimizedPrototype,
    };

class ConditionNotFound : public std::runtime_error {
 public:
  ConditionNotFound(Cnd t);
  ConditionNotFound(std::string xws);
};

class Condition {
 public:
  static Condition GetCondition(Cnd c);
  static Condition GetCondition(std::string xws);
  static std::vector<Condition> GetAllConditions();

  Cnd         GetType();
  bool        IsUnique();
  std::string GetXws();
  std::string GetName();
  std::string GetText();

 private:
  Cnd         type;
  bool        isUnique;
  std::string xws;
  std::string name;
  std::string text;

  static std::vector<Condition> conditions;

  Condition(Cnd         t,
	    bool        u,
	    std::string x,
	    std::string n,
	    std::string txt);
};

}
