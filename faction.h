#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

// Faction
enum class Fac {
  None     = 0x00,
  Rebel    = 0x01,
  Imperial = 0x02,
  Scum     = 0x04,
  All      = 0x07
};

Fac operator|(Fac a, Fac b);
Fac operator&(Fac a, Fac b);

class FactionNotFound : public std::runtime_error {
 public:
  FactionNotFound(Fac f);
  FactionNotFound(std::string xws);
};

class Faction {
 public:
  static Faction GetFaction(Fac s);
  static Faction GetFaction(std::string xws);
  Fac         GetType()      const;
  std::string GetXws()       const;
  std::string GetName()      const;

 private:
  Fac type;
  std::string xws;
  std::string name;

  static std::vector<Faction> factions;

  Faction(Fac         t,
          std::string x,
          std::string n);
};



// SubFaction
enum class SFa {
  None           = 0x00,
  RebelAlliance  = 0x01,
  Resistance     = 0x02,
  GalacticEmpire = 0x04,
  FirstOrder     = 0x08,
  ScumAndVillany = 0x10,
  All            = 0x1F
};

SFa operator|(SFa a, SFa b);
SFa operator&(SFa a, SFa b);

class SubFactionNotFound : public std::runtime_error {
 public:
  SubFactionNotFound(SFa f);
  SubFactionNotFound(std::string xws);
};

class SubFaction {
 public:
  static SubFaction GetSubFaction(SFa s);
  static SubFaction GetSubFaction(std::string xws);
  SFa         GetType()       const;
  Fac         GetParentType() const;
  std::string GetXws()        const;
  std::string GetName()       const;

 private:
  SFa type;
  Fac parentType;
  std::string xws;
  std::string name;

  static std::vector<SubFaction> subFactions;

  SubFaction(SFa         t,
	     Fac         p,
	     std::string x,
	     std::string n);
};

}
