#include "glyph.h"
#include <map>

namespace libxwing {

std::vector<ShipGlyph> ShipGlyph::shipGlyphs = {
  { Shp::AlphaClassStarWing,      "xwing-miniatures-ship-alphaclassstarwing",      "&" },
  { Shp::ARC170,                  "xwing-miniatures-ship-arc170",                  "c" },
  { Shp::AttackShuttle,           "xwing-miniatures-ship-attackshuttle",           "g" },
  { Shp::AuzituckGunship,         "xwing-miniatures-ship-auzituckgunship",         "@" },
  { Shp::AWing,                   "xwing-miniatures-ship-awing",                   "a" },
  { Shp::BSF17Bomber,             "xwing-miniatures-ship-bsf17bomber",             "Z" },
  { Shp::BWing,                   "xwing-miniatures-ship-bwing",                   "b" },
  { Shp::CR90Corvette,            "xwing-miniatures-ship-cr90corvette",            "2" },
  { Shp::CROC,                    "xwing-miniatures-ship-croccruiser",             "5" },
  { Shp::EWing,                   "xwing-miniatures-ship-ewing",                   "e" },
  { Shp::Firespray31,             "xwing-miniatures-ship-firespray31",             "f" },
  { Shp::G1A,                     "xwing-miniatures-ship-g1astarfighter",          "n" },
  { Shp::GozantiClassCruiser,     "xwing-miniatures-ship-gozanticlasscruiser",     "4" },
  { Shp::GR75MediumTransport,     "xwing-miniatures-ship-gr75mediumtransport",     "1" },
  { Shp::HWK290,                  "xwing-miniatures-ship-hwk290",                  "h" },
  { Shp::IG2000,                  "xwing-miniatures-ship-ig2000",                  "i" },
  { Shp::JumpMaster5000,          "xwing-miniatures-ship-jumpmaster5000",          "p" },
  { Shp::Kihraxz,                 "xwing-miniatures-ship-kihraxzfighter",          "r" },
  { Shp::KWing,                   "xwing-miniatures-ship-kwing",                   "k" },
  { Shp::LambdaClassShuttle,      "xwing-miniatures-ship-lambdaclassshuttle",      "l" },
  { Shp::LancerClassPursuitCraft, "xwing-miniatures-ship-lancerclasspursuitcraft", "L" },
  { Shp::M12LKimogila,            "xwing-miniatures-ship-m12lkimogilafighter",     "K" },
  { Shp::M3AInterceptor,          "xwing-miniatures-ship-m3ainterceptor",          "s" },
  { Shp::ProtectorateStarfighter, "xwing-miniatures-ship-protectoratestarfighter", "M" },
  { Shp::Quadjumper,              "xwing-miniatures-ship-quadjumper",              "q" },
  { Shp::RaiderClassCorvette,     "xwing-miniatures-ship-raiderclasscorvette",     "3" },
  { Shp::ScurrgH6Bomber,          "xwing-miniatures-ship-scurrgh6bomber",          "H" },
  { Shp::SheathipedeClassShuttle, "xwing-miniatures-ship-sheathipedeclassshuttle", "%" },
  { Shp::StarViper,               "xwing-miniatures-ship-starviper",               "v" },
  { Shp::T70XWing,                "xwing-miniatures-ship-t70xwing",                "w" },
  { Shp::TIEAdvanced,             "xwing-miniatures-ship-tieadvanced",             "A" },
  { Shp::TIEAdvancedPrototype,    "xwing-miniatures-ship-tieadvancedprototype",    "R" },
  { Shp::TIEAggressor,            "xwing-miniatures-ship-tieaggressor",            "`" },
  { Shp::TIEBomber,               "xwing-miniatures-ship-tiebomber",               "B" },
  { Shp::TIEDefender,             "xwing-miniatures-ship-tiedefender",             "D" },
  { Shp::TIEFighter,              "xwing-miniatures-ship-tiefighter",              "F" },
  { Shp::TIEfoFighter,            "xwing-miniatures-ship-tiefofighter",            "O" },
  { Shp::TIEInterceptor,          "xwing-miniatures-ship-tieinterceptor",          "I" },
  { Shp::TIEPhantom,              "xwing-miniatures-ship-tiephantom",              "P" },
  { Shp::TIEPunisher,             "xwing-miniatures-ship-tiepunisher",             "N" },
  { Shp::TIEReaper,               "xwing-miniatures-ship-tiereaper",               "\u0056" },
  { Shp::TIEsfFighter,            "xwing-miniatures-ship-tiesffighter",            "S" },
  { Shp::TIESilencer,             "xwing-miniatures-ship-tiesilencer",             "$" },
  { Shp::TIEStriker,              "xwing-miniatures-ship-tiestriker",              "T" },
  { Shp::UpsilonClassShuttle,     "xwing-miniatures-ship-upsilonclassshuttle",     "U" },
  { Shp::UWing,                   "xwing-miniatures-ship-uwing",                   "u" },
  { Shp::VCX100,                  "xwing-miniatures-ship-vcx100",                  "G" },
  { Shp::VT49Decimator,           "xwing-miniatures-ship-vt49decimator",           "d" },
  { Shp::XWing,                   "xwing-miniatures-ship-xwing",                   "x" },
  { Shp::YT1300,                  "xwing-miniatures-ship-yt1300",                  "m" },
  { Shp::YT2400,                  "xwing-miniatures-ship-yt2400",                  "o" },
  { Shp::YV666,                   "xwing-miniatures-ship-yv666",                   "t" },
  { Shp::YWing,                   "xwing-miniatures-ship-ywing",                   "y" },
  { Shp::Z95Headhunter,           "xwing-miniatures-ship-z95headhunter",           "z" },
};

ShipGlyphNotFound::ShipGlyphNotFound(Shp s) : runtime_error("Ship not found: (enum " + std::to_string((int)s) + ")") {  }

ShipGlyph ShipGlyph::GetShipGlyph(Shp s) {
  for(ShipGlyph &sg : ShipGlyph::shipGlyphs) {
    if(sg.GetType() == s) {
      return sg;
    }
  }
  throw ShipGlyphNotFound(s);
}

std::string ShipGlyph::GetGlyph(Shp s) {
  try        { return ShipGlyph::GetShipGlyph(s).GetGlyph(); }
  catch(...) { return ""; }
}

std::vector<ShipGlyph> ShipGlyph::GetAllShipGlyphs() { return ShipGlyph::shipGlyphs; }

Shp         ShipGlyph::GetType()     { return this->type; }
std::string ShipGlyph::GetScssName() { return this->scssName; }
std::string ShipGlyph::GetGlyph()    { return this->glyph; }

ShipGlyph::ShipGlyph(Shp t, std::string scss, std::string gly)
  : type(t), scssName(scss), glyph(gly) { }








std::vector<IconGlyph> IconGlyph::iconGlyphs = {
  { Ico::agility,                               "xwing-miniatures-font-agility",                               "",                    "\u005e" },
  { Ico::astromech,                             "xwing-miniatures-font-astromech",                             "{ASTROMECH}",         "\u0041" },
  { Ico::attack,                                "xwing-miniatures-font-attack",                                "",                    "\u0025" },
  { Ico::attack_180,                            "xwing-miniatures-font-attack-180",                            "",                    "\u003e" },
  { Ico::attack_bullseye,                       "xwing-miniatures-font-attack-bullseye",                       "",                    "\u003f" },
  { Ico::attack_bullseye_top_separated,         "xwing-miniatures-font-attack-bullseye-top-separated",         "",                    "\u2019" },
  { Ico::attack_frontback,                      "xwing-miniatures-font-attack-frontback",                      "",                    "\u2019" },
  { Ico::attack_turret,                         "xwing-miniatures-font-attack-turret",                         "",                    "\u0024" },
  { Ico::bankleft,                              "xwing-miniatures-font-bankleft",                              "{LBANK}",             "\u0037" },
  { Ico::bankright,                             "xwing-miniatures-font-bankright",                             "{RBANK}",             "\u0039" },
  { Ico::barrelroll,                            "xwing-miniatures-font-barrelroll",                            "{BARRELROLL}",        "\u0072" },
  { Ico::bomb,                                  "xwing-miniatures-font-bomb",                                  "{BOMB}",              "\u0042" },
  { Ico::boost,                                 "xwing-miniatures-font-boost",                                 "{BOOST}",             "\u0062" },
  { Ico::cannon,                                "xwing-miniatures-font-cannon",                                "{CANNON}",            "\u0043" },
  { Ico::cargo,                                 "xwing-miniatures-font-cargo",                                 "{CARGO}",             "\u0047" },
  { Ico::cloak,                                 "xwing-miniatures-font-cloak",                                 "{CLOAK}",             "\u006b" },
  { Ico::coordinate,                            "xwing-miniatures-font-coordinate",                            "{COORDINATE}",        "\u006f" },
  { Ico::condition,                             "xwing-miniatures-font-condition",                             "",                    "\u00b0" },
  { Ico::condition_outline,                     "xwing-miniatures-font-condition-outline",                     "",                    "\u00c6" },
  { Ico::crew,                                  "xwing-miniatures-font-crew",                                  "{CREW}",              "\u0057" },
  { Ico::crit,                                  "xwing-miniatures-font-crit",                                  "{CRIT}",              "\u0063" },
  { Ico::dalan_bankleft,                        "xwing-miniatures-font-dalan-bankleft",                        "",                    "\u005b" },
  { Ico::dalan_bankright,                       "xwing-miniatures-font-dalan-bankright",                       "",                    "\u005d" },
  { Ico::elite,                                 "xwing-miniatures-font-elite",                                 "{ELITE}",             "\u0045" },
  { Ico::empire,                                "xwing-miniatures-font-empire",                                "",                    "\u0040" },
  { Ico::energy,                                "xwing-miniatures-font-energy",                                "",                    "\u0028" },
  { Ico::epic,                                  "xwing-miniatures-font-epic",                                  "",                    "\u0029" },
  { Ico::evade,                                 "xwing-miniatures-font-evade",                                 "{EVADE}",             "\u0065" },
  { Ico::focus,                                 "xwing-miniatures-font-focus",                                 "{FOCUS}",             "\u0066" },
  { Ico::firstorder,                            "xwing-miniatures-font-firstorder",                            "",                    "\u002b" },
  { Ico::hardpoint,                             "xwing-miniatures-font-hardpoint",                             "{HARDPOINT}",         "\u0048" },
  { Ico::helmet_rebel,                          "xwing-miniatures-font-helmet-rebel",                          "",                    "\u0078" },
  { Ico::helmet_imperial,                       "xwing-miniatures-font-helmet-imperial",                       "",                    "\u0079" },
  { Ico::helmet_scum,                           "xwing-miniatures-font-helmet-scum",                           "",                    "\u007a" },
  { Ico::hit,                                   "xwing-miniatures-font-hit",                                   "{HIT}",               "\u0064" },
  { Ico::hull,                                  "xwing-miniatures-font-hull",                                  "",                    "\u0026" },
  { Ico::ig88d_sloopleft,                       "xwing-miniatures-font-ig88d-sloopleft",                       "",                    "\u0022" },
  { Ico::ig88d_sloopright,                      "xwing-miniatures-font-ig88d-sloopright",                      "",                    "\u0027" },
  { Ico::illicit,                               "xwing-miniatures-font-illicit",                               "{ILLICIT}",           "\u0049" },
  { Ico::jam,                                   "xwing-miniatures-font-jam",                                   "{JAM}",               "\u006a" },
  { Ico::kturn,                                 "xwing-miniatures-font-kturn",                                 "{KTUEN}",             "\u0032" },
  { Ico::missile,                               "xwing-miniatures-font-missile",                               "{MISSILE}",           "\u004d" },
  { Ico::modification,                          "xwing-miniatures-font-modification",                          "",                    "\u006d" },
  { Ico::obstacle_coreasteroid0,                "xwing-miniatures-font-obstacle-coreasteroid0",                "",                    "\u011e" },
  { Ico::obstacle_coreasteroid1,                "xwing-miniatures-font-obstacle-coreasteroid1",                "",                    "\u011f" },
  { Ico::obstacle_coreasteroid2,                "xwing-miniatures-font-obstacle-coreasteroid2",                "",                    "\u011d" },
  { Ico::obstacle_coreasteroid3,                "xwing-miniatures-font-obstacle-coreasteroid3",                "",                    "\u0121" },
  { Ico::obstacle_coreasteroid4,                "xwing-miniatures-font-obstacle-coreasteroid4",                "",                    "\u0120" },
  { Ico::obstacle_coreasteroid5,                "xwing-miniatures-font-obstacle-coreasteroid5",                "",                    "\u011c" },
  { Ico::obstacle_core2asteroid0,               "xwing-miniatures-font-obstacle-core2asteroid0",               "",                    "\u0125" },
  { Ico::obstacle_core2asteroid1,               "xwing-miniatures-font-obstacle-core2asteroid1",               "",                    "\u0128" },
  { Ico::obstacle_core2asteroid2,               "xwing-miniatures-font-obstacle-core2asteroid2",               "",                    "\u0126" },
  { Ico::obstacle_core2asteroid3,               "xwing-miniatures-font-obstacle-core2asteroid3",               "",                    "\u0127" },
  { Ico::obstacle_core2asteroid4,               "xwing-miniatures-font-obstacle-core2asteroid4",               "",                    "\u0129" },
  { Ico::obstacle_core2asteroid5,               "xwing-miniatures-font-obstacle-core2asteroid5",               "",                    "\u012a" },
  { Ico::obstacle_vt49decimatordebris0,         "xwing-miniatures-font-obstacle-vt49decimatordebris0",         "",                    "\u012c" },
  { Ico::obstacle_vt49decimatordebris1,         "xwing-miniatures-font-obstacle-vt49decimatordebris1",         "",                    "\u012d" },
  { Ico::obstacle_vt49decimatordebris2,         "xwing-miniatures-font-obstacle-vt49decimatordebris2",         "",                    "\u0123" },
  { Ico::obstacle_yt2400debris0,                "xwing-miniatures-font-obstacle-yt2400debris0",                "",                    "\u012b" },
  { Ico::obstacle_yt2400debris1,                "xwing-miniatures-font-obstacle-yt2400debris1",                "",                    "\u0124" },
  { Ico::obstacle_yt2400debris2,                "xwing-miniatures-font-obstacle-yt2400debris2",                "",                    "\u0122" },
  { Ico::obstacle_coreasteroid0_outline,        "xwing-miniatures-font-obstacle-coreasteroid0-outline",        "",                    "\u0102" },
  { Ico::obstacle_coreasteroid1_outline,        "xwing-miniatures-font-obstacle-coreasteroid1-outline",        "",                    "\u0103" },
  { Ico::obstacle_coreasteroid2_outline,        "xwing-miniatures-font-obstacle-coreasteroid2-outline",        "",                    "\u0101" },
  { Ico::obstacle_coreasteroid3_outline,        "xwing-miniatures-font-obstacle-coreasteroid3-outline",        "",                    "\u0105" },
  { Ico::obstacle_coreasteroid4_outline,        "xwing-miniatures-font-obstacle-coreasteroid4-outline",        "",                    "\u0104" },
  { Ico::obstacle_coreasteroid5_outline,        "xwing-miniatures-font-obstacle-coreasteroid5-outline",        "",                    "\u0100" },
  { Ico::obstacle_core2asteroid0_outline,       "xwing-miniatures-font-obstacle-core2asteroid0-outline",       "",                    "\u0109" },
  { Ico::obstacle_core2asteroid1_outline,       "xwing-miniatures-font-obstacle-core2asteroid1-outline",       "",                    "\u010c" },
  { Ico::obstacle_core2asteroid2_outline,       "xwing-miniatures-font-obstacle-core2asteroid2-outline",       "",                    "\u010a" },
  { Ico::obstacle_core2asteroid3_outline,       "xwing-miniatures-font-obstacle-core2asteroid3-outline",       "",                    "\u010b" },
  { Ico::obstacle_core2asteroid4_outline,       "xwing-miniatures-font-obstacle-core2asteroid4-outline",       "",                    "\u010d" },
  { Ico::obstacle_core2asteroid5_outline,       "xwing-miniatures-font-obstacle-core2asteroid5-outline",       "",                    "\u010e" },
  { Ico::obstacle_vt49decimatordebris0_outline, "xwing-miniatures-font-obstacle-vt49decimatordebris0-outline", "",                    "\u0110" },
  { Ico::obstacle_vt49decimatordebris1_outline, "xwing-miniatures-font-obstacle-vt49decimatordebris1-outline", "",                    "\u0111" },
  { Ico::obstacle_vt49decimatordebris2_outline, "xwing-miniatures-font-obstacle-vt49decimatordebris2-outline", "",                    "\u0107" },
  { Ico::obstacle_yt2400debris0_outline,        "xwing-miniatures-font-obstacle-yt2400debris0-outline",        "",                    "\u010f" },
  { Ico::obstacle_yt2400debris1_outline,        "xwing-miniatures-font-obstacle-yt2400debris1-outline",        "",                    "\u0108" },
  { Ico::obstacle_yt2400debris2_outline,        "xwing-miniatures-font-obstacle-yt2400debris2-outline",        "",                    "\u0106" },
  { Ico::overlay_180,                           "xwing-miniatures-font-overlay-180",                           "",                    "\u002e" },
  { Ico::overlay_frontback,                     "xwing-miniatures-font-overlay-frontback",                     "",                    "\u002c" },
  { Ico::overlay_turret,                        "xwing-miniatures-font-overlay-turret",                        "",                    "\u002f" },
  { Ico::rebel,                                 "xwing-miniatures-font-rebel",                                 "",                    "\u0021" },
  { Ico::rebel_outline,                         "xwing-miniatures-font-rebel-outline",                         "",                    "\u002d" },
  { Ico::recover,                               "xwing-miniatures-font-recover",                               "",                    "\u0076" },
  { Ico::reinforce,                             "xwing-miniatures-font-reinforce",                             "",                    "\u0069" },
  { Ico::reload,                                "xwing-miniatures-font-reload",                                "",                    "\u003d" },
  { Ico::reversebankleft,                       "xwing-miniatures-font-reversebankleft",                       "",                    "\u004a" },
  { Ico::reversebankright,                      "xwing-miniatures-font-reversebankright",                      "",                    "\u004c" },
  { Ico::reversestraight,                       "xwing-miniatures-font-reversestraight",                       "",                    "\u004b" },
  { Ico::rotatearc,                             "xwing-miniatures-font-rotatearc",                             "",                    "\u0052" },
  { Ico::salvagedastromech,                     "xwing-miniatures-font-salvagedastromech",                     "{SALVAGEDASTROMECH}", "\u0056" },
  { Ico::scum,                                  "xwing-miniatures-font-scum",                                  "",                    "\u0023" },
  { Ico::shield,                                "xwing-miniatures-font-shield",                                "",                    "\u002a" },
  { Ico::slam,                                  "xwing-miniatures-font-slam",                                  "{SLAM}",              "\u0073" },
  { Ico::sloopleft,                             "xwing-miniatures-font-sloopleft",                             "{LSLOOP}",            "\u0031" },
  { Ico::sloopright,                            "xwing-miniatures-font-sloopright",                            "{RSLOOP}",            "\u0033" },
  { Ico::squad_point_cost,                      "xwing-miniatures-font-squad-point-cost",                      "",                    "\u0030" },
  { Ico::stop,                                  "xwing-miniatures-font-stop",                                  "{STATIONARY}",        "\u0035" },
  { Ico::straight,                              "xwing-miniatures-font-straight",                              "{STRAIGHT}",          "\u0038" },
  { Ico::system,                                "xwing-miniatures-font-system",                                "{SYSTEM}",            "\u0053" },
  { Ico::targetlock,                            "xwing-miniatures-font-targetlock",                            "{TARGETLOCK}",        "\u006c" },
  { Ico::team,                                  "xwing-miniatures-font-team",                                  "{TEAM}",              "\u0054" },
  { Ico::tech,                                  "xwing-miniatures-font-tech",                                  "",                    "\u0058" },
  { Ico::title,                                 "xwing-miniatures-font-title",                                 "",                    "\u0074" },
  { Ico::token_cannotattack,                    "xwing-miniatures-font-token-cannotattack",                    "",                    "\u00d6" },
  { Ico::token_cannotattack_outline,            "xwing-miniatures-font-token-cannotattack-outline",            "",                    "\u00ed" },
  { Ico::token_cloak,                           "xwing-miniatures-font-token-cloak",                           "",                    "\u00e5" },
  { Ico::token_cloak_outline,                   "xwing-miniatures-font-token-cloak-outline",                   "",                    "\u00f4" },
  { Ico::token_crit,                            "xwing-miniatures-font-token-crit",                            "",                    "\u00c7" },
  { Ico::token_crit_outline,                    "xwing-miniatures-font-token-crit-outline",                    "",                    "\u00e8" },
  { Ico::token_energy,                          "xwing-miniatures-font-token-energy",                          "",                    "\u00e1" },
  { Ico::token_energy_outline,                  "xwing-miniatures-font-token-energy-outline",                  "",                    "\u00ee" },
  { Ico::token_evade,                           "xwing-miniatures-font-token-evade",                           "",                    "\u00c5" },
  { Ico::token_evade_outline,                   "xwing-miniatures-font-token-evade-outline",                   "",                    "\u00e9" },
  { Ico::token_extramunitions,                  "xwing-miniatures-font-token-extramunitions",                  "",                    "\u00e2" },
  { Ico::token_extramunitions_outline,          "xwing-miniatures-font-token-extramunitions-outline",          "",                    "\u00f1" },
  { Ico::token_focus,                           "xwing-miniatures-font-token-focus",                           "",                    "\u00c4" },
  { Ico::token_focus_outline,                   "xwing-miniatures-font-token-focus-outline",                   "",                    "\u00e7" },
  { Ico::token_illicit,                         "xwing-miniatures-font-token-illicit",                         "",                    "\u00e6" },
  { Ico::token_illicit_outline,                 "xwing-miniatures-font-token-illicit-outline",                 "",                    "\u00f5" },
  { Ico::token_ion,                             "xwing-miniatures-font-token-ion",                             "",                    "\u00e4" },
  { Ico::token_ion_outline,                     "xwing-miniatures-font-token-ion-outline",                     "",                    "\u00f3" },
  { Ico::token_reinforce,                       "xwing-miniatures-font-token-reinforce",                       "",                    "\u00dc" },
  { Ico::token_reinforce_outline,               "xwing-miniatures-font-token-reinforce-outline",               "",                    "\u00ec" },
  { Ico::token_shield,                          "xwing-miniatures-font-token-shield",                          "",                    "\u00d1" },
  { Ico::token_shield_outline,                  "xwing-miniatures-font-token-shield-outline",                  "",                    "\u00eb" },
  { Ico::token_stress,                          "xwing-miniatures-font-token-stress",                          "",                    "\u00c9" },
  { Ico::token_stress_outline,                  "xwing-miniatures-font-token-stress-outline",                  "",                    "\u00ea" },
  { Ico::token_targetlock,                      "xwing-miniatures-font-token-targetlock",                      "",                    "\u00e3" },
  { Ico::token_targetlock_outline,              "xwing-miniatures-font-token-targetlock-outline",              "",                    "\u00f2" },
  { Ico::token_tractorbeam,                     "xwing-miniatures-font-token-tractorbeam",                     "",                    "\u00e0" },
  { Ico::token_tractorbeam_outline,             "xwing-miniatures-font-token-tractorbeam-outline",             "",                    "\u00ef" },
  { Ico::torpedo,                               "xwing-miniatures-font-torpedo",                               "{TORPEDO}",           "\u0050" },
  { Ico::trollleft,                             "xwing-miniatures-font-trollleft",                             "{LTROLL}",            "\u003a" },
  { Ico::trollright,                            "xwing-miniatures-font-trollright",                            "{RTROLL}",            "\u003b" },
  { Ico::turnleft,                              "xwing-miniatures-font-turnleft",                              "{LTURN}",             "\u0034" },
  { Ico::turnright,                             "xwing-miniatures-font-turnright",                             "{RTURN}",             "\u0036" },
  { Ico::turret,                                "xwing-miniatures-font-turret",                                "{TURRET}",            "\u0055" },
  { Ico::unique,                                "xwing-miniatures-font-unique",                                "",                    "\u0075" },
  { Ico::unique_outline,                        "xwing-miniatures-font-unique-outline",                        "",                    "\u2022" },
  // I added this as a way to rturn nothing without throwing an exception
  { Ico::blank,                                 "",                                                            "", ""       },
};

IconGlyphNotFound::IconGlyphNotFound(Ico i) : runtime_error("Icon not found: (enum " + std::to_string((int)i) + ")") {  }



Ico IconGlyph::ActToIco(Act a) {
  switch(a) {
  case Act::Focus:      return Ico::focus;
  case Act::TargetLock: return Ico::targetlock;
  case Act::BarrelRoll: return Ico::barrelroll;
  case Act::Boost:      return Ico::boost;
  case Act::Evade:      return Ico::evade;
  case Act::Recover:    return Ico::recover;
  case Act::Reinforce:  return Ico::reinforce;
  case Act::Coordinate: return Ico::coordinate;
  case Act::Jam:        return Ico::jam;
  case Act::Cloak:      return Ico::cloak;
  case Act::SLAM:       return Ico::slam;
  case Act::RotateArc:  return Ico::rotatearc;
  case Act::Reload:     return Ico::reload;
  default:              return Ico::blank;
  }
}

Ico IconGlyph::FacToIco(Fac f) {
  switch(f) {
  case Fac::Rebel:          return Ico::rebel;
  case Fac::Imperial:       return Ico::empire;
  case Fac::Scum:           return Ico::scum;
  default:                  return Ico::blank;
  }
}

Ico IconGlyph::SFaToIco(SFa s) {
  switch(s) {
  case SFa::RebelAlliance:  return Ico::rebel;
  case SFa::Resistance:     return Ico::rebel_outline;
  case SFa::GalacticEmpire: return Ico::empire;
  case SFa::FirstOrder:     return Ico::firstorder;
  case SFa::ScumAndVillany: return Ico::scum;
  default:                  return Ico::blank;
  }
}


Ico IconGlyph::UpgToIco(Upg u) {
  switch(u) {
  case Upg::Elite:             return Ico::elite;
  case Upg::Astromech:         return Ico::astromech;
  case Upg::Torpedo:           return Ico::torpedo;
  case Upg::Turret:            return Ico::turret;
  case Upg::Missile:           return Ico::missile;
  case Upg::Crew:              return Ico::crew;
  case Upg::Title:             return Ico::title;
  case Upg::Modification:      return Ico::modification;
  case Upg::Cannon:            return Ico::cannon;
  case Upg::Bomb:              return Ico::bomb;
  case Upg::Cargo:             return Ico::cargo;
  case Upg::System:            return Ico::system;
  case Upg::Hardpoint:         return Ico::hardpoint;
  case Upg::Team:              return Ico::team;
  case Upg::SalvagedAstromech: return Ico::salvagedastromech;
  case Upg::Illicit:           return Ico::illicit;
  case Upg::Tech:              return Ico::tech;
  default:                     return Ico::blank;
  }
}

Ico IconGlyph::ExToIco(Upg u) {
  switch(u) {
  case Upg::Torpedo:
  case Upg::Missile:
  case Upg::Bomb:              return Ico::token_extramunitions;
  case Upg::Illicit:           return Ico::token_illicit;
  default:                     return Ico::blank;
  }
}

Ico IconGlyph::AttdToIco(AttD a) {
  switch(a) {
  case AttD::Blank: return Ico::blank;
  case AttD::Focus: return Ico::focus;
  case AttD::Hit:   return Ico::hit;
  case AttD::Crit:  return Ico::crit;
  default:          return Ico::blank;
  }
}

Ico IconGlyph::DefdToIco(DefD d) {
  switch(d) {
  case DefD::Blank: return Ico::blank;
  case DefD::Focus: return Ico::focus;
  case DefD::Evade: return Ico::evade;
  default:          return Ico::blank;
  }
}

Ico IconGlyph::ArcToIco(Arc a) {
  switch(a) {
  case Arc::None:      return Ico::attack;
  case Arc::Turret:    return Ico::attack_180;
  case Arc::FrontRear: return Ico::attack_frontback;
  case Arc::FrontHalf: return Ico::attack_180;
  case Arc::Mobile:    return Ico::attack;
  case Arc::Bullseye:  return Ico::attack_bullseye;
  default:             return Ico::blank;
  }
}

Ico IconGlyph::BrnToIco(Brn b) {
  switch(b) {
  case Brn::LTurn:       return Ico::turnleft;
  case Brn::LBank:       return Ico::bankleft;
  case Brn::Straight:    return Ico::straight;
  case Brn::RBank:       return Ico::bankright;
  case Brn::RTurn:       return Ico::turnright;
  case Brn::KTurn:       return Ico::kturn;
  case Brn::Stationary:  return Ico::stop;
  case Brn::LSloop:      return Ico::sloopleft;
  case Brn::RSloop:      return Ico::sloopright;
  case Brn::LTroll:      return Ico::trollleft;
  case Brn::RTroll:      return Ico::trollright;
  case Brn::RevLBank:    return Ico::reversebankleft;
  case Brn::RevStraight: return Ico::reversestraight;
  case Brn::RevRBank:    return Ico::reversebankright;
  default:               return Ico::blank;
  }
}



IconGlyph   IconGlyph::GetIconGlyph(Ico i) {
  for(IconGlyph &ig : IconGlyph::iconGlyphs) {
    if(ig.GetType() == i) {
      return ig;
    }
  }
  throw IconGlyphNotFound(i);
}

std::string IconGlyph::GetGlyph(Ico i) {
  try        { return IconGlyph::GetIconGlyph(i).GetGlyph(); }
  catch(...) { return ""; }
}

std::string IconGlyph::GetGlyph(std::string ctph) {
  if(ctph == "") { return ""; }
  for(IconGlyph &ig : IconGlyph::iconGlyphs) {
    if(ig.GetCardTextPlaceholder() == ctph) {
      return ig.GetGlyph();
    }
  }
  return ctph;
}

IconGlyph IconGlyph::GetIconGlyph(Act a) { return IconGlyph::GetIconGlyph(ActToIco(a)); }
std::string IconGlyph::GetGlyph(Act a)   { return IconGlyph::GetGlyph(ActToIco(a));     }

IconGlyph IconGlyph::GetIconGlyph(Fac f) { return IconGlyph::GetIconGlyph(FacToIco(f)); }
std::string IconGlyph::GetGlyph(Fac f)   { return IconGlyph::GetGlyph(FacToIco(f));     }

IconGlyph IconGlyph::GetIconGlyph(SFa s) { return IconGlyph::GetIconGlyph(SFaToIco(s)); }
std::string IconGlyph::GetGlyph(SFa s)   { return IconGlyph::GetGlyph(SFaToIco(s));     }

IconGlyph IconGlyph::GetIconGlyph(Upg u) { return IconGlyph::GetIconGlyph(UpgToIco(u)); }
std::string IconGlyph::GetGlyph(Upg u)   { return IconGlyph::GetGlyph(UpgToIco(u));     }

IconGlyph IconGlyph::GetIconGlyphEx(Upg u) { return IconGlyph::GetIconGlyph(ExToIco(u)); }
std::string IconGlyph::GetGlyphEx(Upg u)   { return IconGlyph::GetGlyph(ExToIco(u));     }

IconGlyph    IconGlyph::GetIconGlyph(AttD a) { return IconGlyph::GetIconGlyph(AttdToIco(a)); }
std::string  IconGlyph::GetGlyph(AttD a)     { return IconGlyph::GetGlyph(AttdToIco(a));     }
IconGlyph    IconGlyph::GetIconGlyph(DefD d) { return IconGlyph::GetIconGlyph(DefdToIco(d)); }
std::string  IconGlyph::GetGlyph(DefD d)     { return IconGlyph::GetGlyph(DefdToIco(d));     }

IconGlyph IconGlyph::GetIconGlyph(Arc a) { return IconGlyph::GetIconGlyph(ArcToIco(a)); }
std::string IconGlyph::GetGlyph(Arc a)   { return IconGlyph::GetGlyph(ArcToIco(a));     }

IconGlyph IconGlyph::GetIconGlyph(Brn b) { return IconGlyph::GetIconGlyph(BrnToIco(b)); }
std::string IconGlyph::GetGlyph(Brn b)   { return IconGlyph::GetGlyph(BrnToIco(b));     }

std::vector<IconGlyph> IconGlyph::GetAllIconGlyphs() { return IconGlyph::iconGlyphs; }

Ico         IconGlyph::GetType()                { return this->type; }
std::string IconGlyph::GetScssName()            { return this->scssName; }
std::string IconGlyph::GetCardTextPlaceholder() { return this->cardTextPlaceholder; }
std::string IconGlyph::GetGlyph()               { return this->glyph; }

IconGlyph::IconGlyph(Ico i, std::string scss, std::string ctph, std::string gly)
  : type(i), scssName(scss), cardTextPlaceholder(ctph), glyph(gly) { }

}
