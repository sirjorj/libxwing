#pragma once
#include "shared.h"
#include "ship.h"
#include <experimental/optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

// UpgradeType
enum class Upg {
  Elite             =  1,
  Astromech         =  2,
  Torpedo           =  3,
  Turret            =  4,
  Missile           =  5,
  Crew              =  6,
  Title             =  7,
  Modification      =  8,
  Cannon            =  9,
  Bomb              = 10,
  Cargo             = 11,
  System            = 12,
  Hardpoint         = 13,
  Team              = 14,
  SalvagedAstromech = 15,
  Illicit           = 16,
  Tech              = 17,
};

class UpgradeTypeNotFound : public std::runtime_error {
 public:
  UpgradeTypeNotFound(Upg u);
  UpgradeTypeNotFound(std::string xws);
};

class UpgradeType {
 public:
  static UpgradeType GetUpgradeType(Upg typ);
  static UpgradeType GetUpgradeType(std::string xws);
  Upg         GetType()       const;
  std::string GetXws()        const;
  std::string GetName()       const;
  std::string GetShortName()  const;

 private:
  Upg type;
  std::string xws;
  std::string name;
  std::string shortName;

  static std::vector<UpgradeType> upgradeTypes;

  UpgradeType(Upg         t,
              std::string x,
              std::string n,
              std::string s);
};
Upg operator|(Upg a, Upg b);
Upg operator&(Upg a, Upg b);



// Upgrade
class UpgradeNotFound : public std::runtime_error {
 public:
  UpgradeNotFound(std::string cat, std::string name);
};



class Pilot;
typedef std::function<int8_t          (const Pilot&)> SModifier;
typedef std::function<Act             (const Pilot&)> AModifier;
typedef std::function<std::vector<Upg>(const Pilot&)> UModifier;

struct StatModifiers {
  SModifier skill;
  SModifier attack;
  SModifier agility;
  SModifier hull;
  SModifier shield;
  SModifier cost;
  AModifier addAct;
  AModifier remAct;
  UModifier addUpg;
  UModifier remUpg;
};

typedef std::function<std::vector<std::string>(const Pilot&, const std::vector<Pilot>&)> RestrictionCheck;
typedef std::function<void(Pilot&, std::vector<Pilot>&)> Prepper;


// so far, all maneuver modifications promote to green based on bearing or speed so fill these what with becomes green
struct ManModifiers {
  std::vector<Brn>    bearings;
  std::vector<int8_t> speeds;
};



struct AttackStats {
  const uint8_t attack;
  const uint8_t minRange;
  const uint8_t maxRange;
};

struct UpgradeSide {
  const StatModifiers statModifier;
  const ManModifiers  manModifier;
  const std::string   name;
  const std::string   nameShort;
  const std::experimental::optional<AttackStats> attackStats;
  const std::experimental::optional<uint8_t>     energyLimit;
  const std::experimental::optional<int8_t>      energyModifier;
  const std::string   text;
};



class Upgrade {
 public:
  // factories
  static Upgrade GetUpgrade(std::string category, std::string name);
  static std::vector<Upgrade> FindUpgrade(std::string u, std::vector<Upgrade> src=Upgrade::upgrades);
  static std::vector<Upgrade> GetAllUpgrades();

  // maintenance
  static void SanityCheck();

  // info
  std::string GetXws()             const;
  std::string GetName()            const;
  std::string GetShortName()       const;
  UpgradeType GetType()            const;
  std::string GetRestrictionText() const;
  std::string GetText()            const;
  bool        IsUnique()           const;
  bool        IsLimited()          const;
  uint8_t     GetPerSquad()        const;
  uint8_t     GetPerPilot()        const;
  bool        IsUnreleased()       const;
  std::experimental::optional<AttackStats> GetAttackStats()    const;
  std::experimental::optional<uint8_t>     GetEnergyLimit()    const;
  std::experimental::optional<int8_t>      GetEnergyModifier() const;

  // stats
  std::vector<Upg> GetReqSlots()         const;
  int8_t           GetCost()             const;
  StatModifiers    GetStatModifier()     const;
  ManModifiers     GetManModifier()      const;
  RestrictionCheck GetRestrictionCheck() const;
  uint8_t          GetExtras()           const;
  void             SetExtras(uint8_t);
  void             ExtraUp();
  void             ExtraDn();

  // gamestate
  bool IsEnabled() const;
  void Enable();
  void Disable();
  bool IsDualSided() const;
  void Flip();

  Prepper GetPrepper();

 private:
  bool             isUnreleased;
  bool             isEnabled;
  std::string      nameXws;
  Upg              type;
  std::vector<Upg> slots;
  int8_t           cost;
  uint8_t          perSquad;
  uint8_t          perPilot;
  RestrictionCheck restrictionCheck;
  std::string      restrictionText;
  Prepper          prepper;
  uint8_t          extras;

  uint8_t curSide;
  std::vector<UpgradeSide> sides;
  
  static std::vector<Upgrade> upgrades;

  Upgrade(std::string      nx,
          Upg              typ,
          std::vector<Upg> slt,
          int8_t           cst,
          uint8_t          psq,
          uint8_t          ppi,
          RestrictionCheck rc,
	  std::string      rt,
	  Prepper          p,
          UpgradeSide      s1,
          bool             iu=false);

  Upgrade(std::string      nx,
          Upg              typ,
          std::vector<Upg> slt,
          int8_t           cst,
          uint8_t          psq,
          uint8_t          ppi,
          RestrictionCheck rc,
	  std::string      rt,
	  Prepper          p,
          UpgradeSide      s1,
          UpgradeSide      s2,
          bool             iu=false);
};

}
