#include "ship.h"
#include <map>

namespace libxwing {

// PrimaryArc
PrimaryArcNotFound::PrimaryArcNotFound(Arc a) : runtime_error("Primary Arc not found (enum " + std::to_string((int)a) + ")") { }

std::vector<PrimaryArc> PrimaryArc::primaryArcs = {
  { Arc::None,      "None",      "None",      },
  { Arc::Front,     "Front",     "Front",     },
  { Arc::Turret,    "Turret",    "Turret",    },
  { Arc::FrontRear, "FrontRear", "FrontRear", },
  { Arc::FrontHalf, "FrontHalf", "FrontHalf", },
  { Arc::Mobile,    "Mobile",    "Mobile",    },
  { Arc::Bullseye,  "Bullseye",  "Bullseye",  },
};

PrimaryArc PrimaryArc::GetPrimaryArc(Arc typ) {
  for(PrimaryArc a : PrimaryArc::primaryArcs) {
    if(a.GetType() == typ) {
      return a;
    }
  }
  throw PrimaryArcNotFound(typ);
}

Arc         PrimaryArc::GetType()      const { return this->type; }
std::string PrimaryArc::GetName()      const { return this->name; }
std::string PrimaryArc::GetShortName() const { return this->shortName; }

PrimaryArc::PrimaryArc(Arc         t,
                       std::string n,
                       std::string s)
  : type(t), name(n), shortName(s) { }



// Bearing
BearingNotFound::BearingNotFound(Brn b)         : runtime_error("Bearing not found (enum " + std::to_string((int)b) + ")") { }
BearingNotFound::BearingNotFound(std::string t) : runtime_error("Bearing not found (" + t + ")") { }

std::vector<Bearing> Bearing::bearings = {
  { Brn::LTurn,       "Left Turn",           "L Turn",   "↰", "{LTURN}"      },
  { Brn::LBank,       "Left Bank",           "L Bank",   "↖", "{LBANK}"      },
  { Brn::Straight,    "Straight",            "Straight", "↑", "{STRAIGHT}"   },
  { Brn::RBank,       "Right Bank",          "R Bank",   "↗", "{RBANK}"      },
  { Brn::RTurn,       "Right Turn",          "R Turn",   "↱", "{RTURN}"      },
  { Brn::KTurn,       "Koiogran Turn",       "K-Turn",   "K", "{KTURN}"      },
  { Brn::Stationary,  "Stationary",          "Stop",     "■", "{STATIONARY}" },
  { Brn::LSloop,      "Left Segnor’s Loop",  "L Sloop",  "↖", "{LSLOOP}"     },
  { Brn::RSloop,      "Right Segnor’s Loop", "R Sloop",  "↗", "{RSLOOP}"     },
  { Brn::LTroll,      "Left Tallon Roll",    "L TRoll",  "↰", "{LTROLL}"     },
  { Brn::RTroll,      "Right Tallon Roll",   "R TRoll",  "↱", "{RTROLL}"     },
  { Brn::RevLBank,    "Reverse Left Bank",   "Rev LB",   "↙", "{REVLBANK}"   },
  { Brn::RevStraight, "Reverse Straight",    "Rev Str",  "↓", "{REVSTR}"     },
  { Brn::RevRBank,    "Reverse Right Bank",  "Rev RB",   "↘", "{REVRBANK}"   },
};

Bearing Bearing::GetBearing(Brn typ) {
  for(Bearing b : Bearing::bearings) {
    if(b.GetType() == typ) {
      return b;
    }
  }
  throw BearingNotFound(typ);
}

Bearing Bearing::GetBearing(std::string text) {
  for(Bearing b : Bearing::bearings) {
    if(b.GetText() == text) {
      return b;
    }
  }
  throw BearingNotFound(text);
}

Brn         Bearing::GetType()      const { return this->type; }
std::string Bearing::GetName()      const { return this->name; }
std::string Bearing::GetShortName() const { return this->shortName; }
std::string Bearing::GetCharacter() const { return this->character; }
std::string Bearing::GetText()      const { return this->text; }

Bearing::Bearing(Brn         b,
                 std::string n,
                 std::string s,
                 std::string c,
                 std::string t)
  : type(b), name(n), shortName(s), character(c), text(t) { }



bool FindManeuver(Maneuvers maneuvers, uint8_t speed, Brn bearing, Maneuver &maneuver) {
  for(auto m : maneuvers) {
    if((m.speed == speed) && (m.bearing == bearing)) {
      maneuver = m;
      return true;
    }
  }
  return false;
}



// Ship
ShipNotFound::ShipNotFound(Shp s)           : runtime_error("Ship not found (enum " + std::to_string((int)s) + ")") { }
ShipNotFound::ShipNotFound(std::string xws) : runtime_error("Ship not found '" + xws + "'") { }

Ship Ship::GetShip(Shp typ) {
  for(Ship s : Ship::ships) {
    if(s.GetType() == typ) {
      return s;
    }
  }
  throw ShipNotFound(typ);
}

Ship Ship::GetShip(std::string xws) {
  for(Ship s : Ship::ships) {
    if(s.GetXws() == xws) {
      return s;
    }
  }
  throw ShipNotFound(xws);
}

std::vector<Ship> Ship::FindShip(std::string s) {
  std::vector<Ship> ret;
  std::string ss = ToLower(s); // searchString
  for(Ship &sh : Ship::ships) {
    if((ToLower(sh.GetName()).find(ss)      != std::string::npos) ||
       (ToLower(sh.GetShortName()).find(ss) != std::string::npos) ||
       (ToLower(sh.GetXws()).find(ss)   != std::string::npos)
       ) {
      ret.push_back(sh);
    }
  }
  return ret;
}

std::vector<Ship> Ship::GetAllShips() { return Ship::ships; }

Shp         Ship::GetType()       const { return this->type; }
std::string Ship::GetXws()        const { return this->xws; }
std::string Ship::GetName()       const { return this->name; }
std::string Ship::GetShortName()  const { return this->shortName; }
Maneuvers   Ship::GetManeuvers()  const { return this->maneuvers; }
PrimaryArc  Ship::GetPrimaryArc() const {return PrimaryArc::GetPrimaryArc(this->arc); }

Ship::Ship(Shp         t,
           std::string x,
           std::string n,
           std::string s,
           Maneuvers   m,
           Arc         a)
  : type(t), xws(x), name(n), shortName(s), maneuvers(m), arc(a) { }

}
