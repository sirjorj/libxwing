#pragma once
#include "condition.h"
#include "faction.h"
#include "ship.h"
#include "shared.h"
#include "token.h"
#include "upgrade.h"
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

class ReleaseNotFound : public std::runtime_error {
 public:
  ReleaseNotFound(std::string sku);
};

struct RelShip {
  Shp         ship;
  std::string desc; // leave blank for standaard, add text if alternate paint (ex. aces)
};

struct RelPilot {
  std::string name;
  Fac         faction;
  Shp         ship;
};

struct RelUpgrade {
  Upg type;
  std::string name;
};

struct RelCondition {
  std::string name;
};

enum class RelGroup {
  CoreSets,
  Epic,
  Aces,
  Wave1,
  Wave2,
  Wave3,
  Wave4,
  Wave5,
  Wave6,
  Wave7,
  Wave8,
  Wave9,
  Wave10,
  Wave11,
  Wave12,
  Wave13,
  Wave14,
};

struct RelDate {
  unsigned short year;
  unsigned char  month;
  unsigned char  date;
};

struct RelUrls {
  std::string              announcement;
  std::vector<std::string> previews;
  std::string              release;
};

class Release{
 public:
  static std::vector<Release> GetPilot(std::string name, Fac fac, Shp shp);
  static std::vector<Release> GetUpgrade(Upg type, std::string name);
  static Release GetRelease(std::string sku);
  static std::vector<Release> GetAllReleases();

  bool                     IsUnreleased();
  std::string              GetSku();
  std::string              GetIsbn();
  std::string              GetName();
  RelGroup                 GetGroup();
  RelDate                  GetAnnounceDate();
  RelDate                  GetReleaseDate();
  std::string              GetAnnouncementUrl();
  std::vector<std::string> GetPreviewUrls();
  std::string              GetReleaseUrl();
  std::vector<RelShip>     GetShips();
  std::vector<RelPilot>    GetPilots();
  std::vector<RelUpgrade>  GetUpgrades();
  std::vector<Cnd>         GetConditions();
  TokenCollection          GetTokens();

 private:
  std::string sku;
  std::string isbn;
  std::string name;
  RelGroup group;
  RelDate announceDate;
  RelDate releaseDate;
  RelUrls urls;
  std::vector<RelShip>    ships;
  std::vector<RelPilot>   pilots;
  std::vector<RelUpgrade> upgrades;
  std::vector<Cnd>        conditions;
  TokenCollection         tokens;
  bool                    isUnreleased;

  static std::vector<Release> releases;

  Release(std::string               sku,
          std::string               isbn,
          std::string               nam,
          RelGroup                  gr,
          RelDate                   ad,
          RelDate                   rd,
          RelUrls                   url,
          std::vector<RelShip>      shps,
          std::vector<RelPilot>     plts,
          std::vector<RelUpgrade>   upgs,
          std::vector<Cnd>          cond,
          std::vector<std::shared_ptr<Tokens>> tok,
          bool                      iu=false);
};

}
