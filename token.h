#pragma once
#include <memory>
#include <string>
#include <vector>
#include <stdint.h>

namespace libxwing {

// token
enum class Tok : uint16_t {
  Crit              =   0,
  Evade,
  Focus,
  ID,
  Shield,
  Stress,
  TargetLock,
  Ion,
  ProximityMine,
  SeismicCharge,
  Energy,
  Reinforce,
  ProtonBomb,
  Cloak,
  ConnerNet,
  IonBomb,
  Ordnance,
  WeaponsDisabled,
  ClusterMine,
  Initiative,
  ThermalDetonator,
  TractorBeam,
  Cargo,
  Illicit,
  QuickReleaseContainer,
  SuppressiveFire,
  ISYTDS,
  FanaticalDevotion,
  ADebtToPay,
  Bomblet,
  Jam,
  Rattled,
  Scrambled,
  OptimizedPrototype,

  Asteroid          = 128,
  DebrisCloud,

  Satellite         = 256,
  SenatorsShuttle,
  Tracking,
  Container,
  Escort,
  Smuggler,
  Bounty,
  Control,
  Group,
  IonBlast,
  Maneuver,
  Mine,
  Role,
  ScopeHyperspace,
  Turbolaser,
  DisabledShip,
  Damage,
  Function,
  Prototype,
  ActiveCharge,
  ClassECargoContainer,
  DudCharge,
  Hyperdrive,
  HyperdriveRepair,
  Scope,
  Cache,
  EscapePod,
  ActiveSentry,
  DormantSentry,
  Signal,
  Scanner,
  Squadmate,
  SabotageExplosives,
  SabotageOperative,
  Microjump,
  ImperialDisruptorBomb,
  RebelDisruptorBomb,
  Intel,
  Hyperspace,
  Target,
};

class TokenNotFound : public std::runtime_error {
 public:
  TokenNotFound(Tok t);
};

class Token {
 public:
  static Token GetToken(Tok t);
  Tok         GetType()      const;
  std::string GetName()      const;
  std::string GetShortName() const;

 private:
  Tok type;
  std::string name;
  std::string shortName;

  static std::vector<Token> tokens;

  Token(Tok         t,
        std::string name,
        std::string shortName);
};



// tokens
class Tokens {
 public:
  Tokens(Tok t, uint16_t c);
  virtual ~Tokens();
  Tok GetType();
  uint16_t GetCount();
 private:
  Tok type;
  uint16_t count;
};

class TLTokens : public Tokens {
 public:
  TLTokens(std::string l1, std::string l2);
  std::string GetLetter1();
  std::string GetLetter2();
 private:
  std::string tl1, tl2;
};

class IDTokens : public Tokens {
 public:
  IDTokens(uint8_t n);
  uint8_t GetNumber();
 private:
  uint8_t number;
};



// tokencollection
class TokenCollection {
 public:
  TokenCollection(std::vector<std::shared_ptr<Tokens>> t);
  std::vector<Tok> GetTokenTypes();
  uint16_t GetTokenCount(Tok t);
  std::vector<std::pair<std::string, std::string>> GetTargetLockTokens();
  std::vector<uint8_t> GetIdTokens();
 private:
  std::vector<std::shared_ptr<Tokens>> tokens;
};

}
