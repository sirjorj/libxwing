#include "pilot.h"
#include "shared.h"
#include <algorithm>

namespace libxwing {

PilotNotFound::PilotNotFound(std::string p, std::string f, std::string s) : runtime_error("Pilot not found: '" + p + "/" + f + "/" + s + "'") { }

// factories
Pilot Pilot::GetPilot(std::string pilot, std::string faction, std::string ship) {
  for(Pilot &ps : Pilot::pilots) {
    if((ps.GetXws() == pilot) && (ps.GetFaction().GetType() <= Faction::GetFaction(faction).GetType()) && (ps.GetShip().GetXws() == ship)) {
      return ps;
    }
  }
  throw PilotNotFound(pilot, faction, ship);
}

std::vector<Pilot> Pilot::FindPilot(std::string p, Fac f, std::vector<Pilot> src) {
  std::vector<Pilot> ret;
  std::string ss = ToLower(p); // searchString
  for(Pilot &ps : src) {
    if((ps.GetFaction().GetType() & f) == ps.GetFaction().GetType()) {
      if((ToLower(ps.GetXws()).find(ss)       != std::string::npos) ||
	 (ToLower(ps.GetName()).find(ss)      != std::string::npos) ||
	 (ToLower(ps.GetShortName()).find(ss) != std::string::npos)
	 ) {
	ret.push_back(ps);
      }
    }
  }
  return ret;
}

std::vector<Pilot> Pilot::GetAllPilots(Fac faction) {

  if(faction == Fac::All) { return Pilot::pilots; }
  std::vector<Pilot> ret;
  for(Pilot p : Pilot::pilots) {
    if(p.GetFaction().GetType() == faction) {
      ret.push_back(p);
    }
  }
  return ret;
}

// maintenance
void Pilot::SanityCheck() {
  typedef std::tuple<std::string, std::string, std::string> Entry;
  std::vector<Entry> entries;
  std::vector<Entry> dupes;
  int counter=0;
  printf("Checking Pilots");
  for(Pilot p : pilots) {
    counter++;
    Entry e = Entry{p.GetXws(), p.GetFaction().GetXws(), p.GetShip().GetXws() };
    if(std::find(entries.begin(), entries.end(), e) == entries.end()) {
      printf("."); fflush(stdout);
    } else {
      dupes.push_back(e);
      printf("X"); fflush(stdout);
    }
    entries.push_back(e);
  }
  printf("DONE\n");
  printf("Pilots: %zu\n", entries.size());
  printf("Dupes: %zu\n", dupes.size());
  if(dupes.size()) {
    for(Entry e : dupes) {
      printf("  %s - %s - %s\n", std::get<0>(e).c_str(), std::get<1>(e).c_str(), std::get<2>(e).c_str());
    }
  }
}

// info
std::string Pilot::GetXws()        const { return this->pilotNameXws; }
std::string Pilot::GetName()       const { return this->sides[this->curSide].pilotName; }
std::string Pilot::GetShortName()  const { return this->sides[this->curSide].pilotNameShort; }
Faction     Pilot::GetFaction()    const { return Faction::GetFaction(this->GetSubFaction().GetParentType()); };
SubFaction  Pilot::GetSubFaction() const { return SubFaction::GetSubFaction(this->subFaction); }
BaseSize    Pilot::GetBaseSize()   const { return BaseSize::GetBaseSize(this->baseSize); }
Ship        Pilot::GetShip()       const { return Ship::GetShip( this->ship); }
bool        Pilot::HasAbility()    const { return this->sides[this->curSide].hasAbility; }
std::string Pilot::GetText()       const { return this->sides[this->curSide].text; }
bool        Pilot::IsUnique()      const { return (this->perSquad > 0); }
uint8_t     Pilot::GetPerSquad()   const { return this->perSquad; }
bool        Pilot::IsUnreleased()  const { return this->isUnreleased; }

// stats
int8_t Pilot::GetNatSkill() const { return this->skill; }
int8_t Pilot::GetModSkill() const {
  if(oSkill) { return *oSkill; }
  int8_t skill = this->GetNatSkill();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      skill += u.GetStatModifier().skill(*this);
    }
  }
  return skill;
}

int8_t Pilot::GetNatAttack() const { return this->sides[this->curSide].attack; }
int8_t Pilot::GetModAttack() const {
  if(oAttack) { return *oAttack; }
  int8_t attack = this->GetNatAttack();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      attack += u.GetStatModifier().attack(*this);
    }
  }
  return attack;
}

int8_t Pilot::GetNatAgility() const { return this->sides[this->curSide].agility; }
int8_t Pilot::GetModAgility() const {
  if(oAgility) { return *oAgility; }
  int8_t agility = this->GetNatAgility();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      agility += u.GetStatModifier().agility(*this);
    }
  }
  return agility;
}

int8_t Pilot::GetNatHull() const { return this->sides[this->curSide].hull; }
int8_t Pilot::GetModHull() const {
  if(oHull) { return *oHull; }
  int8_t hull = this->GetNatHull();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      hull += u.GetStatModifier().hull(*this);
    }
  }
  return hull;
}

int8_t Pilot::GetNatShield() const { return this->sides[this->curSide].shield; }
int8_t Pilot::GetModShield() const {
  if(oShield) { return *oShield; }
  int8_t shield = this->GetNatShield();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      shield += u.GetStatModifier().shield(*this);
    }
  }
  return shield;
}

int8_t Pilot::GetNatCost() const { return this->sides[this->curSide].cost; }
int8_t Pilot::GetModCost() const {
  int8_t cost = this->GetNatCost();
  for(Upgrade u : this->appliedUpgrades) {
    cost += u.GetCost();
    cost += u.GetStatModifier().cost(*this);
  }
  return cost;
}

Act Pilot::GetNatActions() const { return this->sides[this->curSide].actions; }
Act Pilot::GetModActions() const {
  Act act = this->GetNatActions();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      act = act + u.GetStatModifier().addAct(*this);
      act = act - u.GetStatModifier().remAct(*this);
    }
  }
  return act;
}

Maneuvers Pilot::GetNatManeuvers() const { return this->GetShip().GetManeuvers(); }
Maneuvers Pilot::GetModManeuvers() const {
  Maneuvers man = this->GetShip().GetManeuvers();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      for(Brn b : u.GetManModifier().bearings) {
        for(Maneuver &m : man) {
          if(m.bearing == b) {
            m.difficulty = Difficulty::Green;
          }
        }
      }
      for(int8_t s : u.GetManModifier().speeds) {
        for(Maneuver &m : man) {
          if(m.speed == s) {
            m.difficulty = Difficulty::Green;
          }
        }
      }
    }
  }
  return man;
}

std::vector<Upg> Pilot::GetNatPossibleUpgrades() const { return this->sides[this->curSide].possibleUpgrades; }
std::vector<Upg> Pilot::GetModPossibleUpgrades() const {
  std::vector<Upg> upg = this->GetNatPossibleUpgrades();
  for(Upgrade u : this->appliedUpgrades) {
    if(u.IsEnabled()) {
      for(Upg x : u.GetStatModifier().addUpg(*this)) { upg.push_back(x); }
      for(Upg x : u.GetStatModifier().remUpg(*this)) {
	auto it = std::find(upg.rbegin(), upg.rend(), x);
	if(it != upg.rend()) {
	  upg.erase(std::next(it).base());
	}
      }
    }
  }
  return upg;
}

// loadout
std::vector<Upgrade>& Pilot::GetAppliedUpgrades() { return this->appliedUpgrades; }

std::map<Upg, std::vector<Upgrade>> Pilot::GetOrganizedUpgrades() {
  std::map<Upg, std::vector<Upgrade>> ret;
  std::vector<Upgrade> unhandled = this->GetAppliedUpgrades();
  std::vector<Upgrade>::iterator it;
  for(Upg u : this->GetModPossibleUpgrades()) {
    for(it=unhandled.begin(); it != unhandled.end();) {
      if(it->GetType().GetType() == u) {
	ret[u].push_back(*it);
	it = unhandled.erase(it);
      }
      else {
	++it;
      }
    }
  }
  return ret;
}

void Pilot::ReplaceUpgrade(uint8_t index, std::string type, std::string name) {
  this->appliedUpgrades[index] = Upgrade::GetUpgrade(type, name);
}

void Pilot::ApplyUpgrade(Upgrade u) { this->appliedUpgrades.push_back(u); }

// overrides
void Pilot::OverrideSkill(int8_t o)   { this->oSkill   = o; }
void Pilot::OverrideAttack(int8_t o)  { this->oAttack  = o; }
void Pilot::OverrideAgility(int8_t o) { this->oAgility = o; }
void Pilot::OverrideHull(int8_t o)    { this->oHull    = o; }
void Pilot::OverrideShield(int8_t o)  { this->oShield  = o; }
void Pilot::ClearOverrideSkill()      { this->oSkill   = {}; }
void Pilot::ClearOverrideAttack()     { this->oAttack  = {}; }
void Pilot::ClearOverrideAgility()    { this->oAgility = {}; }
void Pilot::ClearOverrideHull()       { this->oHull    = {}; }
void Pilot::ClearOverrideShield()     { this->oShield  = {}; }

// state
bool Pilot::IsEnabled()  const { return this->isEnabled; }
void Pilot::Enable()           { this->isEnabled = true; }
void Pilot::Disable()          { this->isEnabled = false; }

int8_t Pilot::GetCurShield()  const { return this->GetModShield() - this->shieldHits; }
int8_t Pilot::GetShieldHits() const { return this->shieldHits; }
void   Pilot::ShieldUp()            { if(this->shieldHits > 0) this->shieldHits--; }
void   Pilot::ShieldDn()            { if(this->shieldHits < this->GetModShield()) this->shieldHits++; }

int8_t Pilot::GetCurHull()  const { return this->GetModHull() - this->hullHits; }
int8_t Pilot::GetHullHits() const { return this->hullHits; }
void   Pilot::HullUp()            { if(this->hullHits > 0) this->hullHits--; }
void   Pilot::HullDn()            { if(this->hullHits < this->GetModHull()) this->hullHits++; }

std::vector<DamageCard> Pilot::GetCrits() const {
  std::vector<DamageCard> ret;
  for(DmgCrd d : this->crits) {
    ret.push_back(DamageCard::GetDamageCard(d));
  }
  return ret;
}

void Pilot::AddCrit(DmgCrd c) { this->crits.push_back(c); }

void Pilot::RemoveCrit(DmgCrd c) {
  std::vector<DmgCrd>::iterator it = std::find(this->crits.begin(), this->crits.end(), c);
  if(it != this->crits.end()) {
    this->crits.erase(it);
  }
}

bool Pilot::IsDualSided() const { return this->sides.size() > 1; }
void Pilot::Flip() {
  if(this->sides.size() > 1) {
    this->curSide = (this->curSide+1) % 2;
  }
}


// constructor
Pilot::Pilot(SFa         sfa,
             BSz         bs,
             std::string xnam,
             Shp         sh,
             uint8_t     psq,
             int8_t      ps,
             PilotSide   side,
             bool        iu)
  : isUnreleased(iu), subFaction(sfa), baseSize(bs), pilotNameXws(xnam), ship(sh), perSquad(psq),
    skill(ps), isEnabled(true), shieldHits(0), hullHits(0), curSide(0), sides({side}) { }

Pilot::Pilot(SFa         sfa,
             BSz         bs,
             std::string xnam,
             Shp         sh,
             uint8_t     psq,
             int8_t      ps,
             PilotSide   s1,
             PilotSide   s2,
             bool        iu)
  : isUnreleased(iu), subFaction(sfa), baseSize(bs), pilotNameXws(xnam), ship(sh), perSquad(psq),
    skill(ps), isEnabled(true), shieldHits(0), hullHits(0), curSide(0), sides({s1, s2}) { }

}
