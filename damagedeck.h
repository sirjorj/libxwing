#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing {

// deck
enum class Dck {
  CoreSet,
  RebelTransport_Fore,
  RebelTransport_Aft,
  TantiveIV_Fore,
  TantiveIV_Aft,
  ImperialRaider_Fore,
  ImperialRaider_Aft,
  AssaultCarrier_Fore,
  AssaultCarrier_Aft,
  TFA,
  CROC_Fore,
  CROC_Aft,
};

class DeckNotFound : public std::runtime_error {
 public:
  DeckNotFound(Dck d);
  DeckNotFound(std::string d);
};

class Deck {
 public:
  static Deck              GetDeck(Dck d);
  static Deck              GetDeck(std::string d);
  static std::vector<Deck> GetAllDecks();
  Dck         GetType() const;
  std::string GetXws()  const;
  std::string GetName() const;

 private:
  Dck type;
  std::string xws;
  std::string name;

  static std::vector<Deck> deck;

  Deck(Dck         t,
       std::string x,
       std::string n);
};



// trait
enum class Trt {
  Pilot,
  Ship,
};

class TraitNotFound : public std::runtime_error {
 public:
  TraitNotFound(Trt d);
};

class Trait {
 public:
  static Trait GetTrait(Trt d);
  Trt          GetType() const;
  std::string  GetName() const;

 private:
  Trt type;
  std::string name;

  static std::vector<Trait> trait;

  Trait(Trt         t,
        std::string n);
};



// Damage Condition
enum class Dmg {
  // CoreSet
  BlindedPilot,
  ConsoleFire,
  DamagedCockpit,
  DamagedEngine,
  DamagedSensorArray,
  DirectHit,
  InjuredPilot,
  MinorExplosion,
  MinorHullBreach,
  MunitionsFailure,
  StructuralDamage,
  StunnedPilot,
  ThrustControlFire,
  WeaponMalfunction,

  // RebelTransport_Fore
  BroadcastMalfunction,
  DamagedStabilizers,
  //DirectHit,
  HullBreach,
  SecondaryDriveFailure,

  // RebelTransport_Aft
  CommandPodCasualties,
  EngineDamage,
  ProjectorPowerFailure,
  ReactorCowlRupture,
  ReactorLeak,

  // TantiveIV_Fore
  CommsFailure,
  DeckBreach,
  //DirectHit,
  LifeSupportFailure,
  ScrambledScopes,
  //SecondaryDriveFailure,
  TrackingMisalignment,
  WeaponDamaged,

  // TantiveIV_Aft
  //DeckBreach,
  //LifeSupportFailure,
  GridOverload,
  PowerPlantFailure,
  //ProjectorPowerFailure,
  //ReactorLeak,
  StructuralCollapse,

  // IperialRaider_Fore
  //DeckBreach,
  //DirectHit,
  //PowerPlantFailure,
  //ProjectorPowerFailure,
  //ReactorLeak,
  //SecondaryDriveFailure,
  //TrackingMisalignment,
  //WeaponDamaged,

  // ImperialRaider_Aft
  CommandDeckBreach,
  //CommsFailure,
  //DeckBreach,
  //LifeSupportFailure,
  MisfiringThrusters,
  //ScrambledScopes,
  //StructuralCollapse,
  //WeaponDamaged,

  // AssaultCarrier_Fore
  //CommsFailure,
  DamagedDockingClamp,
  //HullBreach,
  //ScrambledScopes,
  //SecondaryDriveFailure,
  ViewportRupture,

  // AssaultCarrier_Aft
  //DamagedDockingClamp,
  //DamagedStabilizers,
  //ProjectorPowerFailure,
  //ReactorCowlRupture,
  //ReactorLeak,
  WeaponsOffline,

  // TFA
  //BlindedPilot,
  //ConsoleFire,
  //DamagedCockpit,
  //DamagedEngine,
  //DamagedSensorArray,
  //DirectHit,
  LooseStabilizer,
  MajorExplosion,
  MajorHullBreach,
  ShakenPilot,
  //StructuralDamage,
  //StunnedPilot,
  //ThrustControlFire,
  WeaponsFailure,

  // CROC_Fore
  //HullBreach,
  //ScrambledScopes,
  //SecondaryDriveFailure,
  SpilledCargo,
  // ViewportRupture

  // CROC_Aft
 DamagedDish,
  //DamagedStabilizers,
  //ProjectorPowerFailure,
  //ReactorCowlRupture,
  //ReactorLeak,
  //SpilledCargo,
  //WeaponsOffline,
};



typedef std::pair<Dmg,Dck> DmgCrd;



// damage deck
class DamageCardNotFound : public std::runtime_error {
 public:
  DamageCardNotFound(DmgCrd dc);
};

class DamageCard {
 public:
  static DamageCard              GetDamageCard(DmgCrd dc);
  static std::vector<DamageCard> GetAllDamageCards(Dck d);
  Dmg         GetDamage() const;
  Deck        GetDeck()   const;
  Trait       GetTrait()  const;
  uint8_t     GetCount()  const;
  std::string GetName()   const;
  std::string GetText()   const;

 private:
  Dmg         damage;
  Dck         deck;
  Trt         trait;
  uint8_t     count;
  std::string name;
  std::string text;

  static std::vector<DamageCard> damageCards;

  DamageCard(Dmg         dmg,
	     Dck         dck,
             Trt         trt,
             uint8_t     cou,
             std::string nam,
             std::string txt);
};

}
